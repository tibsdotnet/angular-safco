using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Abp.Auditing;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.IO.Extensions;
using Abp.Runtime.Session;
using Abp.UI;
using Abp.Web.Mvc.Authorization;
using TIBS.SAFCO.Authorization.Users;
using TIBS.SAFCO.Net.MimeTypes;
using TIBS.SAFCO.Storage;
using Abp.Domain.Repositories;
using TIBS.SAFCO.Employees;
using TIBS.SAFCO.Employees.Dto;
using Abp.Linq;
using System.Linq;
using Abp.Linq.Extensions;
using System.IO;
using TIBS.SAFCO.Companies.Dto;
using TIBS.SAFCO.EntityFramework;

namespace TIBS.SAFCO.Web.Controllers
{
    [AbpMvcAuthorize]
    public class ProfileController : SAFCOControllerBase
    {
        private readonly UserManager _userManager;
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IRepository<Employee> _EmployeeRepository;
        SAFCODbContext db = new SAFCODbContext();
        public ProfileController(IRepository<Employee> EmployeeRepository, UserManager userManager, IBinaryObjectManager binaryObjectManager)
        {
            _userManager = userManager;
            _binaryObjectManager = binaryObjectManager;
            _EmployeeRepository = EmployeeRepository;
        }

        [DisableAuditing]
        public async Task<FileResult> GetProfilePicture()
        {
            var user = await _userManager.GetUserByIdAsync(AbpSession.GetUserId());
            if (user.ProfilePictureId == null)
            {
                return GetDefaultProfilePicture();
            }

            return await GetProfilePictureById(user.ProfilePictureId.Value);
        }

        [DisableAuditing]
        public async Task<FileResult> GetProfilePictureById(string id = "")
        {
            if (id.IsNullOrEmpty())
            {
                return GetDefaultProfilePicture();                
            }

            return await GetProfilePictureById(Guid.Parse(id));
        }

        [UnitOfWork]
        public async virtual Task ChangeProfilePicture()
        {
            if (Request.Files.Count <= 0 || Request.Files[0] == null)
            {
                throw new UserFriendlyException(L("ProfilePicture_Change_Error"));
            }
            
            var file = Request.Files[0];

            if (file.ContentLength > 30720) //30KB.
            {
                throw new UserFriendlyException(L("ProfilePicture_Warn_SizeLimit"));
            }

            var user = await _userManager.GetUserByIdAsync(AbpSession.GetUserId());
            long userId = AbpSession.GetUserId();
            var emp = _EmployeeRepository.GetAll().Where(p=>p.UserId==userId).FirstOrDefault();
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("ProfilePicture_Change_Error"));
                }
               
                string filename = file.FileName;
                string path = System.Web.HttpContext.Current.Server.MapPath("~/EmployeeProfilePicture/" + emp.Id + "");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                path = Path.Combine(Server.MapPath("~/EmployeeProfilePicture/" + emp.Id + ""), filename);
                //path = Path.Combine(Server.MapPath("~/Photos"), empId + ".jpg");
                file.SaveAs(path);
                path = Url.Content(Path.Combine("/EmployeeProfilePicture/" + emp.Id + "/" + filename));
                CreateCompanyInput input = new CreateCompanyInput();


                var contactinput = db.Employees.Where(p => p.Id == emp.Id).FirstOrDefault();
                contactinput.PhotoPath = path;
                // var contactcompany = contactinput.MapTo<CompanyContact>();
                db.SaveChanges();


            //Delete old picture
            if (user.ProfilePictureId.HasValue)
            {
                await _binaryObjectManager.DeleteAsync(user.ProfilePictureId.Value);
            }

            //Save new picture
            var storedFile = new BinaryObject(file.InputStream.GetAllBytes());
            await _binaryObjectManager.SaveAsync(storedFile);
            
            //Update new picture on the user
            user.ProfilePictureId = storedFile.Id;
        }

        public FileResult GetDefaultProfilePicture()
        {
            return File(Server.MapPath("~/Common/Images/default-profile-picture.png"), MimeTypeNames.ImagePng);
        }

        public async Task<FileResult> GetProfilePictureById(Guid profilePictureId)
        {
            var file = await _binaryObjectManager.GetOrNullAsync(profilePictureId);
            if (file == null)
            {
                return GetDefaultProfilePicture();
            }

            return File(file.Bytes, MimeTypeNames.ImageJpeg);
        }
    }
}