﻿using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TIBS.SAFCO.Companies.Dto;
using TIBS.SAFCO.CompanyContacts;
using Abp.AutoMapper;
using TIBS.SAFCO.EntityFramework;
using System.Threading.Tasks;
using TIBS.SAFCO.Authorization.Users;
using TIBS.SAFCO.Storage;
using Abp.Authorization.Users;
using Abp.IO.Extensions;
using Abp.Domain.Uow;

namespace TIBS.SAFCO.Web.Controllers
{
    public class FileUploadController : SAFCOControllerBase
    {
       public static int companycontactperonid;
       private readonly IRepository<CompanyContact> _ContactRepository;
       SAFCODbContext db = new SAFCODbContext();
       private readonly UserManager _userManager;
       private readonly IBinaryObjectManager _binaryObjectManager;
        //
        // GET: /FileUpload/
       public FileUploadController(IRepository<CompanyContact> ContactRepository, UserManager userManager, IBinaryObjectManager binaryObjectManager)
       {
           _ContactRepository = ContactRepository;
           _userManager = userManager;
           _binaryObjectManager = binaryObjectManager;
       }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult UploadPicture()
        {
            if (companycontactperonid == 0)
            {
                companycontactperonid = (db.CompanyContacts.Select(x => (int?)x.Id).Max() ?? 0);

            }
            if (Request.Files.Count <= 0 || Request.Files[0] == null)
            {
                throw new UserFriendlyException(L("ProfilePicture_Change_Error"));
            }

            var file = Request.Files[0];
            string filename = file.FileName;
            string path = System.Web.HttpContext.Current.Server.MapPath("~/ProfilePicture/" + companycontactperonid + "");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            path = Path.Combine(Server.MapPath("~/ProfilePicture/" + companycontactperonid + ""), filename);
            //path = Path.Combine(Server.MapPath("~/Photos"), empId + ".jpg");
            file.SaveAs(path);
            path = Url.Content(Path.Combine("ProfilePicture/" + companycontactperonid + "/" + filename));
            CreateCompanyInput input = new CreateCompanyInput();


            var contactinput = db.CompanyContacts.Where(p => p.Id == companycontactperonid).FirstOrDefault();
                contactinput.ProfilePictuePath = path;
               // var contactcompany = contactinput.MapTo<CompanyContact>();
                db.SaveChanges();
            
               // _ContactRepository.UpdateAsync(contactcompany);


            
            return View();
        }

        [UnitOfWork]
        public async virtual Task UploadEmployeeProfileImage()
        {
            try
            {
                if (companycontactperonid == 0)
                {
                    companycontactperonid = (db.Employees.Select(x => (int?)x.Id).Max() ?? 0);

                }
                if (Request.Files.Count <= 0 || Request.Files[0] == null)
                {
                    throw new UserFriendlyException(L("ProfilePicture_Change_Error"));
                }
                var file = Request.Files[0];
                string filename = file.FileName;
                string path = System.Web.HttpContext.Current.Server.MapPath("~/EmployeeProfilePicture/" + companycontactperonid + "");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                path = Path.Combine(Server.MapPath("~/EmployeeProfilePicture/" + companycontactperonid + ""), filename);
                //path = Path.Combine(Server.MapPath("~/Photos"), empId + ".jpg");
                file.SaveAs(path);
                path = Url.Content(Path.Combine("/EmployeeProfilePicture/" + companycontactperonid + "/" + filename));
                CreateCompanyInput input = new CreateCompanyInput();


                var contactinput = db.Employees.Where(p => p.Id == companycontactperonid).FirstOrDefault();
                contactinput.PhotoPath = path;
                // var contactcompany = contactinput.MapTo<CompanyContact>();
                db.SaveChanges();

                long UserId = (long)contactinput.UserId;

                //await ChangeUserProfilePicture(UserId);

                var user = await _userManager.GetUserByIdAsync(UserId);
                //Delete old picture
                if (user.ProfilePictureId.HasValue)
                {
                    await _binaryObjectManager.DeleteAsync(user.ProfilePictureId.Value);
                }

                //Save new picture
                var storedFiles = new BinaryObject(file.InputStream.GetAllBytes());
                await _binaryObjectManager.SaveAsync(storedFiles);
                user.ProfilePictureId = storedFiles.Id;
            }
            catch (Exception ex)
            {

            }
           
        }
        //[UnitOfWork]
        //public async Task ChangeUserProfilePicture(long Id)
        //{
        //    var file = Request.Files[0];
        //    var user = await _userManager.GetUserByIdAsync(Id);
        //    //Delete old picture
        //    if (user.ProfilePictureId.HasValue)
        //    {
        //        await _binaryObjectManager.DeleteAsync(user.ProfilePictureId.Value);
        //    }

        //    //Save new picture
        //    var storedFile = new BinaryObject(file.InputStream.GetAllBytes());
        //    await _binaryObjectManager.SaveAsync(storedFile);
        //    user.ProfilePictureId = storedFile.Id;
        //}
        public void GetInput(int Id)
        {
            companycontactperonid = Id;
        }
	}
}