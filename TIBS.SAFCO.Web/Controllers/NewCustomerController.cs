﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Abp.Runtime.Session;
using TIBS.SAFCO.EntityFramework;
using TIBS.SAFCO.Authorization.Users;
using System.Data.SqlClient;

namespace TIBS.SAFCO.Web.Controllers
{
    
    public class NewCustomerController : SAFCOControllerBase
    {
        SAFCODbContext db = new SAFCODbContext();
        
        //
        // GET: /NewCustomer/
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult ResultOFCustomer(long id)
        {
            long userId = id;
            if (userId == 0)
            {
                userId = (long)AbpSession.GetUserId();
            }

            string name=null;
            using (var context = new SAFCODbContext())
            {
                try
                {
                    var Id = new SqlParameter
                    {
                        ParameterName = "UserId",
                        Value = userId
                    };
                    var data = context.Database.SqlQuery<returnn>("exec Sp_GetUserRoles @UserId", Id).ToList();
                    name = data[0].RoleName;
                }
                catch (Exception ex)
                {

                }
            }

            int customercount=0;
            if (name == "Admin" || name == "Management" || name == "Audit/Reporting")
            {
                var customer = from c in db.Companies where c.StatusId == 4 select c;
                customercount = customer.Count();
            }
            else
            {
                var emp = (from c in db.Employees where c.UserId == userId select c.Id).FirstOrDefault();
                var customer = from c in db.Companies where c.ManagedId == emp && c.StatusId == 4 select c;
                customercount = customer.Count();
            }
           
            return Json(customercount);
        }
        public JsonResult ResultNegotiation(long id)
        {
            long userId = id;
            if (userId == 0)
            {
                userId = (long)AbpSession.GetUserId();
            }

            string name = null;
            using (var context = new SAFCODbContext())
            {
                try
                {
                    var Id = new SqlParameter
                    {
                        ParameterName = "UserId",
                        Value = userId
                    };
                    var data = context.Database.SqlQuery<returnn>("exec Sp_GetUserRoles @UserId", Id).ToList();
                    name = data[0].RoleName;
                }
                catch (Exception ex)
                {

                }
            }
            int negotiationcount = 0;
            if (name == "Admin" || name == "Management" || name == "Audit/Reporting")
            {
                var negotiation = from c in db.Leads where c.MileStoneId == 4 select c;
                negotiationcount = negotiation.Count();
            }
            else
            {
                var emp = (from c in db.Employees where c.UserId == userId select c.Id).FirstOrDefault();
                var negotiation = from c in db.Leads where c.EmployeeId == emp && c.MileStoneId == 4 select c;
                negotiationcount = negotiation.Count();
            }
            return Json(negotiationcount);
        }
        public JsonResult ResultProspect(long id)
        {
            long userId = id;
            if (userId == 0)
            {
                userId = (long)AbpSession.GetUserId();
            }

            string name = null;
            using (var context = new SAFCODbContext())
            {
                try
                {
                    var Id = new SqlParameter
                    {
                        ParameterName = "UserId",
                        Value = userId
                    };
                    var data = context.Database.SqlQuery<returnn>("exec Sp_GetUserRoles @UserId", Id).ToList();
                    name = data[0].RoleName;
                }
                catch (Exception ex)
                {

                }
            }
            int prospectcount = 0;
            if (name == "Admin" || name == "Management" || name == "Audit/Reporting")
            {
                var prospect = from c in db.Leads where c.MileStoneId == 3 select c;
                prospectcount = prospect.Count();
            }
            else
            {
                var emp = (from c in db.Employees where c.UserId == userId select c.Id).FirstOrDefault();
                var prospect = from c in db.Leads where c.EmployeeId == emp && c.MileStoneId == 3 select c;
                prospectcount = prospect.Count();
            }
            return Json(prospectcount);
        }
	}

    public class returnn
    {
        public string RoleName { get; set; }
    }
}