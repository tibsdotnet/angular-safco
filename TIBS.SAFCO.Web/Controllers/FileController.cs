﻿using System.IO;
using System.Web.Mvc;
using Abp.Auditing;
using Abp.Authorization;
using Abp.UI;
using Abp.Web.Mvc.Authorization;
using TIBS.SAFCO.Dto;
using System.Data.SqlClient;
using System;
using TIBS.SAFCO.EntityFramework;
using TIBS.SAFCO.Compnaies;
using Abp.Domain.Repositories;
using System.Linq;

namespace TIBS.SAFCO.Web.Controllers
{
    public class FileController : SAFCOControllerBase
    {
        private readonly IAppFolders _appFolders;

        public FileController(IAppFolders appFolders)
        {
            _appFolders = appFolders;
        }

        [AbpMvcAuthorize]
        [DisableAuditing]
        public ActionResult DownloadTempFile(FileDto file)
        {
            CheckModelState();

            var filePath = Path.Combine(_appFolders.TempFileDownloadFolder, file.FileToken);
            if (!System.IO.File.Exists(filePath))
            {
                throw new UserFriendlyException(L("RequestedFileDoesNotExists"));
            }

            var fileBytes = System.IO.File.ReadAllBytes(filePath);
            System.IO.File.Delete(filePath);
            return File(fileBytes, file.FileType, file.FileName);
        }

        public void Update(int companyid, int employeeid, int id)
        {

        }

        public void UpdateStatus(int leadId, int StatusId)
        {
        }
    }
}