﻿using System.Web.Mvc;

namespace TIBS.SAFCO.Web.Controllers
{
    public class HomeController : SAFCOControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}