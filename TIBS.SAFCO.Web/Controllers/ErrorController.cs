﻿using System.Web.Mvc;
using Abp.Auditing;

namespace TIBS.SAFCO.Web.Controllers
{
    public class ErrorController : SAFCOControllerBase
    {
        [DisableAuditing]
        public ActionResult E404()
        {
            return View();
        }
    }
}