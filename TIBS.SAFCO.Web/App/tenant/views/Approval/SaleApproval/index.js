﻿(function () {
    appModule.controller('tenant.views.Approval.SaleApproval.index', [
        '$scope', '$modal', 'uiGridConstants', 'abp.services.app.saleApproval','$location',
        function ($scope, $modal, uiGridConstants, saleApprovalService,$location) {
            var vm = this;
           // alert("jhui");
            $scope.$on('$viewContentLoaded', function () {
                Metronic.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            var cellToolTipTemplate =
                '<div class="ui-grid-cell-contents" tooltip = "{{COL_FIELD}}" tooltip-append-to-body="true" tooltip-popup-delay="200" >{{ COL_FIELD }}</div>';

            vm.SaleApprovalGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                     {
                         name: app.localize('Actions'),
                         enableSorting: false,
                         width: 80,
                         cellTemplate:

                             '<div class=\"ui-grid-cell-contents text-center\">' +
                                 '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.saleedit(row.entity)"><i class="fa fa-pencil-square-o font-blue"></i></button>' +
                              //    '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.deleteCompany(row.entity)"><i class="fa fa-trash-o font-red"></i></button>' +
                                 '</div>'
                     },
                    
                    {
                        name: app.localize('Company'),
                        field: 'title',
                        minWidth: 120,
                        cellTemplate: cellToolTipTemplate
                    },
                   
                    {
                        name: app.localize('FirstName'),
                        field: 'firstName',
                        minWidth: 120,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('LastName'),
                        field: 'lastName',
                        minWidth: 120,
                        cellTemplate: cellToolTipTemplate
                    },
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.getSaleApproval();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getSaleApproval();
                    });
                },
                data: []
            };

            vm.saleedit = function (lead) {
                $location.path('createsaleapproval/' + lead.id);
            };

            vm.getSaleApproval = function () {
                vm.loading = true;
                saleApprovalService.getSaleApprovalReport({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {

                    vm.SaleApprovalGridOptions.totalItems = result.totalCount;
                    vm.SaleApprovalGridOptions.data = result.items;

                }).finally(function () {
                    vm.loading = false;
                });
            };



            vm.getSaleApproval();
        }
    ]);
})();