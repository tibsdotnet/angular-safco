﻿appModule.filter('propsFilter', function () {
    return function (items, props) {
        var out = [];

        if (angular.isArray(items)) {
            var keys = Object.keys(props);

            items.forEach(function (item) {
                var itemMatches = false;

                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});

(function () {
    appModule.controller('tenant.views.Approval.SaleApproval.EditApproval', [
        '$scope', '$modal', 'abp.services.app.saleApproval', '$location',
        function ($scope, $modal, saleApprovalService, $location) {
            var vm = this;
            vm.saving = false;
            vm.lead = null;
            var leadId = $scope.leadId;
            vm.save = function () {
                vm.saving = true;
                vm.lead.actualClosure = vm.dates.date1;
                vm.lead.chefId = $scope.salesSupport.selected.employeeId;
                vm.lead.employeeId = $scope.salesEmployee.selected.employeeId;
                saleApprovalService.updateSalesApproval(vm.lead).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $location.path('/saleapproval');
                    //$modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            function init() {
                saleApprovalService.getLeadForEdit({
                    id: leadId
                }).success(function (result) {
                    //console.log(result.lead);
                    vm.companyName = result.companyName;
                    vm.salesEmployee = result.employee;
                    vm.salesSupport = result.userList;
                    vm.lead = result.lead;

                    for(var i=0;i<vm.salesEmployee.length;i++)
                    {
                        var emp = vm.salesEmployee[i].employeeId;
                        if(emp==vm.lead.employeeId)
                        {
                            $scope.salesEmployee.selected = { employeeId: vm.salesEmployee[i].employeeId, employeeName: vm.salesEmployee[i].employeeName };
                        }
                    }
                });
            }

            init();

            vm.cancel = function () {
                // $modalInstance.dismiss();
                $location.path('/salesapproval');
            };
            var in10Days = new Date();
            in10Days.setDate(in10Days.getDate() + 10);

            this.dates = {
                date1: new Date(),
                date2: new Date(),
            };

            //vm.dated.date1 = false;

            this.open = {
                date1: false,
                date2: false,

            };

            // Disable weekend selection
            //this.disabled = function (date, mode) {
            //    return (mode === 'day' && (new Date().toDateString() == date.toDateString()));
            //};

            this.dateOptions = {
                showWeeks: false,
                startingDay: 1
            };

            this.timeOptions = {
                readonlyInput: false,
                showMeridian: true
            };

            this.dateModeOptions = {
                minMode: 'year',
                maxMode: 'year'
            };

            this.openCalendar = function (e, date) {

                vm.open[date] = true;


            };
            $scope.salesEmployee = {};
            $scope.salesSupport = {};
        }
    ]);
})();