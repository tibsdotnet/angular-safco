﻿appModule.filter('propsFilter', function () {
    return function (items, props) {
        var out = [];

        if (angular.isArray(items)) {
            var keys = Object.keys(props);

            items.forEach(function (item) {
                var itemMatches = false;

                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});

(function () {
    appModule.controller('tenant.views.Geography.Region.createOrEditModal', [
        '$scope', '$modalInstance', 'abp.services.app.region', 'Regionid',
        function ($scope, $modalInstance, RegionService, Regionid) {
            var vm = this;
            vm.saving = false;
            vm.region = null;
            vm.country = [];

            vm.save = function () {
                //var assignedcountryNames = _.map(
                //    _.where(vm.country, { isAssigned: true }), //Filter assigned roles
                //    function (country) {
                //        vm.region.countryId = country.countryId;
                //        return country.countryName; //Get names
                //    });
                vm.saving = true;
                vm.region.countryId = $scope.country.selected.countryId;
                RegionService.createOrUpdateRegion(vm.region).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            //vm.getAssignedcountryCount = function () {
            //    return _.where(vm.country, { isAssigned: true }).length;
            //};

            //$scope.country = {};
            //$scope.refreshcountrys = function (country) {
            //    var params = { country: country, sensor: false };
            //    select2Region.country({ name: params.country }).success(function (result) {
            //        $scope.countrys = result.select2data;
            //    });
            //};

            function init() {
                
                RegionService.getRegionForEdit({ 
                    id: Regionid
                }).success(function (result) {
                    vm.region = result.regions;
                    vm.country = result.countryName;
                    console.log(vm.country);
                    for (i = 0; i < vm.country.length; i++) {
                        var da = vm.country[i].countryId;
                        if (da == vm.region.countryId) {
                            $scope.country.selected = { countryId: vm.country[i].countryId, countryName: vm.country[i].countryName };
                        }
                    }
                    
                });
            }

            init();

            $scope.country = {};
        }
    ]);
})();