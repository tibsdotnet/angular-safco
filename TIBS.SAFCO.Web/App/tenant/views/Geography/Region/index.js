﻿(function () {
    appModule.controller('tenant.views.Geography.Region.index', [
        '$scope', '$modal', 'uiGridConstants', 'abp.services.app.region',
        function ($scope, $modal, uiGridConstants, RegionDetail) {
            var vm = this;
           //alert("nkj");

            $scope.$on('$viewContentLoaded', function () {
                Metronic.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };
            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.AddressBook.Region.CreateNewRegion'),
                'edit': abp.auth.hasPermission('Pages.Tenant.AddressBook.Region.EditRegion'),
                'delete': abp.auth.hasPermission('Pages.Tenant.AddressBook.Region.DeleteRegion')
            };
            vm.exportToExcels = function () {
                RegionDetail.getRegionToExcel(vm.requestParams)
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };
            var cellToolTipTemplate =
               '<div class="ui-grid-cell-contents" tooltip = "{{COL_FIELD}}" tooltip-append-to-body="true" tooltip-popup-delay="200" >{{ COL_FIELD }}</div>';

            vm.RegionGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                            //'<div class=\"ui-grid-cell-contents\">' +
                            //'  <div class="btn-group dropdown" dropdown="">' +
                            //'    <button class="btn btn-xs btn-primary blue" dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            //'    <ul class="dropdown-menu">' +
                            //'      <li><a ng-click="grid.appScope.editRegion(row.entity)">' + app.localize('Edit') + '</a></li>' +
                            //'      <li><a ng-click="grid.appScope.deleteRegion(row.entity)">' + app.localize('Delete') + '</a></li>' +
                            //'    </ul>' +
                            //'  </div>' +
                            //'</div>'

                            '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button class="btn btn-default btn-xs" ng-if="grid.appScope.permissions.edit" ng-click="grid.appScope.editRegion(row.entity)"><i class="fa fa-pencil-square-o font-blue"></i></button>' +
                                 '  <button class="btn btn-default btn-xs" ng-if="grid.appScope.permissions.delete" ng-click="grid.appScope.deleteRegion(row.entity)"><i class="fa fa-trash-o font-red"></i></button>' +
                                '</div>'
                    },
                    {
                        name: app.localize('RegionCode'),
                        field: 'code',
                        minWidth: 120,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('RegionName'),
                        field: 'name',
                        minWidth: 120,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('Country'),
                        field: 'countryName',
                        enableSorting: false,
                        minWidth: 150,
                        cellTemplate: cellToolTipTemplate
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.getRegion();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getRegion();
                    });
                },
                data: []
            };

            vm.getRegion = function () {
               
                vm.loading = true;
                RegionDetail.getRegion({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.RegionGridOptions.totalItems = result.totalCount;
                    vm.RegionGridOptions.data = result.items;
                    
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.openCreateRegionModal = function () {
                openRegionModal(null);
            };

            vm.editRegion = function (Region) {
                openRegionModal(Region.id);
            };

            function openRegionModal(Regionid) {
                
                var modalInstance = $modal.open({
                   
                    templateUrl: '~/App/tenant/views/Geography/Region/createOrEditModal.cshtml',
                    controller: 'tenant.views.Geography.Region.createOrEditModal as vm',
                    backdrop: 'static',
                    resolve: {
                        Regionid: function () {
                            return Regionid;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    vm.getRegion();
                });
            }

            vm.exportToExcel = function () {

                RegionDetail.getRegionToExcel(vm.requestParams)
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.deleteRegion = function (Region) {
                abp.message.confirm(
                    app.localize('AreYouSureToDelete Region', Region.RegionName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            RegionDetail.deleteRegion({
                                id: Region.id
                            }).success(function () {
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                                vm.getRegion();
                            });
                        }
                    }
                );
            };
           
            vm.getRegion();
           // alert("nkj");
        }
    ]);
})();