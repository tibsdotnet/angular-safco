﻿(function () {
    appModule.controller('tenant.views.Geography.Country.CreateOrEdit', [
        '$scope', '$modalInstance', 'abp.services.app.country', 'countryid',
        function ($scope, $modalInstance, countryService, countryid) {
            var vm = this;
            vm.saving = false;
            vm.country = [];
           // alert(countryid);
            
            vm.save = function () {
                vm.saving = true;
                countryService.createOrUpdateUser(vm.country).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };
           // alert("khk");
            function init() {
                countryService.getCountryForEdit({
                    id: countryid
                }).success(function (result) {
                    vm.country = result.country;
                });
            }

            init();

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();