﻿(function () {
    appModule.controller('tenant.views.Geography.Country.index', [
        '$scope', '$modal', 'uiGridConstants', 'abp.services.app.country',
        function ($scope, $modal, uiGridConstants, countryService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                Metronic.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };
            vm.permissions = {
                createCountry: abp.auth.hasPermission('Pages.Tenant.AddressBook.Country.CreateNewCountry'),
                'edit': abp.auth.hasPermission('Pages.Tenant.AddressBook.Country.EditCountry'),
                'delete': abp.auth.hasPermission('Pages.Tenant.AddressBook.Country.DeleteCountry')
            };
            var cellToolTipTemplate =
               '<div class="ui-grid-cell-contents" tooltip = "{{COL_FIELD}}" tooltip-append-to-body="true" tooltip-popup-delay="200" >{{ COL_FIELD }}</div>';
            vm.exportToExcel = function () {
                countryService.getCountryToExcel(vm.requestParams)
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };
            vm.CountryGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 90,
                        cellTemplate:
                           
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button class="btn btn-default btn-xs" ng-if="grid.appScope.permissions.edit" ng-click="grid.appScope.editCountry(row.entity)"><i class="fa fa-pencil-square-o font-blue"></i></button>' +
                                 '  <button class="btn btn-default btn-xs" ng-if="grid.appScope.permissions.delete" ng-click="grid.appScope.deleteCountry(row.entity)"><i class="fa fa-trash-o font-red"></i></button>' +
                                '</div>'
                    },
                    {
                        name: app.localize('CountryName'),
                        field: 'countryName',
                        minWidth: 140,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('PrintCountryName'),
                        field: 'printCountryName',
                        minWidth: 180,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('ISDCode'),
                        field: 'isdCode',
                        enableSorting: false,
                        minWidth: 100,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('ISOCode'),
                        field: 'isoCode',
                        enableSorting: false,
                        minWidth: 100,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('ISO3Code'),
                        field: 'isO3Code',
                        enableSorting: false,
                        minWidth: 100,  
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('NumericCode'),
                        field: 'numericCode',
                        enableSorting: false,
                        minWidth: 120,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('Divisions'),
                        field: 'divisions',
                        minWidth: 115,
                        cellTemplate: cellToolTipTemplate
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.getCountry();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getCountry();
                    });
                },
                data: []
            };

            vm.getCountry = function () {
                vm.loading = true;
                countryService.getCountry({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.CountryGridOptions.totalItems = result.totalCount;
                    vm.CountryGridOptions.data = result.items;

                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editCountry = function (country) {
                openCountryModal(country.id);
            };

            vm.openCreateCountryModal = function () {
                openCountryModal(null);
            };

            function openCountryModal(countryid) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/Geography/Country/CreateOrEdit.cshtml',
                    controller: 'tenant.views.Geography.Country.CreateOrEdit as vm',
                    backdrop: 'static',
                    resolve: {
                        countryid: function () {
                            return countryid;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    vm.getCountry();
                });
            }


            vm.deleteCountry = function (country) {
                abp.message.confirm(
                    app.localize('AreYouSureToDeleteCountry', country.countryName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            countryService.deleteCountry({
                                id: country.id
                            }).success(function () {
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                                vm.getCountry();
                            });
                        }
                    }
                );
            };

            vm.getCountry();
        }
    ]);
})();