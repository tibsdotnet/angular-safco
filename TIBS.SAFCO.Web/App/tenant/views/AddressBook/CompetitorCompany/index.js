﻿(function () {
    appModule.controller('tenant.views.AddressBook.CompetitorCompany.index', [
        '$scope', '$modal', 'uiGridConstants', 'abp.services.app.competitorCompany',
        function ($scope, $modal, uiGridConstants, competitorCompaniesService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                Metronic.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };
            vm.exportToExcel = function () {
                competitorCompaniesService.getCompetitorCompanyToExcel(vm.requestParams)
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };
            var cellToolTipTemplate =
               '<div class="ui-grid-cell-contents" tooltip = "{{COL_FIELD}}" tooltip-append-to-body="true" tooltip-popup-delay="200" >{{ COL_FIELD }}</div>';

            vm.permissions = {
                createCompany: abp.auth.hasPermission('Pages.Tenant.AddressBook.CompetitorCompany.CreateNewCompetitorCompany'),
                'edit': abp.auth.hasPermission('Pages.Tenant.AddressBook.CompetitorCompany.EditCompetitorCompany'),
                'delete': abp.auth.hasPermission('Pages.Tenant.AddressBook.CompetitorCompany.DeleteCompetitorCompany')
            };
            vm.CompetitorCompaniesGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                           
                        '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button class="btn btn-default btn-xs" ng-if="grid.appScope.permissions.edit" ng-click="grid.appScope.editCompany(row.entity)"><i class="fa fa-pencil-square-o font-blue"></i></button>' +
                                 '  <button class="btn btn-default btn-xs" ng-if="grid.appScope.permissions.delete" ng-click="grid.appScope.deleteCompany(row.entity)"><i class="fa fa-trash-o font-red"></i></button>' +
                                '</div>'
                    },
                    {
                        name: app.localize('CompetitorCompanyCode'),
                        field: 'competitorCompanyCode',
                        minWidth: 120,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('CompetitorCompanyName'),
                        field: 'competitorCompanyName',
                        minWidth: 120,
                        cellTemplate: cellToolTipTemplate
                    }


                ],
                onRegisterApi: function (gridApi) {

                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.getCompany();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getCompany();
                    });
                },
                data: []
            };

            vm.getCompany = function () {
                vm.loading = true;
                competitorCompaniesService.getCompetitorCompany({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {

                    vm.CompetitorCompaniesGridOptions.totalItems = result.totalCount;
                    vm.CompetitorCompaniesGridOptions.data = result.items;

                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.openCreateCompanyModal = function () {
                openCreateCompanyModal(null);
            };

            vm.editCompany = function (company) {
                openCreateCompanyModal(company.id);
            };

            function openCreateCompanyModal(companyid) {

                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/AddressBook/CompetitorCompany/createCompanyModal.cshtml',
                    controller: 'tenant.views.AddressBook.CompetitorCompany.createCompanyModal as vm',
                    backdrop: 'static',
                    resolve: {
                        companyid: function () {
                            return companyid;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    vm.getCompany();
                });
            }


            vm.deleteCompany = function (company) {
                abp.message.confirm(
                    app.localize('AreYouSureToDeleteCompany', company.competitorCompanyName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            competitorCompaniesService.deleteCompetitorCompany({
                                id: company.id
                            }).success(function () {
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                                vm.getCompany();
                            });
                        }
                    }
                );
            };

            vm.getCompany();
        }
    ]);
})();