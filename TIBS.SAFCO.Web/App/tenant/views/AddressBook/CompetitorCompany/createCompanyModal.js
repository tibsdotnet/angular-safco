﻿(function () {
    appModule.controller('tenant.views.AddressBook.CompetitorCompany.createCompanyModal', [
        '$scope', '$modalInstance', 'abp.services.app.competitorCompany', 'companyid',
        function ($scope, $modalInstance, competitorCompaniesService, companyid) {
            var vm = this;
            vm.saving = false;
            vm.company = null;

            vm.save = function () {
                vm.saving = true;
                competitorCompaniesService.createOrUpdateCompetitorCompany(vm.company).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            function init() {
                competitorCompaniesService.getCompanyForEdit({
                    id: companyid
                }).success(function (result) {
                    //console.log(result.company);
                    vm.company = result.company;
                });
            }

            init();

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();