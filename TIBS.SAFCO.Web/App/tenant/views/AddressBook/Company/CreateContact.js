﻿appModule.filter('propsFilter', function () {
    return function (items, props) {
        var out = [];

        if (angular.isArray(items)) {
            var keys = Object.keys(props);

            items.forEach(function (item) {
                var itemMatches = false;

                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});
(function () {
    appModule.controller('tenant.views.AddressBook.Company.CreateContact', [
        '$scope', '$uibModalInstance', 'abp.services.app.company','FileUploader', 'companyid','contactid', 'companyName',
    function ($scope, $modalInstance, companyService,fileUploader, companyid,contactid, companyName) {
        var vm = this;
        vm.saving = false;
        vm.Contact = [];
       // alert(companyName);
        vm.companyName = companyName;
        vm.cancel = function () {
            $modalInstance.dismiss();
        };
        vm.uploader = new fileUploader({
            url: abp.appPath + 'FileUpload/UploadPicture',
            queueLimit: 1,
            filters: [{
                name: 'imageFilter',
                fn: function (item, options) {
                    //File type check
                    var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                    if ('|jpg|jpeg|'.indexOf(type) === -1) {
                        abp.message.warn(app.localize('ProfilePicture_Warn_FileType'));
                        return false;
                    }

                    //File size check
                    if (item.size > 30720) //30KB
                    {
                        abp.message.warn(app.localize('ProfilePicture_Warn_SizeLimit'));
                        return false;
                    }

                    return true;
                }
            }]
        });
        vm.save = function () {
            vm.saving = true;
            vm.Contact.regionId = $scope.region.selected.regionId;
            vm.Contact.companyId = companyid;
           // console.log(vm.Contact);
            companyService.createOrUpdate(vm.Contact ).success(function () {
                abp.notify.info(app.localize('SavedSuccessfully'));
                $.ajax({


                    type: 'POST',
                    url: '/FileUpload/GetInput',
                    data: { id: contactid },
                    success: function () {

                        vm.uploader.uploadAll();


                    },
                    failure: function (response) {
                        $('#result').html(response);

                    }
                });
                $modalInstance.close();
            }).finally(function () {
                vm.saving = false;
            });
        };
        function init() {
            companyService.getContactForEdit({
                id: contactid
            }).success(function (result) {
                vm.Contact = result.companyContact;
                console.log(vm.region);
                vm.region = result.region;
                for (i = 0; i < vm.region.length; i++) {
                    var da = vm.region[i].regionId;
                    if (da == vm.Contact.regionId) {
                        $scope.region.selected = { regionId: vm.region[i].regionId, regionName: vm.region[i].regionName };
                    }
                }
            })
        }
         init();
        $scope.region = {};

    }
    ]);
})();