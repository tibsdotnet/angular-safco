﻿(function () {
    appModule.controller('tenant.views.AddressBook.Company.index', [
        '$scope', '$modal', 'uiGridConstants', 'abp.services.app.company',
        function ($scope, $modal, uiGridConstants, CompanyService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                Metronic.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };
            
            vm.permissions = {
                createCompany: abp.auth.hasPermission('Pages.Tenant.AddressBook.Company.CreateNewCompany'),
                'edit': abp.auth.hasPermission('Pages.Tenant.AddressBook.Company.EditCompany'),
                'delete': abp.auth.hasPermission('Pages.Tenant.AddressBook.Company.DeleteCompany')
            };

            var cellToolTipTemplate =
               '<div class="ui-grid-cell-contents" tooltip = "{{COL_FIELD}}" tooltip-append-to-body="true" tooltip-popup-delay="200" >{{ COL_FIELD }}</div>';

            vm.getCompany = function () {
                CompanyService.getCompany({ filter: vm.filterText }).success(function (result) {
                    vm.countries = result.items;
                });
            }
            vm.exportToExcel = function () {
                CompanyService.getCompanyToExcel(vm.requestParams)
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.CompanyGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: rowTemplate(),
                //rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 125,
                        cellTemplate:
                          '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button class="btn btn-default btn-xs" ng-if="grid.appScope.permissions.edit" ng-show="row.entity.editDeltePermission" ng-click="grid.appScope.editCompany(row.entity)"><i class="fa fa-pencil-square-o font-blue"></i></button>' +
                                 '  <button class="btn btn-default btn-xs" ng-if="grid.appScope.permissions.delete" ng-show="row.entity.editDeltePermission" ng-click="grid.appScope.deleteCompany(row.entity)"><i class="fa fa-trash-o font-red"></i></button>' +
                                  '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.openCompanyModal(row.entity)"><i class="fa fa-search font-blue"></i></button>' +
                                '</div>'
                    },
                    {
                        name: app.localize('CompanyName'),
                        field: 'title',
                        minWidth: 200,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('PhoneNo'),
                        field: 'phoneNo',
                        minWidth: 130,
                        cellTemplate: cellToolTipTemplate
                    },
                    //{
                    //    name: app.localize('Description'),
                    //    field: 'description',
                    //    minWidth: 180,
                    //    cellTemplate: cellToolTipTemplate
                    //},
                    {
                        name: app.localize('Region'),
                        field: 'region',
                        minWidth: 110,
                        cellTemplate: cellToolTipTemplate
                    },
                    //{
                    //    name: app.localize('LeadSource'),
                    //    field: 'leadSource',
                    //    minWidth: 120,  
                    //    cellTemplate: cellToolTipTemplate
                    //},
                    {
                        name: app.localize('Status'),
                        field: 'status',
                        minWidth: 90,
                        cellTemplate: cellToolTipTemplate
                    },
                    //{
                    //    name: app.localize('WebSite'),
                    //    field: 'webSite',
                    //    minWidth: 200,
                    //    cellTemplate: cellToolTipTemplate
                    //},
                    //{
                    //    name: app.localize('Fax'),
                    //    field: 'fax',
                    //    minWidth: 100,
                    //    cellTemplate: cellToolTipTemplate
                    //},
                    {
                        name: app.localize('ContributedBy'),
                        field: 'contributedName',
                        minWidth: 130,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('ManagedBy'),
                        field: 'managedBy',
                        minWidth: 140,
                        cellTemplate: cellToolTipTemplate
                    }
                    //{
                    //    name: app.localize('Address'),
                    //    field: 'address',
                    //    minWidth: 100,
                    //    cellTemplate: cellToolTipTemplate
                    //}
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.getCompany();
                    });

                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getCompany();
                    });
                },
                data: []
            };
            function rowTemplate() {
                return '<div <div ng-class=row.entity.status>' +
                           '  <div ng-if="row.entity.merge">{{row.entity.title}}</div>' +
                           '  <div ng-if="!row.entity.merge" ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader }"  ui-grid-cell></div>' +
                           '</div>';
            }

            vm.getCompany = function () {
                vm.loading = true;
                CompanyService.getCompany({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.CompanyGridOptions.totalItems = result.totalCount;
                    vm.CompanyGridOptions.data = result.items;

                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editCompany = function (Company) {
                openCompanyModal(Company.id);
            };

            vm.openCreateCompanyModal = function () {
                openCompanyModal(null);
            };

            function openCompanyModal(Companyid) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/AddressBook/Company/CreateOrEdit.cshtml',
                    controller: 'tenant.views.AddressBook.Company.CreateOrEdit as vm',
                    backdrop: 'static',
                    resolve: {
                        Companyid: function () {
                            return Companyid;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    vm.getCompany();
                });
            }


            vm.deleteCompany = function (Company) {
                abp.message.confirm(
                    app.localize('AreYouSureToDeleteCompany', Company.CompanyName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            CompanyService.deleteCompany({
                                id: Company.id
                            }).success(function () {
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                                vm.getCompany();
                            });
                        }
                    }
                );
            };

            vm.getCompany();

            vm.openCompanyModal = function (Company) {
                //alert(Company.id);
                var Companyid = Company.id;
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/AddressBook/Company/ViewCompanydetails.cshtml',
                    controller: 'tenant.views.AddressBook.Company.ViewCompanydetails as vm',
                    backdrop: 'static',
                    resolve: {
                        Companyid: function () {
                            return Companyid;
                        }
                    }
                });

            };
        }
    ]);
})();