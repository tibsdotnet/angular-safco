﻿(function () {
    appModule.controller('tenant.views.AddressBook.JunkCompany.index', [
        '$scope', '$modal', 'uiGridConstants', 'abp.services.app.junkCompany',
        function ($scope, $modal, uiGridConstants, CompanyService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                Metronic.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };
            vm.exportToExcel = function () {
                CompanyService.getJunkCompanyToExcel(vm.requestParams)
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };
            var cellToolTipTemplate =
               '<div class="ui-grid-cell-contents" tooltip = "{{COL_FIELD}}" tooltip-append-to-body="true" tooltip-popup-delay="200" >{{ COL_FIELD }}</div>';

            vm.getCompany = function () {
                CompanyService.getCompany({ filter: vm.filterText }).success(function (result) {
                    vm.countries = result.items;
                });
            }

            vm.CompanyGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 90,
                        cellTemplate:
                           
                            '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.editCompany(row.entity)"><i class="fa fa-pencil-square-o font-blue"></i></button>' +
                             //    '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.deleteCompany(row.entity)"><i class="fa fa-trash-o font-red"></i></button>' +
                                '</div>'
                    },
                    {
                        name: app.localize('CompanyName'),
                        field: 'companyName',
                        minWidth: 200,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('PhoneNumber'),
                        field: 'phoneNumber',
                        minWidth: 130,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('Region'),
                        field: 'region',
                        minWidth: 110,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('Status'),
                        field: 'status',
                        minWidth: 90,  
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('ContributedBy'),
                        field: 'contributedBy',
                        minWidth: 130,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('ManagedBy'),
                        field: 'managedBy',
                        minWidth: 140,
                        cellTemplate: cellToolTipTemplate
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.getCompany();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getCompany();
                    });
                },
                data: []
            };

            vm.getCompany = function () {
                vm.loading = true;
                CompanyService.getJunkCompany({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.CompanyGridOptions.totalItems = result.totalCount;
                    vm.CompanyGridOptions.data = result.items;

                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editCompany = function (Company) {
                openCompanyModal(Company.id);
            };

            vm.openCreateCompanyModal = function () {
                openCompanyModal(null);
            };

            function openCompanyModal(Companyid) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/AddressBook/Company/CreateOrEdit.cshtml',
                    controller: 'tenant.views.AddressBook.Company.CreateOrEdit as vm',
                    backdrop: 'static',
                    resolve: {
                        Companyid: function () {
                            return Companyid;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    alert('kjjj');
                    vm.getCompany();
                });
            }


            vm.deleteCompany = function (Company) {
                abp.message.confirm(
                    app.localize('AreYouSureToDeleteCompany', Company.CompanyName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            CompanyService.deleteCompany({
                                id: Company.id
                            }).success(function () {
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                                vm.getCompany();
                            });
                        }
                    }
                );
            };

            vm.getCompany();
        }
    ]);
})();