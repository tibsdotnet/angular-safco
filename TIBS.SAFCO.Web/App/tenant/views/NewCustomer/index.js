﻿(function () {
    appModule.controller('tenant.views.NewCustomer.index', [
        '$scope', '$modal', 'uiGridConstants', 'abp.services.app.customer',
        function ($scope, $modal, uiGridConstants, CompanyService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                Metronic.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };
            vm.exportToExcel = function () {
                CompanyService.getNewCustomerToExcel(vm.requestParams)
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };
            var cellToolTipTemplate =
               '<div class="ui-grid-cell-contents" tooltip = "{{COL_FIELD}}" tooltip-append-to-body="true" tooltip-popup-delay="200" >{{ COL_FIELD }}</div>';

            var todayAsString = moment().format('YYYY-MM-DD');
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: todayAsString,
                endDate: todayAsString
            };

            vm.CompanyGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                   
                    {
                        name: app.localize('CompanyName'),
                        field: 'companyName',
                        minWidth: 140,
                        cellTemplate: cellToolTipTemplate
                    },

                    {
                        name: app.localize('ManagedBy'),
                        field: 'managedBy',
                        minWidth: 120,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('ContributedBy'),
                        field: 'contributedBy',
                        minWidth: 120,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('OrderNumber'),
                        field: 'orderNumber',
                        minWidth: 150,
                        cellTemplate: cellToolTipTemplate
                    }, {
                        name: app.localize('Amount'),
                        field: 'amount',
                        minWidth: 150,
                        cellTemplate: cellToolTipTemplate
                    }, {
                        name: app.localize('ClosedDate'),
                        field: 'closedDate',
                        minWidth: 150,
                        cellTemplate: cellToolTipTemplate
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.change();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.change();
                    });
                },
                data: []
            };
            vm.requestParams = {
               
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.change = function () {
                vm.loading = true;
                CompanyService.getCustomers($.extend({}, vm.requestParams, vm.dateRangeModel)).success(function (result) {
                    vm.CompanyGridOptions.totalItems = result.totalCount;
                    vm.CompanyGridOptions.data = result.items;

                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.getCompany = function () {
                vm.loading = true;
                CompanyService.getCustomers($.extend({}, vm.requestParams, vm.dateRangeModel)).success(function (result) {
                    vm.CompanyGridOptions.totalItems = result.totalCount;
                    vm.CompanyGridOptions.data = result.items;

                }).finally(function () {
                    vm.loading = false;
                });
            };

            

           
           
            vm.getCompany();
        }
    ]);
})();