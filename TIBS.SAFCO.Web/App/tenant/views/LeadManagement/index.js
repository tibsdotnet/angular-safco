﻿(function () {
    appModule.controller('tenant.views.LeadManagement.index', [
        '$scope', '$modal', 'uiGridConstants', 'abp.services.app.lead','$location',
        function ($scope, $modal, uiGridConstants, leadService, $location) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                Metronic.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };
            vm.exportToExcel = function () {
                leadService.getLeadToExcel(vm.requestParams)
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };
            var cellToolTipTemplate =
                '<div class="ui-grid-cell-contents" tooltip = "{{COL_FIELD}}" tooltip-append-to-body="true" tooltip-popup-delay="200" >{{ COL_FIELD }}</div>';

            vm.getLead = function () {
                leadService.getCompany({ filter: vm.filterText }).success(function (result) {
                    vm.countries = result.items;
                });
            }

            vm.LeadGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: rowTemplate(),
               // rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 80,
                        cellTemplate:

                            '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.editLead(row.entity)"><i class="fa fa-pencil-square-o font-blue"></i></button>' +
                             //    '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.deleteCompany(row.entity)"><i class="fa fa-trash-o font-red"></i></button>' +
                                '</div>'
                    },
                    {
                        name: app.localize('CompanyName'),
                        field: 'companyName',
                        minWidth: 200,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('AllotedTo'),
                        field: 'allotedTo',
                        minWidth: 150,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('ContributedBy'),
                        field: 'contributedBy',
                        minWidth: 150,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('Status'),
                        field: 'status',
                        minWidth: 100,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('ActualAmount'),
                        field: 'actualAmount',
                        enableSorting: false,
                        minWidth: 150,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('ActualClosure'),
                        field: 'actualClosure',
                        minWidth: 150,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('ExpectedAmount'),
                        field: 'expAmount',
                        enableSorting: false,
                        minWidth: 150,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('ExpectedClosure'),
                        field: 'expClosure',
                        minWidth: 180,
                        cellTemplate: cellToolTipTemplate
                    }
                    
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.getLead();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getLead();
                    });
                },
                data: []
            };
            vm.editLead = function (lead) {

                $location.path('/leaddetails/' + lead.companyId+'/'+lead.id);
            };

            vm.getLead = function () {
                vm.loading = true;
                leadService.getLeads({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.LeadGridOptions.totalItems = result.totalCount;
                    vm.LeadGridOptions.data = result.items;

                }).finally(function () {
                    vm.loading = false;
                });
            };

          
            function rowTemplate() {
                return '<div <div ng-class=row.entity.status>' +
                           '  <div ng-if="row.entity.merge">{{row.entity.title}}</div>' +
                           '  <div ng-if="!row.entity.merge" ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader }"  ui-grid-cell></div>' +
                           '</div>';
            }

          

            vm.getLead();
        }
    ]);
})();