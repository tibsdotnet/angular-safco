﻿(function () {
    appModule.controller('tenant.views.LeadManagement.Contactdetails', [
        '$scope', '$modalInstance', 'abp.services.app.lead', 'contactId',
        function ($scope, $modalInstance, leadService, contactId) {
            var vm = this;
            vm.saving = false;
            //alert(contactId);
            vm.contact = [];

           
            function init() {
                leadService.getContactForEdit({ id: contactId }).success(function (result) {
                    
                    vm.contact = result.companyContact;
                    console.log(vm.contact);
                    vm.region = result.region;
                    for (i = 0; i < vm.region.length; i++) {
                        var da = vm.region[i].regionId;
                        if (da == vm.contact.regionId) {
                            vm.regionName = vm.region[i].regionName;

                        }
                    }

                });
            }

            init();

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();