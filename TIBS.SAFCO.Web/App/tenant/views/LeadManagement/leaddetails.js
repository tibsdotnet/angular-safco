﻿appModule.filter('propsFilter', function () {
    return function (items, props) {
        var out = [];

        if (angular.isArray(items)) {
            var keys = Object.keys(props);

            items.forEach(function (item) {
                var itemMatches = false;

                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});



(function () {
    appModule.controller('tenant.views.LeadManagement.leaddetails', [
        '$scope', '$modal', 'abp.services.app.company','abp.services.app.lead', 'uiGridConstants', '$state',
    function ($scope, $modal, companyService,leadService, uiGridConstants, $state) {
        var vm = this;
        vm.saving = false;
        vm.Company = [];
        var companyId = $scope.companyId;
       
        var companyName = null;
        // alert(companyId);
        //var cellToolTipTemplate =
        // '<div class="ui-grid-cell-contents" tooltip = "{{COL_FIELD}}" tooltip-append-to-body="true" tooltip-popup-delay="200" >{{ COL_FIELD }}</div>';
        vm.save = function () {
            vm.saving = true;
            vm.Company.leadSourceId = $scope.leadsource.selected.leadSourceId;
            vm.Company.statusId = $scope.status.selected.statusId;
            vm.Company.contributedId = $scope.contributedName.selected.userId;
            vm.Company.regionId = $scope.region.selected.regionId;
            //console.log(vm.Company);
            companyService.createOrUpdateCompany(vm.Company).success(function () {
                abp.notify.info(app.localize('SavedSuccessfully'));
                $modal.close();
            }).finally(function () {
                vm.saving = false;
            });
        };
        // alert("khk");
        function init() {
            leadService.getLeadEdit({
                id: companyId
            }).success(function (result) {

                vm.Company = result.company;
                if (vm.Company != null) {
                    companyName = result.company.title;

                }
                vm.lead = result.lead;
                console.log(vm.lead);

                vm.propec = false;
                vm.nego = false;
                vm.won = false;
                vm.lost = false;
                vm.junk = false;

                vm.statusId = result.lead.mileStoneId;
                vm.orderstatusId = result.lead.mileStoneId;

                if (vm.statusId == 3) {

                    vm.propec = true;
                } else if (vm.statusId == 4) {

                    vm.nego = true;
                } else if (vm.statusId == 5) {

                    vm.won = true;
                } else if (vm.statusId == 6) {
                    vm.lost = true;
                } else if (vm.statusId == 7) {

                    vm.junk = true;
                }

                vm.region = result.region;
                vm.LeadSource = result.leadSource;
                vm.Status = result.statusName;
                vm.ContributedName = result.userName;
                for (i = 0; i < vm.LeadSource.length; i++) {
                    var da = vm.LeadSource[i].leadSourceId;
                    if (da == vm.Company.leadSourceId) {
                        vm.leadsouce = vm.LeadSource[i].leadSourceName;
                        $scope.leadsource.selected = { leadSourceId: vm.LeadSource[i].leadSourceId, leadSourceName: vm.LeadSource[i].leadSourceName };
                    }
                }
                for (i = 0; i < vm.region.length; i++) {
                    var da = vm.region[i].regionId;
                    if (da == vm.Company.regionId) {
                        vm.regionName = vm.region[i].regionName;
                        $scope.region.selected = { regionId: vm.region[i].regionId, regionName: vm.region[i].regionName };
                    }
                }
                for (i = 0; i < vm.Status.length; i++) {
                    var da = vm.Status[i].statusId;
                    if (da == vm.Company.statusId) {
                       
                        vm.status = vm.Status[i].statusName;
                        $scope.status.selected = { statusId: vm.Status[i].regionId, statusName: vm.Status[i].statusName };
                    }
                }
                for (i = 0; i < vm.ContributedName.length; i++) {
                    var da = vm.ContributedName[i].userId;
                    if (da == vm.Company.contributedId) {
                        vm.contributed = vm.ContributedName[i].userName;
                        $scope.contributedName.selected = { userId: vm.ContributedName[i].userId, userName: vm.ContributedName[i].userName };
                    }
                }
            });
        }

        init();

        vm.saveCompetitor = function () {
            vm.saving = true;
            vm.lead.competitorId = $scope.competitor.selected.competitorId;
            leadService.saveCompetitor(vm.lead).success(function () {
                abp.notify.info(app.localize('SavedSuccessfully'));
            }).finally(function () {
                vm.saving = false;
            });
        };

        $scope.contributedName = {};
        $scope.status = {};
        $scope.region = {};
        $scope.leadsource = {};

        vm.cancel = function () {
            $modal.dismiss();
        };
        vm.ContactGridOptions = {
            enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
            enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
            paginationPageSizes: app.consts.grid.defaultPageSizes,
            paginationPageSize: app.consts.grid.defaultPageSize,
            useExternalPagination: true,
            useExternalSorting: true,
            appScopeProvider: vm,
            rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
            columnDefs: [
                {
                    name: app.localize('Actions'),
                    enableSorting: false,
                    width: 120,
                    cellTemplate:
                        //'<div class=\"ui-grid-cell-contents\">' +
                        //'  <div class="btn-group dropdown" dropdown="">' +
                        //'    <button class="btn btn-xs btn-primary blue" dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                        //'    <ul class="dropdown-menu">' +
                        //'      <li><a  ng-click="grid.appScope.editMainProjectSehedule(row.entity)">' + app.localize('Edit') + '</a></li>' +
                        //'      <li><a  ng-click="grid.appScope.deleteMainprojectschedule(row.entity)">' + app.localize('Delete') + '</a></li>' +
                        //'    </ul>' +
                        //'  </div>' +
                        //'</div>'

                        '<div class=\"ui-grid-cell-contents text-center\">' +
                            '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.editCompanyContact(row.entity)"><i class="fa fa-pencil-square-o font-blue"></i></button>' +
                             '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.deleteMainprojectschedule(row.entity)"><i class="fa fa-trash-o font-red"></i></button>' +
                            '</div>'
                },
               {
                   name: app.localize('CompanyName'),
                   field: 'companyName',
                   minWidth: 100
                   // cellTemplate: cellToolTipTemplate
               },
                {
                    name: app.localize('FirstName'),
                    field: 'firstName',
                    cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <img ng-if="row.entity.profilePicture" ng-src="' + abp.appPath + '{{row.entity.profilePicture}}" width="22" height="22" class="img-rounded img-profile-picture-in-grid" />' +
                        '  <img ng-if="!row.entity.profilePicture" src="' + abp.appPath + 'Common/Images/default-profile-picture.png" width="22" height="22" class="img-rounded" />' +
                        '  {{COL_FIELD CUSTOM_FILTERS}} &nbsp;' +
                        '</div>',
                    minWidth: 150
                    // cellTemplate: cellToolTipTemplate
                },
               {
                   name: app.localize('LastName'),
                   field: 'lastName',
                   minWidth: 120
                   // cellTemplate: cellToolTipTemplate
               },
               {
                   name: app.localize('Regionname'),
                   field: 'regionname',

                   minWidth: 150
               },
               {
                   name: app.localize('Address'),
                   field: 'address',
                   minWidth: 200
                   // cellTemplate: cellToolTipTemplate
               },
               {
                   name: app.localize('Description'),
                   field: 'description',
                   minWidth: 200
                   //   cellTemplate: cellToolTipTemplate
               },
               {
                   name: app.localize('Fax'),
                   field: 'fax',
                   minWidth: 100
                   //cellTemplate: cellToolTipTemplate
               },
               {
                   name: app.localize('POBox'),
                   field: 'poBox',
                   minWidth: 150
                   //cellTemplate: cellToolTipTemplate
               },
               {
                   name: app.localize('EmailId'),
                   field: 'emailId',
                   minWidth: 100
                   // cellTemplate: cellToolTipTemplate
               },


            ],
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
                $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                    if (!sortColumns.length || !sortColumns[0].field) {
                        requestParams.sorting = null;
                    } else {
                        requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                    }
                    vm.getMainProjectSchedules();
                });
                gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                    requestParams.skipCount = (pageNumber - 1) * pageSize;
                    requestParams.maxResultCount = pageSize;

                    vm.getContacts();
                });
            },
            data: []
        };
        
        vm.leadupdate = {};
        vm.updateStatus = function (sId) {
            if (vm.statusId <= sId) {
                $.ajax({
                    type: 'POST',
                    url: '/File/UpdateStatus',
                    data: { leadId: $scope.leadId, StatusId: sId },
                    success: function () {
                        vm.saving = false;
                        $modalInstance.close();
                    },
                    failure: function (response) {

                    }
                })

            }
        }

        vm.openContactModal = function (contact) {
            var contactId = contact.id;
            var modalInstance = $modal.open({
                templateUrl: '~/App/tenant/views/LeadManagement/Contactdetails.cshtml',
                controller: 'tenant.views.LeadManagement.Contactdetails as vm',
                backdrop: 'static',
                resolve: {
                    contactId: function () {
                        return contactId;
                    }
                }
            });

        };

        vm.getContacts = function () {
            vm.loading = true;
            companyService.getContacts({
                id: companyId
            }).success(function (result) {
                //vm.ContactGridOptions.totalItems = result.totalCount;
                //console.log(result.items);
                vm.contactdata = result.items;
                //vm.ContactGridOptions.data = result.items;

            }).finally(function () {
                vm.loading = false;
            });
        };
        vm.getContacts();
        $state.go('leaddetails.information');


        vm.lead = {};
        vm.updateStatus = function (sId) {

            if (vm.statusId <= sId) {
                if (((sId > 3 && (vm.lead.expAmount != 0 || vm.lead.empAmount !== undefined)) || sId <= 3)) {
                    vm.lead.id = $scope.leadId;
                    vm.lead.mileStoneId = sId;
                    leadService.updateStatus(vm.lead).success(function () { })
                } else {
                    abp.message.error('Please Enter Estimation Amount');
                }
            }
            vm.orderstatusId = sId;

        }
        var in10Days = new Date();
        in10Days.setDate(in10Days.getDate() + 10);

        this.dates = {
            date1: new Date(),
            date2: new Date(),
        };


        this.open = {
            date1: false,
            date2: false,

        };


        this.dateOptions = {
            showWeeks: false,
            startingDay: 1
        };

        this.timeOptions = {
            readonlyInput: false,
            showMeridian: true
        };

        this.dateModeOptions = {
            minMode: 'year',
            maxMode: 'year'
        };

        this.openCalendar = function (e, date) {

            vm.open[date] = true;


        };
        vm.lead = {};
        vm.saveEstimation = function () {
            vm.lead.expectedamount = vm.lead.expAmount;
            vm.lead.expectedClosure = vm.dates.date1;
            vm.lead.id = $scope.leadId;

            leadService.updateEsimation(vm.lead).success(function () { abp.notify.info(app.localize('SavedSuccessfully')); });




        };
    }
    



    ]);
})();