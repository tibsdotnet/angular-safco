﻿(function () {
    appModule.controller('tenant.views.LeadManagement.Activities', [
        '$scope', '$modal', 'uiGridConstants', 'abp.services.app.lead', '$state',
        function ($scope, $modal, uiGridConstants, leadService, $state) {
            var vm = this;
            vm.activity = [];
            var leadId = $scope.leadId;
            vm.activitycreate = {};
            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            init = function () {
                leadService.getActivities({ id: $scope.leadId }).success(function (result) {
                    vm.activity = result;
                    console.log(vm.activity);
                })
            };
            vm.tabid = 0;
            vm.name = "Introduction Call";
            vm.tabs = function (name) {
                vm.name = name;
                if (vm.name == 'Schedule') {
                    vm.tabid = 6;
                } else {
                    vm.tabid = 0;
                }
            };


            vm.saveInmail = function () {
                if (vm.name == "Comments") {
                    if (vm.activitycreate.notes === undefined || vm.activitycreate.notes == "" || vm.activitycreate.notes == null) {
                        abp.notify.info(app.localize('ValidComment'));
                    }
                    else {
                        vm.activitycreate.title = vm.name;
                        vm.activitycreate.leadId = $scope.leadId;
                        console.log(vm.activitycreate);
                        leadService.createActivityInput(vm.activitycreate).success(function () {
                            abp.notify.info(app.localize('SavedSuccessfully'));
                            vm.activitycreate.notes = "";
                            init();
                        })
                    }
                }
                else {
                    vm.activitycreate.title = vm.name;
                    vm.activitycreate.leadId = $scope.leadId;
                    leadService.createActivityInput(vm.activitycreate).success(function () {
                        abp.notify.info(app.localize('SavedSuccessfully'));
                        vm.activitycreate.notes = "";
                        init();
                    })
                }
            };
            vm.leadScheduleGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                            //'<div class=\"ui-grid-cell-contents\">' +
                            //'  <div class="btn-group dropdown" dropdown="">' +
                            //'    <button class="btn btn-xs btn-primary blue" dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            //'    <ul class="dropdown-menu">' +
                            //'      <li><a  ng-click="grid.appScope.editleadsSehedule(row.entity)">' + app.localize('Edit') + '</a></li>' +
                            //'      <li><a  ng-click="grid.appScope.deleteleadsschedule(row.entity)">' + app.localize('Delete') + '</a></li>' +
                            //'    </ul>' +
                            //'  </div>' +
                            //'</div>'

                            '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.editleadsSehedule(row.entity)"><i class="fa fa-pencil-square-o font-blue"></i></button>' +
                                 '  <button class="btn btn-default btn-xs" ng-click="grid.appScope.deleteleadsschedule(row.entity)"><i class="fa fa-trash-o font-red"></i></button>' +
                                '</div>'
                    },
                   {
                       name: app.localize('Title'),
                       field: 'title',
                       minWidth: 100,
                       cellTemplate: cellToolTipTemplate
                   },
                   {
                       name: app.localize('ScheduledDate'),
                       field: 'scheduleDate',
                       minWidth: 150,
                       cellTemplate: cellToolTipTemplate
                   },

                   {
                       name: app.localize('Emaill'),
                       field: 'email',
                       minWidth: 100,
                       cellTemplate: cellToolTipTemplate
                   },
                   {
                       name: app.localize('ContactNo'),
                       field: 'contactNo',
                       minWidth: 150,
                       cellTemplate: cellToolTipTemplate
                   },
                   {
                       name: app.localize('CompanyName'),
                       field: 'companyName',
                       minWidth: 100,
                       cellTemplate: cellToolTipTemplate
                   },
                   {
                       name: app.localize('Remind'),
                       field: 'remind',
                       minWidth: 100,
                       cellTemplate: cellToolTipTemplate
                   }

                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.getleadsSchedules();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getleadsSchedules();
                    });
                },
                data: []
            };
            vm.editleadsSehedule = function (schedule) {
                $state.go('leaddetails.createschedule', { scheduleId: schedule.id });

            }
            vm.openScheduleModal = function () {
                $state.go('leaddetails.createschedule', { scheduleId: null });
            };
            vm.deleteleadsschedule = function (schedule) {
                abp.message.confirm(
                    app.localize('AreYouSureToDeleteSchedule'),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            leadService.deletetSchedule({
                                id: schedule.id
                            }).success(function () {
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                                vm.getleadsSchedules();
                            });
                        }
                    }
                );
            };

            vm.getleadsSchedules = function () {
                //vm.loading = true;
                leadService.getleadschedules({
                    leadId: $scope.leadId,
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {

                    // console.log(result);
                    vm.leadScheduleGridOptions.totalItems = result.totalCount;
                    vm.leadScheduleGridOptions.data = result.items;
                }).finally(function () {
                    //vm.loading = false;
                });
            };
            var cellToolTipTemplate =
                '<div class="ui-grid-cell-contents" tooltip = "{{COL_FIELD}}" tooltip-append-to-body="true" tooltip-popup-delay="200" >{{ COL_FIELD }}</div>';


            init();
            vm.getleadsSchedules();

        }
    ])
})();