﻿(function () {
    appModule.controller('tenant.views.Activity.index', [
        '$scope', '$modal', 'abp.services.app.activity',
        function ($scope, $modal, ActivityService) {
            var vm = this;

            vm.Activity = [];
            vm.filterText = '';

            $scope.$on('$viewContentLoaded', function () {
                Metronic.initAjax();
            });

            vm.getActivity = function () {
                ActivityService.getActivity({ filter: vm.filterText }).success(function (result) {
                    vm.Activity = result.items;
                });
            }

            vm.openEditActivityModal = function (Activity) {
                openActivityModal(Activity.id);
            };

            vm.openCreateActivityModal = function () {
                openActivityModal(null);
            };
            vm.exportToExcel = function () {
                ActivityService.getActivityToExcel(vm.requestParams)
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };
            vm.permissions = {
                createActivity: abp.auth.hasPermission('Pages.Tenant.Review.Activity.CreateActivity'),
                editActivity: abp.auth.hasPermission('Pages.Tenant.Review.Activity.EditActivity'),
                deleteActivity: abp.auth.hasPermission('Pages.Tenant.Review.Activity.DeleteActivity')
            };

            function openActivityModal(Activityid) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/Activity/createActivityModal.cshtml',
                    controller: 'tenant.views.Activity.createActivityModal as vm',
                    backdrop: 'static',
                    resolve: {
                        Activityid: function () {
                            return Activityid;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    vm.getActivity();
                });
            }

            vm.deleteActivity = function (Activity) {
                abp.message.confirm(
                    app.localize('AreYouSureToDeleteActivity', Activity.ActivityName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            ActivityService.deleteActivity({
                                id: Activity.id
                            }).success(function () {
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                                vm.getActivity();
                            });
                        }
                    }
                );
            };

            vm.getActivity();
        }
    ]);
})();