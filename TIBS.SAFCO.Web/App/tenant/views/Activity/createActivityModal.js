﻿(function () {
    appModule.controller('tenant.views.Activity.createActivityModal', [
        '$scope', '$modalInstance', 'abp.services.app.activity','Activityid',
        function ($scope, $modalInstance, ActivityService, Activityid) {
            var vm = this;
            vm.saving = false;
            vm.Activity = null;

            vm.save = function () {
                vm.saving = true;
                ActivityService.createOrUpdateUser(vm.Activity).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };
           // alert(Activityid);
            function init() {
                ActivityService.getActivityForEdit({
                    id: Activityid
                }).success(function (result) {
                    //console.log(result.Activity);
                    vm.Activity = result.activity;
                    console.log(vm.Activity);
                });
            }

            init();

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();