﻿(function () {
    appModule.controller('tenant.views.Product.ProductClassification.index', [
        '$scope', '$modal', 'abp.services.app.productClassification',
        function ($scope, $modal, ProductClassificationService) {
            var vm = this;

            vm.ProductClassification = [];
            vm.filterText = '';

            $scope.$on('$viewContentLoaded', function () {
                Metronic.initAjax();
            });

            vm.getProductClassification = function () {
                ProductClassificationService.getProductClassification({ filter: vm.filterText }).success(function (result) {
                    vm.ProductClassification = result.items;
                });
            }

            vm.openEditProductClassificationModal = function (ProductClassification) {
                openProductClassificationModal(ProductClassification.id);
            };

            vm.openCreateProductClassificationModal = function () {
                openProductClassificationModal(null);
            };
            vm.exportToExcel = function () {
                ProductClassificationService.getProductClassificationToExcel(vm.requestParams)
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };
            vm.permissions = {
                createProductClassification: abp.auth.hasPermission('Pages.Tenant.Product.ProductClassification.CreateNewProductClassification'),
                editProductClassification: abp.auth.hasPermission('Pages.Tenant.Product.ProductClassification.EditProductClassification'),
                deleteProductClassification: abp.auth.hasPermission('PagesTenant.Product.ProductClassification.DeleteProductClassification')
            };

            function openProductClassificationModal(ProductClassificationid) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/Product/ProductClassification/createProductClassificationModal.cshtml',
                    controller: 'tenant.views.Product.ProductClassification.createProductClassificationModal as vm',
                    backdrop: 'static',
                    resolve: {
                        ProductClassificationid: function () {
                            return ProductClassificationid;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    vm.getProductClassification();
                });
            }

            vm.deleteProductClassification = function (ProductClassification) {
                abp.message.confirm(
                    app.localize('AreYouSureToDeleteProductClassification', ProductClassification.ProductClassificationName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            ProductClassificationService.deleteProductClassification({
                                id: ProductClassification.id
                            }).success(function () {
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                                vm.getProductClassification();
                            });
                        }
                    }
                );
            };

            vm.getProductClassification();
        }
    ]);
})();