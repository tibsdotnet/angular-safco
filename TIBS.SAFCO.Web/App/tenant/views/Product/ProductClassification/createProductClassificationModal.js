﻿(function () {
    appModule.controller('tenant.views.Product.ProductClassification.createProductClassificationModal', [
        '$scope', '$modalInstance', 'abp.services.app.productClassification','ProductClassificationid',
        function ($scope, $modalInstance, ProductClassificationService, ProductClassificationid) {
            var vm = this;
            vm.saving = false;
            vm.ProductClassification = null;

            vm.save = function () {
                vm.saving = true;
                ProductClassificationService.createOrUpdateUser(vm.ProductClassification).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };
           // alert(ProductClassificationid);
            function init() {
                ProductClassificationService.getProductClassificationForEdit({
                    id: ProductClassificationid
                }).success(function (result) {
                    //console.log(result.ProductClassification);
                    vm.ProductClassification = result.productClassification;
                    console.log(vm.ProductClassification);
                });
            }

            init();

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();