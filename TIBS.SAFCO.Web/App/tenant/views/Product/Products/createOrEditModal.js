﻿(function () {
    appModule.controller('tenant.views.Product.Products.createOrEditModal', [
        '$scope', '$modalInstance', 'abp.services.app.product', 'Productsid',
        function ($scope, $modalInstance, productService, Productid) {
            var vm = this;
            vm.saving = false;
            vm.product = null;
            vm.productClassification = [];

            vm.save = function () {
                //var assignedproductClassificationNames = _.map(
                //    _.where(vm.productClassification, { isAssigned: true }), //Filter assigned roles
                //    function (ProductClassification) {
                //        vm.product.productClassificationId = ProductClassification.productClassificationId;
                //        return ProductClassification.productClassificationName; //Get names
                //    });
                vm.saving = true;
                vm.product.productClassificationId = $scope.productClassification.selected.productClassificationId;
                productService.createOrEditProduct(vm.product).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            //vm.getAssignedProductClassificationCount = function () {
            //    return _.where(vm.productClassification, { isAssigned: true }).length;
            //};

            //$scope.productClassification= {};
            //$scope.refreshProductClassifications = function (ProductClassification) {
            //    var params = { ProductClassification: ProductClassification, sensor: false };
            //    select2Product.ProductClassification({ name: params.ProductClassification }).success(function (result) {
            //        $scope.ProductClassifications = result.select2data;
            //    });
            //};

            function init() {
                
                productService.getProductForEdit({ 
                    id: Productid
                }).success(function (result) {
                    vm.product = result.product;
                    vm.productClassification = result.productClassificationName;
                    for (i = 0; i < vm.productClassification.length; i++) {
                        var da = vm.productClassification[i].productClassificationId;
                        if (da == vm.product.productClassificationId) {
                            $scope.productClassification.selected = { productClassificationId: vm.productClassification[i].productClassificationId, productClassificationName: vm.productClassification[i].productClassificationName };
                        }
                    }
                    
                });
            }

            init();

            $scope.productClassification= {};
        }
    ]);
})();