﻿(function () {
    appModule.controller('tenant.views.Product.Products.index', [
        '$scope', '$modal', 'uiGridConstants', 'abp.services.app.product',
        function ($scope, $modal, uiGridConstants, ProductsDetail) {
            var vm = this;
           

            $scope.$on('$viewContentLoaded', function () {
                Metronic.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };
            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Tenant.Product.Products.CreateProducts'),
                'edit': abp.auth.hasPermission('Pages.Tenant.Product.Products.EditProducts'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Product.Products.DeleteProducts')
            };

            vm.exportToExcels = function () {
                ProductsDetail.getProductToExcel(vm.requestParams)
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            var cellToolTipTemplate =
               '<div class="ui-grid-cell-contents" tooltip = "{{COL_FIELD}}" tooltip-append-to-body="true" tooltip-popup-delay="200" >{{ COL_FIELD }}</div>';

            vm.ProductsGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                            //'<div class=\"ui-grid-cell-contents\">' +
                            //'  <div class="btn-group dropdown" dropdown="">' +
                            //'    <button class="btn btn-xs btn-primary blue" dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            //'    <ul class="dropdown-menu">' +
                            //'      <li><a ng-click="grid.appScope.editProducts(row.entity)">' + app.localize('Edit') + '</a></li>' +
                            //'      <li><a ng-click="grid.appScope.deleteProducts(row.entity)">' + app.localize('Delete') + '</a></li>' +
                            //'    </ul>' +
                            //'  </div>' +
                            //'</div>'

                            '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button class="btn btn-default btn-xs" ng-if="grid.appScope.permissions.edit" ng-click="grid.appScope.editProducts(row.entity)"><i class="fa fa-pencil-square-o font-blue"></i></button>' +
                                 '  <button class="btn btn-default btn-xs" ng-if="grid.appScope.permissions.delete" ng-click="grid.appScope.deleteProducts(row.entity)"><i class="fa fa-trash-o font-red"></i></button>' +
                                '</div>'
                    },
                    {
                        name: app.localize('ProductsName'),
                        field: 'productName',
                        minWidth: 120,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('Description'),
                        field: 'description',
                        minWidth: 120,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('ProductClassification'),
                        field: 'productClassificationName',
                        minWidth: 190,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('MinimumSalePrice'),
                        field: 'minimumSalePrice',
                        enableSorting: false,
                        minWidth: 160,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('MaximumSalePrice'),
                        field: 'maximumSalePrice',
                        enableSorting: false,
                        minWidth: 180,
                        cellTemplate: cellToolTipTemplate
                    }
                    
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.getProducts();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getProducts();
                    });
                },
                data: []
            };

            vm.getProducts = function () {
               
                vm.loading = true;
                ProductsDetail.getProduct({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.ProductsGridOptions.totalItems = result.totalCount;
                    vm.ProductsGridOptions.data = result.items;
                    
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.openCreateProductsModal = function () {
                openProductsModal(null);
            };

            vm.editProducts = function (Products) {
                openProductsModal(Products.id);
            };

            function openProductsModal(Productsid) {
                
                var modalInstance = $modal.open({
                   
                    templateUrl: '~/App/tenant/views/Product/Products/createOrEditModal.cshtml',
                    controller: 'tenant.views.Product.Products.createOrEditModal as vm',
                    backdrop: 'static',
                    resolve: {
                        Productsid: function () {
                            return Productsid;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    vm.getProducts();
                });
            }

            vm.exportToExcel = function () {

                ProductsDetail.getProductsToExcel(vm.requestParams)
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.deleteProducts = function (Products) {
                abp.message.confirm(
                    app.localize('AreYouSureToDelete Product',+ Products.ProductsName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            ProductsDetail.deleteProduct({
                                id: Products.id
                            }).success(function () {
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                                vm.getProducts();
                            });
                        }
                    }
                );
            };

            vm.getProducts();
        }
    ]);
})();