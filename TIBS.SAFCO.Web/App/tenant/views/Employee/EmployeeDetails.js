﻿appModule.filter('propsFilter', function () {
    return function (items, props) {
        var out = [];

        if (angular.isArray(items)) {
            var keys = Object.keys(props);

            items.forEach(function (item) {
                var itemMatches = false;

                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});


appModule.controller('tenant.views.Employee.EmployeeDetails',
     [
        '$scope', '$modal', 'abp.services.app.employee','FileUploader','$location','$state',
       function ($scope, $modal, employeeService, fileUploader, $location, $state) {
            var personid = $scope.personid;
          //  alert(personid);
        var vm = this;
        vm.saving = false;
        vm.profilePictureId = null;
           
        vm.permissions = {
            employeeedit: abp.auth.hasPermission('Pages.Administration.Users.Edit')
            };
       
        //vm.getpicture = function () {
        //    employeeService.getProfile({
        //        id: personid
        //    }).success(function (result) {
        //        vm.profilePicture = result;
        //    });
        //}
        init = function () {
            employeeService.getEmployeeFofEdit({
                id: personid
            }).success(function (result) {
                console.log(result);
                vm.employee = result.employee;
                vm.region = result.region;
                vm.role = result.role;
                vm.TitleOfcourtesy = vm.employee.titleOfCourtesy;
                vm.dates.date1 = vm.employee.birthDate;
                vm.dates.date2 = vm.employee.hireDate;
                //for (i = 0; i < vm.country.length; i++) {
                //    var da = vm.country[i].countryId;
                //    if (da == vm.employee.countryId) {

                //        $scope.country.selected = { countryId: vm.country[i].countryId, countryName: vm.country[i].countryName }

                //    }
                //}
                //for (i = 0; i < vm.city.length; i++) {
                //    var da = vm.city[i].cityId;
                //    if (da == vm.employee.cityId) {

                //        $scope.city.selected = { cityId: vm.city[i].cityId, cityName: vm.city[i].cityName }

                //    }
                //}
                //for (i = 0; i < vm.location.length; i++) {
                //    //alert("huyhu");
                //    var da = vm.location[i].locationId;
                //    if (da == vm.employee.locationId) {

                //        $scope.location.selected = { locationId: vm.location[i].locationId, locationName: vm.location[i].locationName }

                //    }
                //}
                for (i = 0; i < vm.region.length; i++) {
                    //alert("huyhu");
                    var da = vm.region[i].regionId;
                    if (da == vm.employee.regionId) {


                        $scope.region.selected = { regionId: vm.region[i].regionId, regionName: vm.region[i].regionName }

                    }
                }
                for (i = 0; i < vm.role.length; i++) {
                    //alert("huyhu");
                    var da = vm.role[i].roleId;
                    if (da == vm.employee.roleId) {


                        $scope.role.selected = { roleId: vm.role[i].roleId, roleDisplayName: vm.role[i].roleDisplayName }
                        vm.desiginationName = vm.role[i].roleDisplayName;

                    }
                }
                //for (i = 0; i < vm.religion.length; i++) {

                //    var da = vm.religion[i].religionId;

                //    if (da == vm.employee.religionId) {


                //        $scope.religion.selected = { religionId: vm.religion[i].religionId, religionName: vm.religion[i].religionName }

                //    }
                //}
                //for (i = 0; i < vm.educationLevel.length; i++) {

                //    var da = vm.educationLevel[i].educationLevelId;
                //    if (da == vm.employee.educationLevelId) {


                //        $scope.educationLevel.selected = { educationLevelId: vm.educationLevel[i].educationLevelId, educationLevelName: vm.educationLevel[i].educationLevelName }

                //    }
                //}
                //for (i = 0; i < vm.desigination.length; i++) {

                //    var da = vm.desigination[i].desginationId;

                //    if (da == vm.employee.desiginationId) {


                //        vm.desiginationName = vm.desigination[i].desiginationName;
                //        $scope.desigination.selected = { desiginationId: vm.desigination[i].desiginationId, desiginationName: vm.desigination[i].desiginationName }



                //    }
                //}
                //for (i = 0; i < vm.department.length; i++) {

                //    var da = vm.department[i].departmentId;
                //    if (da == vm.employee.departmentId) {


                //        $scope.department.selected = { departmentId: vm.department[i].departmentId, departmentName: vm.department[i].departmentName }

                //    }
                //}
                //for (i = 0; i < vm.gradeType.length; i++) {

                //    var da = vm.gradeType[i].gradeId;

                //    if (da == vm.employee.gradeId) {


                //        $scope.grade.selected = { gradeId: vm.gradeType[i].gradeId, gradeType: vm.gradeType[i].gradeType }

                //    }
                //}
                //for (i = 0; i < vm.status.length; i++) {

                //    var da = vm.status[i].statusId;
                //    if (da == vm.employee.statusId) {


                //        $scope.status.selected = { statusId: vm.status[i].statusId, statusName: vm.status[i].statusName }

                //    }
                //}


            });
        }
        var employeeid = $scope.employeeid;
        vm.save = function () {
            vm.saving = true;
            vm.employee.birthDate = vm.dates.date1;
            vm.employee.hireDate = vm.dates.date2;
            if ($scope.role.selected !== undefined) {
                vm.employee.roleId = $scope.role.selected.roleId;

            }
            if ($scope.region.selected !== undefined) {
                vm.employee.regionId = $scope.region.selected.regionId;

            }
            vm.employee.titleOfCourtesy = vm.TitleOfcourtesy;
            employeeService.createOrUpdateEmployee(vm.employee).success(function () {
                abp.notify.info(app.localize('SavedSuccessfully'));
                $location.path('/employee');
                //$modalInstance.close();
            }).finally(function () {
                vm.saving = false;
            });
        };
            vm.uploader = new fileUploader({
              
                url: abp.appPath + 'FileUpload/UploadEmployeeProfileImage',
                data: ({ id: personid, accountId: 2 }),
                queueLimit: 1
                
                
            });
            vm.uploader.onSuccessItem = function (fileItem, response, status, headers) {
                var profilepicuresrc;
                abp.notify.info(app.localize('SavedSuccessfully'));
                employeeService.getProfile({
                    id: personid
                }).success(function (result) {
                    var profileFilePath = result;
                    $('#EmployeeProfile').attr('src', profileFilePath);

                });
                
            };

            vm.saveProfilePicture = function () {
                //alert("saveProfilePicture");
                $.ajax({


                    type: 'POST',
                    url: '/FileUpload/GetInput',
                    data: { id:personid },
                    success: function () {

                        vm.uploader.uploadAll();

                    },
                    failure: function (response) {
                        $('#result').html(response);

                    }
                });
                
               
              
            };
            var in10Days = new Date();
            in10Days.setDate(in10Days.getDate() + 10);

            this.dates = {
                date1: new Date(),
                date2: new Date(),
            };


            this.open = {
                date1: false,
                date2: false,

            };

            // Disable weekend selection
            //this.disabled = function (date, mode) {
            //    return (mode === 'day' && (new Date().toDateString() == date.toDateString()));
            //};

            this.dateOptions = {
                showWeeks: false,
                startingDay: 1
            };

            this.timeOptions = {
                readonlyInput: false,
                showMeridian: true
            };

            this.dateModeOptions = {
                minMode: 'year',
                maxMode: 'year'
            };

            this.openCalendar = function (e, date) {

                vm.open[date] = true;


            };
           
            vm.cancel = function () {
                $location.path('/employee');
            };

            $scope.region = {};
            $scope.role = {};
            init();
            $state.go('Employeedetails.information');
           
            
 }]);
