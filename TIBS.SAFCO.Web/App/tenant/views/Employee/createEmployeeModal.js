﻿(function () {
    appModule.controller('tenant.views.Employee.createemployeeModal', [
        '$scope', '$modal', 'abp.services.app.employee','$location',
        function ($scope, $modal, employeeService, $location) {
            var vm = this;
            vm.saving = false;
            vm.employee = null;
            var employeeid = $scope.employeeid;
            vm.save = function () {
                vm.saving = true;
                vm.employee.birthDate = vm.dates.date1;
                vm.employee.hireDate = vm.dates.date2;
                vm.employee.titleOfCourtesy = vm.TitleOfcourtesy;
                if ($scope.region.selected !== undefined) {
                    vm.employee.regionId = $scope.region.selected.regionId;

                }
                employeeService.createOrUpdateEmployee(vm.employee).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $location.path('/employee');
                    //$modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            function init() {
                employeeService.getEmployeeFofEdit({
                    id: employeeid
                }).success(function (result) {
                    //console.log(result.employee);
                    vm.employee = result.employee;
                    vm.region = result.region;
                    for (i = 0; i < vm.region.length; i++) {
                        var da = vm.region[i].regionId;
                        if (da == vm.Company.regionId) {
                            $scope.region.selected = { regionId: vm.region[i].regionId, regionName: vm.region[i].regionName };
                        }
                    }
                });
            }

            init();

            vm.cancel = function () {
                $location.path('/employee');
            };
            var in10Days = new Date();
            in10Days.setDate(in10Days.getDate() + 10);

            this.dates = {
                date1: new Date(),
                date2: new Date(),
            };


            this.open = {
                date1: false,
                date2: false,

            };

            // Disable weekend selection
            //this.disabled = function (date, mode) {
            //    return (mode === 'day' && (new Date().toDateString() == date.toDateString()));
            //};

            this.dateOptions = {
                showWeeks: false,
                startingDay: 1
            };

            this.timeOptions = {
                readonlyInput: false,
                showMeridian: true
            };

            this.dateModeOptions = {
                minMode: 'year',
                maxMode: 'year'
            };

            this.openCalendar = function (e, date) {

                vm.open[date] = true;


            };
            $scope.region = {};
        }
    ]);
})();