﻿(function () {
    appModule.controller('tenant.views.Notieceboard.index', [
        '$scope', '$modal', 'abp.services.app.noticeBoard',
        function ($scope, $modal, noticeBoardService) {
            var vm = this;
            
            $scope.options = {
                animate: {
                    duration: 5000,
                    enabled: true
                },
                barColor: '#EF3F3F',
                scaleColor: '#353535',
                lineWidth: 5,
                lineCap: 'circle'
              
            };

            $scope.options1 = {
                animate: {
                    duration: 5000,
                    enabled: true
                },
                barColor: '#EF3F3F',
                scaleColor: '#353535',
                lineWidth: 3,
                lineCap: 'circle'
            };

           // $scope.percent2 = 20;
            $scope.options2 = {
                animate: {
                    duration: 5000,
                    enabled: true
                },
                barColor: '#EF3F3F',
                scaleColor: '#353535',
                lineWidth: 5,
                lineCap: 'circle'
            };

           // $scope.percent3 = 70;
            $scope.options3 = {
                animate: {
                    duration: 5000,
                    enabled: true
                },
                barColor: '#EF3F3F',
                scaleColor: '#353535',
                lineWidth: 5,
                lineCap: 'circle'
            };
            vm.yesterdayactivity = [];
            vm.activity = [];
            vm.init = function () {
                vm.activity = null;
                noticeBoardService.getSchedules().success(function (result) {
                    // vm.yesterdayactivity = result.yesterdayActivity;
                    vm.icon = "fa fa-bell-o";
                    vm.activity = result;
                   // console.log(vm.schdules);
                    //console.log(vm.yesterdayactivity);

                });
            };
            vm.getUser = function () {
                vm.activity = null;
                
                noticeBoardService.getUsers().success(function (result)
                {
                    vm.icon = "fa fa-user";
                    vm.activity = result;

                 })
            }
            vm.getorder = function () {
                vm.activity = null;

                noticeBoardService.getOrders().success(function (result) {
                    vm.icon = "fa fa-cart-plus";
                    vm.activity = result;

                })
                vm.monthactivity = function () {
                    vm.activity = null;

                    noticeBoardService.getMonthActivities().success(function (result) {
                        vm.icon = "fa fa-envelope-o";
                        vm.activity = result;

                    })
                };
            };
            vm.getinit = function () {
                noticeBoardService.getNoticeBoards().success(function (result) {
                   // console.log(result);
                   
                    $scope.Presentpercent = result.presentpercent;
                     //vm.yesterdayactivity = result.yesterdayActivity;
                    //alert($scope.Presentpercent);
                    $scope.LastMonthPrescent = result.lastmonths;
                    vm.totalcustomerformonth = result.presentmonth;
                    $scope.LastSixmonthPrescent = result.sixpercent;
                    $scope.totalcustomer = result.presentmonth;
                    vm.presentmonthamt = result.presentAmt;
                    vm.lastmonthAmt = result.lastAmt;
                    vm.sixmonthAmt = result.sixamt;
                    vm.LeadContributed = result.leadContributed;
                    //alert($scope.totalcustomer);
                });

            };
            vm.announcements = [];
            vm.getannouncement = function () {
                noticeBoardService.getAnnouncements().success(function (result) {
                    vm.announcements = result;
                })
            };
            vm.getActivities = function () {
                noticeBoardService.getActivities().success(function (result) {
                    vm.yesterdayactivity = result;
                })
            };
            vm.getActivities();
            vm.getinit();
            vm.init();
            vm.getannouncement();
        }


    ]);
})();