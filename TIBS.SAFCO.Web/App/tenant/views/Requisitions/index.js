﻿(function () {
    appModule.controller('tenant.views.Requisitions.index', [
        '$scope', '$modal', 'uiGridConstants', 'abp.services.app.requistion',
        function ($scope, $modal, uiGridConstants, requistionService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                Metronic.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            var cellToolTipTemplate =
               '<div class="ui-grid-cell-contents {{COL_FIELD}}" tooltip = "{{COL_FIELD}}" tooltip-append-to-body="true" tooltip-popup-delay="200" >{{ COL_FIELD }}</div>';

            vm.RequistionsGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 190,
                        cellTemplate:

                            '<div class=\"ui-grid-cell-contents text-center\" style="padding-top: 0px;padding-bottom: 0px;">' +
                                 //' <div class="ui-grid-cell-contents" style="padding-top: 3px;display: inline-block;" ng-if="row.entity.enabled==true" tooltip = "Pending" tooltip-append-to-body="true" tooltip-popup-delay="200" ><button class="btn btn-default btn-xs" ng-click="grid.appScope.pending(row.entity)" ng-if="row.entity.enabled==true" ng-disabled="row.entity.disable1"><i class="fa fa-thumbs-down font-black"></i></button></div>' +
                                 ' <div class="ui-grid-cell-contents" style="padding-top: 3px;display: inline-block;" tooltip = "Allot" tooltip-append-to-body="true" tooltip-popup-delay="200" ><button class="btn btn-default btn-xs" ng-click="grid.appScope.allot(row.entity)"><i class="fa fa-hand-o-right font-blue"></i></button></div>' +
                                 ' <div class="ui-grid-cell-contents" style="padding-top: 3px;display: inline-block;" ng-if="row.entity.enabled==true" tooltip = "Approve" tooltip-append-to-body="true" tooltip-popup-delay="200" ><button class="btn btn-default btn-xs" ng-click="grid.appScope.approve(row.entity)" ng-if="row.entity.enabled==true"><i class="fa fa-thumbs-up font-green"></i></button></div>' +
                                 ' <div class="ui-grid-cell-contents" style="padding-top: 3px;display: inline-block;" ng-if="row.entity.enabled==true" tooltip = "Reject" tooltip-append-to-body="true" tooltip-popup-delay="200" ><button class="btn btn-default btn-xs" ng-click="grid.appScope.reject(row.entity)" ng-if="row.entity.enabled==true" ng-disabled="row.entity.disable2"><i class="fa fa-hand-scissors-o font-red"></i></button></div>' +
                                '</div>'
                    },
                   {
                       name: app.localize('Lead'),
                       field: 'lead',
                       minWidth: 200,
                       cellTemplate: cellToolTipTemplate
                   },
                    {
                        name: app.localize('Regionn'),
                        field: 'region',
                        minWidth: 100,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('Contributedby'),
                        field: 'contributedBy',
                        minWidth: 140,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('Requestby'),
                        field: 'requestedBy',
                        minWidth: 140,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('Allottedby'),
                        field: 'allotedTo',
                        minWidth: 100,
                        cellTemplate: cellToolTipTemplate
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.getRole();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getRole();
                    });
                },
                data: []
            };

            vm.exportToExcel = function () {
                requistionService.getRequistionsToExcel(vm.requestParams)
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.pending = function (data) {
                requistionService.leadRequestPending(data.id).success(function () {
                    abp.notify.info(app.localize('requestmsg'));
                }).finally(function () {
                    vm.getRole();
                });
            };

            vm.approve = function (data) {
                requistionService.leadRequestApproved(data.id).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $.ajax({
                        type: 'POST',
                        url: '/File/Update',
                        data: { companyid: data.company_Id, employeeid: data.requestedBy_Employees_ID, id: 0 },
                        success: function () {
                            abp.notify.info(app.localize('requestmsg2'));
                            vm.getRole();
                        },
                        failure: function (response) {
                        }
                    });
                }).finally(function () {
                });
            };

            vm.reject = function (data) {
                requistionService.leadRequestReject(data.id).success(function () {
                    abp.notify.info(app.localize('requestmsg1'));
                }).finally(function () {
                    vm.getRole();
                });
            };

            vm.Requistions1GridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 80,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents text-center\" style="padding-top: 0px;padding-bottom: 0px;">' +
                                '<div class="ui-grid-cell-contents" style="padding-top: 3px;display: inline-block;" ng-if="row.entity.enabled==true" tooltip = "Make Request" tooltip-append-to-body="true" tooltip-popup-delay="200" ><button class="btn btn-default btn-xs" ng-click="grid.appScope.Request(row.entity)" ng-if="row.entity.enabled==true"><i class="fa fa-sign-in font-red"></i></button></div>' +
                                '</div>'
                    },
                     {
                         name: app.localize('title'),
                         field: 'lead',
                         minWidth: 120,
                         cellTemplate: cellToolTipTemplate
                     },
                    {
                        name: app.localize('PhoneNo'),
                        field: 'phoneNo',
                        minWidth: 100,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('Address'),
                        field: 'address',
                        minWidth: 120,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('Regionn'),
                        field: 'region',
                        minWidth: 150,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('requeststatus'),
                        field: 'requestStatus',
                        minWidth: 100,
                        cellTemplate: cellToolTipTemplate
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.getRole();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getRole();
                    });
                },
                data: []
            };

            vm.allot = function (requistion) {
                var modalInstance = $modal.open({
                    templateUrl: '~/App/tenant/views/Allotment/CreateOrEdit.cshtml',
                    controller: 'tenant.views.Allotment.CreateOrEdit as vm',
                    backdrop: 'static',
                    resolve: {
                        requistionid: function () {
                            return requistion.company_Id;
                        },
                        leadId: function () {
                            return 0;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    vm.getRole();
                });
            };

            vm.Request = function (request) {
                console.log(request);
                requistionService.createLeadRequest(request).success(function () {
                    abp.notify.info(app.localize('requestmessage'));
                }).finally(function () {
                    vm.getRole();
                });
            };

            vm.getRole = function () {
                requistionService.getrole().success(function (result) {
                    if (result == 'Admin' || result == 'Management' || result == 'Audit/Reporting') {
                        vm.loading = true;
                        requistionService.getCompanyData({
                            skipCount: requestParams.skipCount,
                            maxResultCount: requestParams.maxResultCount,
                            sorting: requestParams.sorting,
                            filter: vm.filterText
                        }).success(function (result1) {
                            console.log(result1);
                            vm.value = 1;
                            vm.RequistionsGridOptions.totalItems = result1.totalCount;
                            vm.RequistionsGridOptions.data = result1.items;
                        }).finally(function () {
                            vm.loading = false;
                        });
                    }
                    else {
                        vm.loading = true;
                        requistionService.getLeadRequestData({
                            skipCount: requestParams.skipCount,
                            maxResultCount: requestParams.maxResultCount,
                            sorting: requestParams.sorting,
                            filter: vm.filterText
                        }).success(function (result2) {
                            console.log(result2);
                            vm.value = 0;
                            vm.Requistions1GridOptions.totalItems = result2.totalCount;
                            vm.Requistions1GridOptions.data = result2.items;
                        }).finally(function () {
                            vm.loading = false;
                        });
                    }
                }).finally(function () {
                });
            };

            vm.getRole();

        }
    ]);
})();