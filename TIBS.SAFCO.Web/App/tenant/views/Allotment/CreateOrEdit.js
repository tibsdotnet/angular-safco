﻿appModule.filter('propsFilter', function () {
    return function (items, props) {
        var out = [];

        if (angular.isArray(items)) {
            var keys = Object.keys(props);

            items.forEach(function (item) {
                var itemMatches = false;

                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});


(function () {
    appModule.controller('tenant.views.Allotment.CreateOrEdit', [
        '$scope', '$modalInstance', 'abp.services.app.allot', 'requistionid','leadId',
        function ($scope, $modalInstance, allotService, requistionid,leadid) {
            var vm = this;
            vm.saving = false;
            vm.allotment = [];
            
            vm.save = function () {
                vm.saving = true;
                vm.allotment.managedId = $scope.allot.selected.empId;
                allotService.createAllotmentAsync(vm.allotment).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                //    $.ajax({
                //    type: 'POST',
                //    url: '/File/Update',
                //    data: { companyid: vm.allotment.id, employeeid: $scope.allot.selected.empId, id: leadid },
                //    success: function () {
                //        vm.saving = false;
                //        $modalInstance.close();                   
                //    },
                //    failure: function (response) {
                       
                //    }
                //});    
                }).finally(function () {
                    
                });
            };

            $scope.allot = {};

            function init() {
                allotService.getAllotForEdit({
                    id: requistionid
                }).success(function (result) {
                    vm.allotment = result.companydata;
                    vm.allot = result.allotTo;
                });
            }

            init();

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();