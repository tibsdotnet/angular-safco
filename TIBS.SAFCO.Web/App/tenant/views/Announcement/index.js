﻿(function () {
    appModule.controller('tenant.views.Announcement.index', [
        '$scope', '$modal', 'uiGridConstants', 'abp.services.app.announcement','$location',
        function ($scope, $modal, uiGridConstants, AnnouncementService,$location) {
            var vm = this;
            var Announcementid = null;
            

            $scope.$on('$viewContentLoaded', function () {
                Metronic.initAjax();
            });

            vm.loading = false;
            vm.filterText = null;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };
            vm.permissions = {
                createAnnouncement: abp.auth.hasPermission('Pages.Tenant.Announcement.CreateNewAnnouncement'),
                'edit': abp.auth.hasPermission('Pages.Tenant.Announcement.EditAnnouncement'),
                'delete': abp.auth.hasPermission('Pages.Tenant.Announcement.DeleteAnnouncement')
            };
            var cellToolTipTemplate =
               '<div class="ui-grid-cell-contents" tooltip = "{{COL_FIELD}}" tooltip-append-to-body="true" tooltip-popup-delay="200" >{{ COL_FIELD }}</div>';

            vm.exportToExcel = function () {
                AnnouncementService.getAnnouncementToExcel(vm.requestParams)
                    .success(function (result) {
                        app.downloadTempFile(result);
                    });
            };

            vm.AnnouncementGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 120,
                        cellTemplate:
                            //'<div class=\"ui-grid-cell-contents\">' +
                            //'  <div class="btn-group dropdown" dropdown="">' +
                            //'    <button class="btn btn-xs btn-primary blue" dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            //'    <ul class="dropdown-menu">' +
                            //'      <li><a ng-if="grid.appScope.permissions.edit" ng-click="grid.appScope.editAnnouncement(row.entity)">' + app.localize('Edit') + '</a></li>' +
                            //'      <li><a  ng-if="grid.appScope.permissions.delete" ng-click="grid.appScope.deleteAnnouncement(row.entity)">' + app.localize('Delete') + '</a></li>' +
                            //'    </ul>' +
                            //'  </div>' +
                            //'</div>'

                            '<div class=\"ui-grid-cell-contents text-center\">' +
                                '  <button class="btn btn-default btn-xs" ng-if="grid.appScope.permissions.edit" ng-click="grid.appScope.editAnnouncement(row.entity)"><i class="fa fa-pencil-square-o font-blue"></i></button>' +
                                 '  <button class="btn btn-default btn-xs" ng-if="grid.appScope.permissions.delete" ng-click="grid.appScope.deleteAnnouncement(row.entity)"><i class="fa fa-trash-o font-red"></i></button>' +
                                '</div>'
                    },
                    {
                        name: app.localize('Title'),
                        field: 'title',
                        minWidth: 150,
                        cellTemplate: cellToolTipTemplate
                    },
                    {
                        name: app.localize('Description'),
                        field: 'description',
                        minWidth: 300,
                        cellTemplate: cellToolTipTemplate
                    },
                   
                    {
                        name: app.localize('PublishedDate'),
                        field: 'publishedDate',
                        enableSorting: false,
                        minWidth: 200,
                    },
                      {
                          name: app.localize('ExpiryDate'),
                          field: 'expiredDate',
                          enableSorting: false,
                          minWidth: 200,
                          cellTemplate: cellToolTipTemplate,
                      }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }
                        vm.getAnnouncement();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAnnouncement();
                    });
                },
                data: []
            };

            vm.getAnnouncement = function () {
                vm.loading = true;
                AnnouncementService.getAccouncement({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).success(function (result) {
                    vm.AnnouncementGridOptions.totalItems = result.totalCount;
                    vm.AnnouncementGridOptions.data = result.items;
                    
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.openCreateAnnouncementModal = function () {
                Announcementid = "createannouncement";
                openAnnouncementModal('/createannoucement');
            };

            vm.editAnnouncement = function (Announcement) {
                Announcementid = Announcement.id;
                openAnnouncementModal('/createannoucement');
            };

            function openAnnouncementModal(hash) {
                //var modalInstance = $modal.open({
                //    templateUrl: '~/App/tenant/views/Announcement/createAnnouncementModal.cshtml',
                //    controller: 'tenant.views.Announcement.createAnnouncementModal as vm',
                //    backdrop: 'static',
                //    resolve: {
                //        Announcementid: function () {
                //            return Announcementid;
                //        }
                //    }
                //});

                //modalInstance.result.then(function () {
                //    vm.getAnnouncement();
                //});
                $location.path('createannoucement/' + Announcementid);
            }


            vm.deleteAnnouncement = function (Announcement) {
                abp.message.confirm(
                    app.localize('AreYouSureToDeleteAnnouncement', Announcement.title),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            AnnouncementService.deleteAnnouncment({
                                id: Announcement.id
                            }).success(function () {
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                                vm.getAnnouncement();
                            });
                        }
                    }
                );
            };

            vm.getAnnouncement();
        }
    ]);
})();