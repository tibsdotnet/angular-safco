﻿(function () {
    appModule.controller('tenant.views.Announcement.createAnnouncementModal', [
        '$scope', '$modal', 'abp.services.app.announcement','$location',
        function ($scope, $modal, AnnouncementService, $location) {
            var vm = this;
            vm.saving = false;
            vm.announcement = null;
            var Announcementid = $scope.Announcementid;
            vm.save = function () {
                vm.saving = true;
                vm.announcement.publishedDate = vm.dates.date1;
                vm.announcement.expiredDate = vm.dates.date2;
                AnnouncementService.createOrUpdateAnnouncement(vm.announcement).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $location.path('/annoucement');
                    //$modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            function init() {
                AnnouncementService.getAnnouncementForEdit({
                    id: Announcementid
                }).success(function (result) {
                    //console.log(result.Announcement);
                    vm.announcement = result.announcement;
                    vm.dates.date1 = result.announcement.publishedDate;
                    vm.dates.date2 = result.announcement.expiredDate;
                });
            }

            init();

            vm.cancel = function () {
                // $modalInstance.dismiss();
                $location.path('/annoucement');
            };
            var in10Days = new Date();
            in10Days.setDate(in10Days.getDate() + 10);

            this.dates = {
                date1: new Date(),
                date2: new Date(),
            };

            //vm.dated.date1 = false;

            this.open = {
                date1: false,
                date2: false,

            };

            // Disable weekend selection
            //this.disabled = function (date, mode) {
            //    return (mode === 'day' && (new Date().toDateString() == date.toDateString()));
            //};

            this.dateOptions = {
                showWeeks: false,
                startingDay: 1
            };

            this.timeOptions = {
                readonlyInput: false,
                showMeridian: true
            };

            this.dateModeOptions = {
                minMode: 'year',
                maxMode: 'year'
            };

            this.openCalendar = function (e, date) {

                vm.open[date] = true;


            };
        }
    ]);
})();