﻿(function () {
    appModule.controller('common.views.layout.header', [
        '$rootScope', '$scope', '$modal', 'appSession','abp.services.app.targetRatesCalculate',
        function ($rootScope, $scope, $modal, appSession, ProgressService) {
            var vm = this;

            $scope.$on('$includeContentLoaded', function () {
                Layout.initHeader(); // init header
            });

            vm.languages = abp.localization.languages;
            vm.currentLanguage = abp.localization.currentLanguage;

            vm.getShownUserName = function () {
                if (!abp.multiTenancy.isEnabled) {
                    return appSession.user.userName;
                } else {
                    if (appSession.tenant) {
                        return appSession.tenant.tenancyName + '\\' + appSession.user.userName;
                    } else {
                        return '.\\' + appSession.user.userName;
                    }
                }
            };

            vm.editMySettings = function () {
                $modal.open({
                    templateUrl: '~/App/common/views/profile/mySettingsModal.cshtml',
                    controller: 'common.views.profile.mySettingsModal as vm',
                    backdrop: 'static'
                });
            };

            vm.changePassword = function () {
                $modal.open({
                    templateUrl: '~/App/common/views/profile/changePassword.cshtml',
                    controller: 'common.views.profile.changePassword as vm',
                    backdrop: 'static'
                });
            };

            vm.changePicture = function() {
                $modal.open({
                    templateUrl: '~/App/common/views/profile/changePicture.cshtml',
                    controller: 'common.views.profile.changePicture as vm',
                    backdrop: 'static'
                });
            };

            vm.changeLanguage = function(languageName) {
                location.href = abp.appPath + 'AbpLocalization/ChangeCulture?cultureName=' + languageName + '&returnUrl=' + window.location.href;
            };

            function init() {
                ProgressService.userProgressBar().success(function (result) {
                    vm.roles = result.userRole;
                    vm.monthTarget = result.targetAchievedMonth;
                    vm.targetcolor = null;
                    if (vm.monthTarget[0].percentage < 25) {
                        vm.targetcolor = "danger";
                    }
                    else if (vm.monthTarget[0].percentage < 50) {
                        vm.targetcolor = "warning";
                    }
                    else if (vm.monthTarget[0].percentage < 75) {
                        vm.targetcolor = "info";
                    }
                    else {
                        vm.targetcolor = "success";
                    }

                    vm.yearTarget = result.targetAchievedYear;
                    vm.targetcolor1 = null;
                    if (vm.yearTarget[0].percentage < 25) {
                        vm.targetcolor1 = "danger";
                    }
                    else if (vm.yearTarget[0].percentage < 50) {
                        vm.targetcolor1 = "warning";
                    }
                    else if (vm.yearTarget[0].percentage < 75) {
                        vm.targetcolor1 = "info";
                    }
                    else {
                        vm.targetcolor1 = "success";
                    }
                   
                });
            }

            init();
        }
    ]);
})();