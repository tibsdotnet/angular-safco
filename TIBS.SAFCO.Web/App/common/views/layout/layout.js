﻿(function () {
    appModule.controller('common.views.layout', [
        '$scope', 'abp.services.app.tenantDashboard',
        function ($scope, tenantDashboardService) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initComponents(); // init core components
            });

            function init() {
                tenantDashboardService.nAnnouncement().success(function (result) {
                    vm.announcements = result.announcements;
                });
            };

            init();
        }
    ]);
})();