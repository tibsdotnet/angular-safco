﻿(function () {
    appModule.controller('common.views.users.createOrEditModal', [
        '$scope', '$modalInstance', 'abp.services.app.user', 'userId',
        function ($scope, $modalInstance, userService, userId) {
            var vm = this;
            
            vm.saving = false;
            vm.user = null;
            vm.profilePictureId = null;
            vm.roles = [];
            vm.setRandomPassword = (userId == null);
            vm.sendActivationEmail = (userId == null);
            vm.canChangeUserName = true;
            var assignedRoleNames=[];
            vm.method = function (name) {
                assignedRoleNames[0] = name;
            }

            vm.icon = "fa fa-close";
            vm.AnnualConsider = false;

            vm.save = function () {
                //var assignedRoleNames = _.map(
                //    _.where(vm.roles, { isAssigned: true }), //Filter assigned roles
                //    function(role) {
                //        return role.roleName; //Get names
                //    });

                if (vm.setRandomPassword) {
                    vm.user.password = null;
                }

                vm.saving = true;
                userService.createOrUpdateUser({
                    user: vm.user,
                    assignedRoleNames: assignedRoleNames,
                    sendActivationEmail: vm.sendActivationEmail
                }).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.saveTarget = function () {
                vm.saving = true;
                vm.usertarget.monthsOfTarget = 11;
                vm.usertarget.userId = vm.user.id;
                if (vm.AnnualConsider == true)
                {
                    vm.usertarget.monthsOfTarget = 12;
                }
                userService.createorUpdateUserTarget(vm.usertarget).success(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $modalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            vm.getAssignedRoleCount = function() {
                return _.where(vm.roles, { isAssigned: true }).length;
            };

            vm.tabc = 0;
            vm.tab = function () {
                vm.tabc = 1;
            };

            vm.tab1 = function () {
                vm.tabc = 0;
            };

            function init() {
                userService.getUserForEdit({
                    id: userId
                }).success(function (result) {
                    vm.user = result.user;
                    vm.profilePictureId = result.profilePictureId;
                    vm.user.passwordRepeat = vm.user.password;
                    vm.roles = result.roles;
                    vm.usertarget = result.userT;
                    
                    if (vm.usertarget.monthsOfTarget == 12)
                    {
                        vm.AnnualConsider = true;
                    }
                    if (vm.usertarget.totalTarget > 0)
                    {
                        vm.icon = "fa fa-check";
                    }
                    for (var i = 0; i < vm.roles.length; i++) {
                        if (vm.roles[i].isAssigned == true) {
                            assignedRoleNames[0] = vm.roles[i].roleName;
                            vm.roleenable = vm.roles[i].roleDisplayName;
                        }
                    }
                    vm.canChangeUserName = vm.user.userName != app.consts.userManagement.defaultAdminUserName;
                });
            }

            init();
        }
    ]);
})();