﻿/* 'app' MODULE DEFINITION */
var appModule = angular.module("app", [
    "ui.router",
    "ui.bootstrap",
    'ui.utils',
    "ui.jq",
    'ui.select',
    'ui.grid',
    'ui.grid.autoResize',
    'ui.grid.pagination',
    "ui.bootstrap.datetimepicker",
    "oc.lazyLoad",
    "ngSanitize",
    'angularFileUpload',
    'daterangepicker',
    'angular-carousel-3d',
    'easypiechart',
    "highcharts-ng",
    'abp'
]);

/* LAZY LOAD CONFIG */

/* This application does not define any lazy-load yet but you can use $ocLazyLoad to define and lazy-load js/css files.
 * This code configures $ocLazyLoad plug-in for this application.
 * See it's documents for more information: https://github.com/ocombe/ocLazyLoad
 */
appModule.config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        cssFilesInsertBefore: 'ng_load_plugins_before', // load the css files before a LINK element with this ID.
        debug: false,
        events: true,
        modules: []
    });
}]);

/* THEME SETTINGS */
App.setAssetsPath(abp.appPath + 'metronic/assets/');
appModule.factory('settings', ['$rootScope', function ($rootScope) {
    var settings = {
        layout: {
            pageSidebarClosed: false, // sidebar menu state
            pageContentWhite: true, // set page content layout
            pageBodySolid: false, // solid body color state
            pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
        },
        layoutImgPath: App.getAssetsPath() + 'admin/layout2/img/',
        layoutCssPath: App.getAssetsPath() + 'admin/layout2/css/',
        assetsPath: abp.appPath + 'metronic/assets',
        globalPath: abp.appPath + 'metronic/assets/global',
        layoutPath: abp.appPath + 'metronic/assets/admin/layout2'
    };

    $rootScope.settings = settings;

    return settings;
}]);

/* ROUTE DEFINITIONS */

appModule.config([
    '$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {

        // Default route (overrided below if user has permission)
        $urlRouterProvider.otherwise("/welcome");

        //Welcome page
        $stateProvider.state('welcome', {
            url: '/welcome',
            templateUrl: '~/App/common/views/welcome/index.cshtml'
        });

        //COMMON routes

        if (abp.auth.hasPermission('Pages.Administration.Roles')) {
            $stateProvider.state('roles', {
                url: '/roles',
                templateUrl: '~/App/common/views/roles/index.cshtml',
                menu: 'Administration.Roles'
            });
        }

        if (abp.auth.hasPermission('Pages.Administration.Users')) {
            $stateProvider.state('users', {
                url: '/users',
                templateUrl: '~/App/common/views/users/index.cshtml',
                menu: 'Administration.Users'
            });
        }

        if (abp.auth.hasPermission('Pages.Administration.Languages')) {
            $stateProvider.state('languages', {
                url: '/languages',
                templateUrl: '~/App/common/views/languages/index.cshtml',
                menu: 'Administration.Languages'
            });

            if (abp.auth.hasPermission('Pages.Administration.Languages.ChangeTexts')) {
                $stateProvider.state('languageTexts', {
                    url: '/languages/texts/:languageName?sourceName&baseLanguageName&targetValueFilter&filterText',
                    templateUrl: '~/App/common/views/languages/texts.cshtml',
                    menu: 'Administration.Languages'
                });
            }
        }

        if (abp.auth.hasPermission('Pages.Administration.AuditLogs')) {
            $stateProvider.state('auditLogs', {
                url: '/auditLogs',
                templateUrl: '~/App/common/views/auditLogs/index.cshtml',
                menu: 'Administration.AuditLogs'
            });
        }

        //HOST routes

        $stateProvider.state('host', {
            'abstract': true,
            url: '/host',
            template: '<div ui-view class="fade-in-up"></div>'
        });

        if (abp.auth.hasPermission('Pages.Tenants')) {
            $urlRouterProvider.otherwise("/host/tenants"); //Entrance page for the host
            $stateProvider.state('host.tenants', {
                url: '/tenants',
                templateUrl: '~/App/host/views/tenants/index.cshtml',
                menu: 'Tenants'
            });
        }

        if (abp.auth.hasPermission('Pages.Editions')) {
            $stateProvider.state('host.editions', {
                url: '/editions',
                templateUrl: '~/App/host/views/editions/index.cshtml',
                menu: 'Editions'
            });
        }

        if (abp.auth.hasPermission('Pages.Administration.Host.Settings')) {
            $stateProvider.state('host.settings', {
                url: '/settings',
                templateUrl: '~/App/host/views/settings/index.cshtml',
                menu: 'Administration.Settings.Host'
            });
        }

        //TENANT routes

        $stateProvider.state('tenant', {
            'abstract': true,
            url: '/tenant',
            template: '<div ui-view class="fade-in-up"></div>'
        });

        if (abp.auth.hasPermission('Pages.Tenant.Dashboard.Dashboards')) {
            $urlRouterProvider.otherwise("/tenant/dashboard"); //Entrace page for a tenant
            $stateProvider.state('tenant.dashboard', {
                url: '/dashboard',
                templateUrl: '~/App/tenant/views/dashboard/index.cshtml',
                menu: 'Dashboard.Dashboards.Tenant'
            });
        }
        if (abp.auth.hasPermission('Pages.Administration.Tenant.Settings')) {
            $stateProvider.state('tenant.settings', {
                url: '/settings',
                templateUrl: '~/App/tenant/views/settings/index.cshtml',
                menu: 'Administration.Settings.Tenant'
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.AddressBook.CompetitorCompany')) {
            $stateProvider.state('competitorcompany', {
                url: '/competitorcompany',
                templateUrl: '~/App/tenant/views/AddressBook/CompetitorCompany/index.cshtml',
                menu: 'AddressBook.CompetitorCompany'
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.AddressBook.Country')) {
            $stateProvider.state('country', {
                url: '/country',
                templateUrl: '~/App/tenant/views/Geography/Country/index.cshtml',
                menu: 'Geography.Country'
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.AddressBook.Region')) {
            $stateProvider.state('region', {
                url: '/region',
                templateUrl: '~/App/tenant/views/Geography/Region/index.cshtml',
                menu: 'Geography.Region'
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.AddressBook.Company')) {
            $stateProvider.state('company', {
                url: '/company',
                templateUrl: '~/App/tenant/views/AddressBook/Company/index.cshtml',
                menu: 'AddressBook.Company'
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.Accouncement')) {
            $stateProvider.state('annoucement', {
                url: '/annoucement',
                templateUrl: '~/App/tenant/views/Announcement/index.cshtml',
                menu: 'Announcement'
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.Product.ProductClassification')) {
            $stateProvider.state('productclassification', {
                url: '/productclassification',
                templateUrl: '~/App/tenant/views/Product/ProductClassification/index.cshtml',
                menu: 'Product.ProductClassification'
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.Product.Products')) {
            $stateProvider.state('products', {
                url: '/products',
                templateUrl: '~/App/tenant/views/Product/Products/index.cshtml',
                menu: 'Product.Products'
            });
        }
        //if (abp.auth.hasPermission('Pages.Tenant.Review.Activity')) {
        //    $stateProvider.state('activity', {
        //        url: '/activity',
        //        templateUrl: '~/App/tenant/views/Activity/index.cshtml',
        //        menu: 'Review.Activity'
        //    });
        //}
        if (abp.auth.hasPermission('Pages.Tenant.Dashboard.ActivityOverView')) {
            $stateProvider.state('activityoverview', {
                url: '/activityoverview',
                templateUrl: '~/App/tenant/views/Activity OverView/index.cshtml',
                menu: 'Dashboard.ActivityOverView'
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.Dashboard.NoticeBoard')) {
            $stateProvider.state('noticeboard', {
                url: '/noticeboard',
                templateUrl: '~/App/tenant/views/NoticeBoard/index.cshtml',
                menu: 'Dashboard.NoticeBoard'
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.LeadManagement')) {
            $stateProvider.state('leadmanagement', {
                url: '/leadmanagement',
                templateUrl: '~/App/tenant/views/LeadManagement/index.cshtml',
                menu: 'LeadManagement'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.AddressBook.JunkCompanies')) {
            $stateProvider.state('junkcompanies', {
                url: '/junkcompanies',
                templateUrl: '~/App/tenant/views/AddressBook/JunkCompany/index.cshtml',
                menu: 'AddressBook.JunkCompanies'
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.NewCustomer')) {
            $stateProvider.state('newcustomer', {
                url: '/newcustomer',
                templateUrl: '~/App/tenant/views/NewCustomer/index.cshtml',
                menu: 'NewCustomer'
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.Approval.SaleApproval')) {
            $stateProvider.state('saleapproval', {
                url: '/saleapproval',
                templateUrl: '~/App/tenant/views/Approval/SaleApproval/index.cshtml',
                menu: 'Approval.SaleApproval'
            });
        }
        if (abp.auth.hasPermission('Pages.Tenant.Approval.JunkApproval')) {
            $stateProvider.state('junkapproval', {
                url: '/junkapproval',
                templateUrl: '~/App/tenant/views/AddressBook/JunkApproval/index.cshtml',
                menu: 'Approval.JunkApproval'
            });
        }
        $stateProvider.state('createannoucement', {
            url: '/createannoucement/:Announcementid',
            templateUrl: '~/App/tenant/views/Announcement/CreateAnnouncementModal.cshtml',
            controller: function ($scope, $stateParams) {
                $scope.Announcementid = $stateParams.Announcementid;
            }
        });
        if (abp.auth.hasPermission('Pages.Tenant.Employee')) {
            $stateProvider.state('employee', {
                url: '/employee',
                templateUrl: '~/App/tenant/views/Employee/index.cshtml',
                menu: 'Employee'
            });
        }
        $stateProvider.state('createemployee', {
            url: '/createemployee/:employeeid',
            templateUrl: '~/App/tenant/views/Employee/CreateEmployeeModal.cshtml',
            controller: function ($scope, $stateParams) {
                $scope.employeeid = $stateParams.employeeid;
            }
        });

        $stateProvider.state("Employeedetails", {
            url: "/Employeedetails/:personid",
            templateUrl: "~/App/tenant/views/Employee/EmployeeDetails.cshtml",
            // data: { pageTitle: 'User Profile', pageSubTitle: 'user profile sample' },
            controller: function ($scope, $stateParams) {
                $scope.personid = $stateParams.personid;
                // alert($scope.personid);

            },
            // $scope.personid = $stateParams.personid;
            resolve: {

                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    // $scope.personid = $stateParams.personid;
                    return $ocLazyLoad.load({

                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [

                            
                            '../../../assets/admin/pages/css/profile.css',
                            
                            '../../../assets/admin/pages/scripts/profile.js',

                           


                        ]

                    });


                    // alert($scope.personid);

                }]

            }



        })

        $stateProvider.state("Employeedetails.information", {
            url: "/information",
            templateUrl: "~/App/tenant/views/Employee/EmployeeInformation.cshtml",
            
        })

        $stateProvider.state("Employeedetails.account", {
            url: "/account",
            templateUrl: "~/App/tenant/views/Employee/account.cshtml",
           
        })

        if (abp.auth.hasPermission('Pages.Tenant.Requisitions')) {
            $stateProvider.state('requisitions', {
                url: '/requisitions',
                templateUrl: '~/App/tenant/views/Requisitions/index.cshtml',
                menu: 'Requisitions'
            });
        }
          $stateProvider.state('leaddetails', {
            url: '/leaddetails/:companyId/:leadId',
            templateUrl: '~/App/tenant/views/LeadManagement/leaddetails.cshtml',
            controller: function ($scope, $stateParams) {
                $scope.companyId = $stateParams.companyId;
                $scope.leadId = $stateParams.leadId;
            },
            resolve: {

                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    // $scope.personid = $stateParams.personid;
                    return $ocLazyLoad.load({

                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [


                            '../../../assets/admin/pages/css/profile.css',

                            '../../../assets/admin/pages/scripts/profile.js',




                        ]

                    });


                    // alert($scope.personid);

                }]

            }
        });
        $stateProvider.state("leaddetails.information", {
            url: "/information",
            templateUrl: "~/App/tenant/views/LeadManagement/Compnaydetails.cshtml",

        });

        $stateProvider.state("leaddetails.activities", {
            url: "/activities",
            templateUrl: "~/App/tenant/views/LeadManagement/Activities.cshtml",

        });

        $stateProvider.state('createsaleapproval', {
            url: '/createsaleapproval/:leadId',
            templateUrl: '~/App/tenant/views/Approval/SaleApproval/EditSaleApproval.cshtml',
            controller: function ($scope, $stateParams) {
                $scope.leadId = $stateParams.leadId;
            }
        });

        $stateProvider.state("leaddetails.createschedule", {
            url: "/schedule/:scheduleId",
            templateUrl: "~/App/tenant/views/LeadManagement/leadSchedule.cshtml",
            controller: function ($scope, $stateParams) {
                $scope.scheduleId = $stateParams.scheduleId;
            }

        });
    }
]);

appModule.run(["$rootScope", "settings", "$state", function ($rootScope, settings, $state) {
    $rootScope.$state = $state;
    $rootScope.$settings = settings; 

    $rootScope.safeApply = function (fn) {
        var phase = this.$root.$$phase;
        if (phase == '$apply' || phase == '$digest') {
            if (fn && (typeof (fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };
}]);