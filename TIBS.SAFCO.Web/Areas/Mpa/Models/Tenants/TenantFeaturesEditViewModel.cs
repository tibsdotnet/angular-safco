using Abp.AutoMapper;
using TIBS.SAFCO.MultiTenancy;
using TIBS.SAFCO.MultiTenancy.Dto;
using TIBS.SAFCO.Web.Areas.Mpa.Models.Common;

namespace TIBS.SAFCO.Web.Areas.Mpa.Models.Tenants
{
    [AutoMapFrom(typeof (GetTenantFeaturesForEditOutput))]
    public class TenantFeaturesEditViewModel : GetTenantFeaturesForEditOutput, IFeatureEditViewModel
    {
        public Tenant Tenant { get; set; }

        public TenantFeaturesEditViewModel(Tenant tenant, GetTenantFeaturesForEditOutput output)
        {
            Tenant = tenant;
            output.MapTo(this);
        }
    }
}