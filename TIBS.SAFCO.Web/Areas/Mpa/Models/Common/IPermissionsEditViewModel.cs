using System.Collections.Generic;
using TIBS.SAFCO.Authorization.Dto;

namespace TIBS.SAFCO.Web.Areas.Mpa.Models.Common
{
    public interface IPermissionsEditViewModel
    {
        List<FlatPermissionDto> Permissions { get; set; }

        List<string> GrantedPermissionNames { get; set; }
    }
}