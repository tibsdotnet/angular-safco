using System.Collections.Generic;
using Abp.Application.Services.Dto;
using TIBS.SAFCO.Editions.Dto;

namespace TIBS.SAFCO.Web.Areas.Mpa.Models.Common
{
    public interface IFeatureEditViewModel
    {
        List<NameValueDto> FeatureValues { get; set; }

        List<FlatFeatureDto> Features { get; set; }
    }
}