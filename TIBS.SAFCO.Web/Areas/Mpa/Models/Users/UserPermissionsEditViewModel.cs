using Abp.AutoMapper;
using TIBS.SAFCO.Authorization.Users;
using TIBS.SAFCO.Authorization.Users.Dto;
using TIBS.SAFCO.Web.Areas.Mpa.Models.Common;

namespace TIBS.SAFCO.Web.Areas.Mpa.Models.Users
{
    [AutoMapFrom(typeof(GetUserPermissionsForEditOutput))]
    public class UserPermissionsEditViewModel : GetUserPermissionsForEditOutput, IPermissionsEditViewModel
    {
        public User User { get; private set; }

        public UserPermissionsEditViewModel(GetUserPermissionsForEditOutput output, User user)
        {
            User = user;
            output.MapTo(this);
        }
    }
}