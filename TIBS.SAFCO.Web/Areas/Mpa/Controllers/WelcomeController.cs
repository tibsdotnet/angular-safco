using System.Web.Mvc;
using Abp.Web.Mvc.Authorization;
using TIBS.SAFCO.Web.Controllers;

namespace TIBS.SAFCO.Web.Areas.Mpa.Controllers
{
    [AbpMvcAuthorize]
    public class WelcomeController : SAFCOControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}