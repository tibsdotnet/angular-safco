﻿using System.Web.Mvc;
using Abp.Web.Mvc.Authorization;
using TIBS.SAFCO.Authorization;
using TIBS.SAFCO.Web.Controllers;

namespace TIBS.SAFCO.Web.Areas.Mpa.Controllers
{
    [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Dashboard)]
    public class DashboardController : SAFCOControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}