﻿using System.Web.Mvc;
using Abp.Auditing;
using Abp.Web.Mvc.Authorization;
using TIBS.SAFCO.Authorization;
using TIBS.SAFCO.Web.Controllers;

namespace TIBS.SAFCO.Web.Areas.Mpa.Controllers
{
    [DisableAuditing]
    [AbpMvcAuthorize(AppPermissions.Pages_Administration_AuditLogs)]
    public class AuditLogsController : SAFCOControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}