﻿using Abp.Configuration;
using Abp.Dependency;
using Abp.Web.Mvc.Views;

namespace TIBS.SAFCO.Web.Views
{
    public abstract class SAFCOWebViewPageBase : SAFCOWebViewPageBase<dynamic>
    {

    }

    public abstract class SAFCOWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        public ISettingManager SettingManager { get; set; }

        protected SAFCOWebViewPageBase()
        {
            LocalizationSourceName = SAFCOConsts.LocalizationSourceName;
            SettingManager = IocManager.Instance.Resolve<ISettingManager>();
        }
    }
}