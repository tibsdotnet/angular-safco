﻿using Abp.Application.Navigation;
using Abp.Localization;
using TIBS.SAFCO.Authorization;

namespace TIBS.SAFCO.Web.Navigation
{
    /// <summary>
    /// This class defines menus for the application.
    /// It uses ABP's menu system.
    /// When you add menu items here, they are automatically appear in angular application.
    /// See .cshtml and .js files under App/Main/views/layout/header to know how to render menu.
    /// </summary>
    public class AppNavigationProvider : NavigationProvider
    {
        public override void SetNavigation(INavigationProviderContext context)
        {
            context.Manager.MainMenu
                .AddItem(new MenuItemDefinition(
                    PageNames.App.Host.Tenants,
                    L("Tenants"),
                    url: "host.tenants",
                    icon: "icon-globe",
                    requiredPermissionName: AppPermissions.Pages_Tenants
                    )
                ).AddItem(new MenuItemDefinition(
                    PageNames.App.Host.Editions,
                    L("Editions"),
                    url: "host.editions",
                    icon: "icon-grid",
                    requiredPermissionName: AppPermissions.Pages_Editions
                    )
                ).AddItem(new MenuItemDefinition(
                    PageNames.App.Tenant.Dashboard,
                    L("Dashboard"),
                    icon: "fa fa-street-view",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Dashboard
                    //).AddItem(new MenuItemDefinition(
                    //    PageNames.App.Tenant.Activity,
                    //    L("Activity"),
                    //    url: "activity",
                    //    icon: "fa fa-spinner",
                    //    requiredPermissionName: AppPermissions.Pages_Tenant_Review_ActivityOverView)
                        ).AddItem(new MenuItemDefinition(
                    PageNames.App.Tenant.Dashboards,
                    L("Dashboard"),
                    url: "tenant.dashboard",
                    icon: "fa fa-pie-chart",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Dashboard_Dashboards
                    )).AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.ActivityOverView,
                        L("ActivityOverView"),
                        url: "activityoverview",
                        icon: "fa fa-spinner",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Dashboard_ActivityOverView))
                        .AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.NoticeBoard,
                        L("NoticeBoard"),
                        url: "noticeboard",
                        icon: "fa fa-pencil-square-o",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Dashboard_NoticeBoard
                        ))
                        ).AddItem(new MenuItemDefinition(
                    PageNames.App.Tenant.Announcement,
                    L("Anouncement"),
                    icon: "fa fa-bullhorn",
                    url: "annoucement",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Announcement
                    )).AddItem(new MenuItemDefinition(
                    PageNames.App.Tenant.AddressBook,
                    L("AddressBook"),
                    icon: "fa fa-book",
                    requiredPermissionName: AppPermissions.Pages_Tenant_AddressBook
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.Company,
                        L("Company"),
                        url: "company",
                        icon: "fa fa-university",
                    requiredPermissionName: AppPermissions.Pages_Tenant_AddressBook_Company
                        )
                    ).AddItem(new MenuItemDefinition(
                            PageNames.App.Tenant.CompetitorCompany,
                            L("CompetitorCompany"),
                            url: "competitorcompany",
                            icon: "fa fa-building-o",
                            requiredPermissionName: AppPermissions.Pages_Tenant_AddressBook_CompetitorCompany
                            ))
                            .AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.JunkCompanies,
                        L("JunkCompany"),
                        url: "junkcompanies",
                        icon: "fa fa-archive",
                        requiredPermissionName: AppPermissions.Pages_Tenant_AddressBook_JunkCompanies
                        )).AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.Country,
                        L("Country"),
                        url: "country",
                        icon: "fa fa-location-arrow",
                    requiredPermissionName: AppPermissions.Pages_Tenant_AddressBook_Country
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.Region,
                        L("Region"),
                        url: "region",
                        icon: "fa fa-map-signs",
                        requiredPermissionName: AppPermissions.Pages_Tenant_AddressBook_Region
                        )
                    )).AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.Requisitions,
                        L("Requisitions"),
                        url: "requisitions",
                        icon: "fa fa-bolt",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Requisitions
                        )).AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.LeadManagement,
                        L("LeadManagement"),
                        url: "leadmanagement",
                        icon: "fa fa-shopping-cart",
                    requiredPermissionName: AppPermissions.Pages_Tenant_LeadManagement
                        )).AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.Employee,
                        L("Employee"),
                        url: "employee",
                        icon: "fa fa-users",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Employee
                        ))
                        .AddItem(new MenuItemDefinition(
                    PageNames.App.Tenant.Product,
                    L("Product"),
                    icon: "fa fa-fire",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Product
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.Products,
                        L("Products"),
                        url: "products",
                        icon: "fa fa-cube",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Product_Products
                        )).AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.ProductClassification,
                        L("ProductClassification"),
                        url: "productclassification",
                        icon: "fa fa-cubes",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Product_ProductClassification
                        )))
                        .AddItem(new MenuItemDefinition(
                    PageNames.App.Tenant.Approval,
                    L("Approval"),
                    icon: "fa fa-gavel",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Approval
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.SaleApproval,
                        L("SaleApproval"),
                        url: "saleapproval",
                        icon: "fa fa-truck",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Approval_SaleApproval
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.JunkApproval,
                        L("JunkApproval"),
                        url: "junkapproval",
                        icon: "fa fa-exclamation-triangle",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Approval_JunkApproval
                        )
                    ))
                        .AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.NewCustomer,
                        L("NewCustomer"),
                        url: "newcustomer",
                        icon: "fa fa-user-secret",
                        requiredPermissionName: AppPermissions.Pages_Tenant_NewCustomer
                        ))
                .AddItem(new MenuItemDefinition(
                    PageNames.App.Common.Administration,
                    L("Administration"),
                    icon: "icon-wrench"
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Common.Roles,
                        L("Roles"),
                        url: "roles",
                        icon: "icon-briefcase",
                        requiredPermissionName: AppPermissions.Pages_Administration_Roles
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Common.Users,
                        L("Users"),
                        url: "users",
                        icon: "icon-users",
                        requiredPermissionName: AppPermissions.Pages_Administration_Users
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Common.Languages,
                        L("Languages"),
                        url: "languages",
                        icon: "icon-flag",
                        requiredPermissionName: AppPermissions.Pages_Administration_Languages
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Common.AuditLogs,
                        L("AuditLogs"),
                        url: "auditLogs",
                        icon: "icon-lock",
                        requiredPermissionName: AppPermissions.Pages_Administration_AuditLogs
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Host.Settings,
                        L("Settings"),
                        url: "host.settings",
                        icon: "icon-settings",
                        requiredPermissionName: AppPermissions.Pages_Administration_Host_Settings
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.Settings,
                        L("Settings"),
                        url: "tenant.settings",
                        icon: "icon-settings",
                        requiredPermissionName: AppPermissions.Pages_Administration_Tenant_Settings
                        )
                    )
                );
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, SAFCOConsts.LocalizationSourceName);
        }
    }
}
