namespace TIBS.SAFCO.Web.Navigation
{
    public static class PageNames
    {
        public static class App
        {
            public static class Common
            {
                public const string Administration = "Administration";
                public const string Roles = "Administration.Roles";
                public const string Users = "Administration.Users";
                public const string AuditLogs = "Administration.AuditLogs";
                public const string Languages = "Administration.Languages";
            }

            public static class Host
            {
                public const string Tenants = "Tenants";
                public const string Editions = "Editions";
                public const string Settings = "Administration.Settings.Host";
            }

            public static class Tenant
            {
                public const string Dashboard = "Dashboard.Tenant";
                public const string Settings = "Administration.Settings.Tenant";
                public const string Geography = "Geography";
                public const string Country = "Geography.Country";
                public const string Region = "Geography.Region";
                public const string AddressBook = "AddressBook";
                public const string Company = "AddressBook.Company";
                public const string CompetitorCompany = "AddressBook.CompetitorCompany";
                public const string Dashboards = "Dashboard.Dashboards.Tenant";
                public const string ActivityOverView = "Dashboard.ActivityOverView";
                public const string NoticeBoard = "Dashboard.NoticeBoard";
                public const string Announcement = "Announcement";
                public const string Product = "Product";
                public const string ProductClassification = "Product.ProductClassification";
                public const string Products = "Product.Products";
                public const string Requisitions = "Requisitions";
                public const string LeadManagement = "LeadManagement";
                public const string JunkCompanies = "AddressBook.JunkCompanies";
                public const string Approval = "Approval";
                public const string SaleApproval = "Approval.SaleApproval";
                public const string JunkApproval = "Approval.JunkApproval";
                public const string UserSerivce = "UserSerivce";
                public const string Employee = "UserSerivce.Employee";
                public const string NewCustomer = "UserSerivce.NewCustomer";
                
            }
        }

        public static class Frontend
        {
            public const string Home = "Frontend.Home";
            public const string About = "Frontend.About";
        }
    }
}