﻿using Abp.Dependency;

namespace TIBS.SAFCO.Web
{
    public class AppFolders : IAppFolders, ISingletonDependency
    {
        public string TempFileDownloadFolder { get; set; }
    }
}