using Abp.WebApi.Controllers;

namespace TIBS.SAFCO.WebApi
{
    public abstract class SAFCOApiControllerBase : AbpApiController
    {
        protected SAFCOApiControllerBase()
        {
            LocalizationSourceName = SAFCOConsts.LocalizationSourceName;
        }
    }
}