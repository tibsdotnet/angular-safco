﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.ApprovalStatusss;
using TIBS.SAFCO.Compnaies;
using TIBS.SAFCO.Employees;

namespace TIBS.SAFCO.LeadRequests
{
    [Table("LeadRequest")]
    public class LeadRequest:FullAuditedEntity
    {
        [ForeignKey("Company_Id")]
        public virtual Company Companies { get; set; }

        public virtual int Company_Id { get; set; }

        [ForeignKey("RequestedBy_Employees_ID")]
        public virtual Employee Employees { get; set; }

        public virtual int RequestedBy_Employees_ID { get; set; }

        [ForeignKey("ApprovalStatus_IDD")]
        public virtual ApprovalStatus ApprovalStatuss { get; set; }

        public virtual int ApprovalStatus_IDD { get; set; }

        [ForeignKey("ApprovalBy_Employees_ID")]
        public virtual Employee Employes { get; set; }

        public virtual int? ApprovalBy_Employees_ID { get; set; }

        public virtual bool? Archived { get; set; }
    }
}
