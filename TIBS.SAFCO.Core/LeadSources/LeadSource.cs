﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.SAFCO.LeadSources
{
    [Table("LeadSource")]
    public class LeadSource:FullAuditedEntity
    {
        public virtual string Tittle { get; set; }
    }
}
