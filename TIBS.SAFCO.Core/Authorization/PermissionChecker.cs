﻿using Abp.Authorization;
using TIBS.SAFCO.Authorization.Roles;
using TIBS.SAFCO.Authorization.Users;
using TIBS.SAFCO.MultiTenancy;

namespace TIBS.SAFCO.Authorization
{
    /// <summary>
    /// Implements <see cref="PermissionChecker"/>.
    /// </summary>
    public class PermissionChecker : PermissionChecker<Tenant, Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
