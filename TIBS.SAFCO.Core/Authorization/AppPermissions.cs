﻿namespace TIBS.SAFCO.Authorization
{
    /// <summary>
    /// Defines string constants for application's permission names.
    /// <see cref="AppAuthorizationProvider"/> for permission definitions.
    /// </summary>
    public static class AppPermissions
    {
        //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

        public const string Pages = "Pages";
        
        public const string Pages_Administration = "Pages.Administration";

        public const string Pages_Administration_Roles = "Pages.Administration.Roles";
        public const string Pages_Administration_Roles_Create = "Pages.Administration.Roles.Create";
        public const string Pages_Administration_Roles_Edit = "Pages.Administration.Roles.Edit";
        public const string Pages_Administration_Roles_Delete = "Pages.Administration.Roles.Delete";

        public const string Pages_Administration_Users = "Pages.Administration.Users";
        public const string Pages_Administration_Users_Create = "Pages.Administration.Users.Create";
        public const string Pages_Administration_Users_Edit = "Pages.Administration.Users.Edit";
        public const string Pages_Administration_Users_Delete = "Pages.Administration.Users.Delete";
        public const string Pages_Administration_Users_ChangePermissions = "Pages.Administration.Users.ChangePermissions";

        public const string Pages_Administration_Languages = "Pages.Administration.Languages";
        public const string Pages_Administration_Languages_Create = "Pages.Administration.Languages.Create";
        public const string Pages_Administration_Languages_Edit = "Pages.Administration.Languages.Edit";
        public const string Pages_Administration_Languages_Delete = "Pages.Administration.Languages.Delete";
        public const string Pages_Administration_Languages_ChangeTexts = "Pages.Administration.Languages.ChangeTexts";

        public const string Pages_Administration_AuditLogs = "Pages.Administration.AuditLogs";

        //TENANT-SPECIFIC PERMISSIONS

        public const string Pages_Tenant_Dashboard = "Pages.Tenant.Dashboard";
        public const string Pages_Tenant_Dashboard_Dashboards = "Pages.Tenant.Dashboard.Dashboards";
        public const string Pages_Tenant_Dashboard_ActivityOverView = "Pages.Tenant.Dashboard.ActivityOverView";
        public const string Pages_Tenant_Dashboard_NoticeBoard = "Pages.Tenant.Dashboard.NoticeBoard";

        public const string Pages_Administration_Tenant_Settings = "Pages.Administration.Tenant.Settings";
        
        //HOST-SPECIFIC PERMISSIONS

        public const string Pages_Editions = "Pages.Editions";
        public const string Pages_Editions_Create = "Pages.Editions.Create";
        public const string Pages_Editions_Edit = "Pages.Editions.Edit";
        public const string Pages_Editions_Delete = "Pages.Editions.Delete";

        public const string Pages_Tenants = "Pages.Tenants";
        public const string Pages_Tenants_Create = "Pages.Tenants.Create";
        public const string Pages_Tenants_Edit = "Pages.Tenants.Edit";
        public const string Pages_Tenants_ChangeFeatures = "Pages.Tenants.ChangeFeatures";
        public const string Pages_Tenants_Delete = "Pages.Tenants.Delete";

        public const string Pages_Administration_Host_Settings = "Pages.Administration.Host.Settings";

        //AddressBook
        public const string Pages_Tenant_AddressBook = "Pages.Tenant.AddressBook";
        public const string Pages_Tenant_AddressBook_Company = "Pages.Tenant.AddressBook.Company";
        public const string Pages_Tenant_AddressBook_Company_CreateNewCompany = "Pages.Tenant.AddressBook.Company.CreateNewCompany";
        public const string Pages_Tenant_AddressBook_Company_EditCompany = "Pages.Tenant.AddressBook.Company.EditCompany";
        public const string Pages_Tenant_AddressBook_Company_DeleteCompany = "Pages.Tenant.AddressBook.Company.DeleteCompany";
        public const string Pages_Tenant_AddressBook_CompetitorCompany = "Pages.Tenant.AddressBook.CompetitorCompany";
        public const string Pages_Tenant_AddressBook_CompetitorCompany_CreateNewCompetitorCompany = "Pages.Tenant.AddressBook.CompetitorCompany.CreateNewCompetitorCompany";
        public const string Pages_Tenant_AddressBook_CompetitorCompany_EditCompetitorCompany = "Pages.Tenant.AddressBook.CompetitorCompany.EditCompetitorCompany";
        public const string Pages_Tenant_AddressBook_CompetitorCompany_DeleteCompetitorCompany = "Pages.Tenant.AddressBook.CompetitorCompany.DeleteCompetitorCompany";
        public const string Pages_Tenant_AddressBook_JunkCompanies = "Pages.Tenant.AddressBook.JunkCompanies";
        public const string Pages_Tenant_AddressBook_Country = "Pages.Tenant.AddressBook.Country";
        public const string Pages_Tenant_AddressBook_Country_CreateNewCountry = "Pages.Tenant.AddressBook.Country.CreateNewCountry";
        public const string Pages_Tenant_AddressBook_Country_EditCountry = "Pages.Tenant.AddressBook.Country.EditCountry";
        public const string Pages_Tenant_AddressBook_Country_DeleteCountry = "Pages.Tenant.AddressBook.Country.DeleteCountry";
        public const string Pages_Tenant_AddressBook_Region = "Pages.Tenant.AddressBook.Region";
        public const string Pages_Tenant_AddressBook_Region_CreateNewRegion = "Pages.Tenant.AddressBook.Region.CreateNewRegion";
        public const string Pages_Tenant_AddressBook_Region_EditRegion = "Pages.Tenant.AddressBook.Region.EditRegion";
        public const string Pages_Tenant_AddressBook_Region_DeleteRegion = "Pages.Tenant.AddressBook.Region.DeleteRegion";
        //Product
        public const string Pages_Tenant_Product = "Pages.Tenant.Product";
        public const string Pages_Tenant_Product_Products = "Pages.Tenant.Product.Products";
        public const string Pages_Tenant_Product_Products_CreateProduct = "Pages.Tenant.Product.Products.CreateProducts";
        public const string Pages_Tenant_Product_Products_EditProduct = "Pages.Tenant.Product.Products.EditProducts";
        public const string Pages_Tenant_Product_Products_DeleteProduct = "Pages.Tenant.Product.Products.DeleteProducts";
        public const string Pages_Tenant_Product_ProductClassification = "Pages.Tenant.Product.ProductClassification";
        public const string Pages_Tenant_Product_ProductClassification_CreateNewProductClassification="Pages.Tenant.Product.ProductClassification.CreateNewProductClassification";
        public const string Pages_Tenant_Product_ProductClassification_EditProductClassification = "Pages.Tenant.Product.ProductClassification.EditProductClassification";
        public const string Pages_Tenant_Product_ProductClassification_DeleteProductClassification = "PagesTenant.Product.ProductClassification.DeleteProductClassification";
        //Review
        //public const string Pages_Tenant_Review_Activity = "Pages.Tenant.Review.Activity";
        //public const string Pages_Tenant_Review_Activity_CreateActivity = "Pages.Tenant.Review.Activity.CreateActivity";
        //public const string Pages_Tenant_Review_Activity_EditActivity = "Pages.Tenant.Review.Activity.EditActivity";
        //public const string Pages_Tenant_Review_Activity_DeleteActivity = "Pages.Tenant.Review.Activity.DeleteActivity";
        public const string Pages_Tenant_Announcement = "Pages.Tenant.Accouncement";
        public const string Pages_Tenant_Announcement_CreateAnnouncement = "Pages.Tenant.Announcement.CreateNewAnnouncement";
        public const string Pages_Tenant_Announcement_EditAnnouncement = "Pages.Tenant.Announcement.EditAnnouncement";
        public const string Pages_Tenant_Announcement_DeleteAnnouncement = "Pages.Tenant.Announcement.DeleteAnnouncement";
        
        //Requisitions
        public const string Pages_Tenant_Requisitions = "Pages.Tenant.Requisitions";
        public const string Pages_Tenant_Requisitions_CreateNewRequisitions = "Pages.Tenant.Requisitions.CreateNewRequisitions";
        public const string Pages_Tenant_Requisitions_EditRequisitions = "Pages.Tenant.Requisitions.EditRequisitions";
        public const string Pages_Tenant_Requisitions_DeleteRequisitions = "Pages.Tenant.Requisitions.DeleteRequisitions";
        //LeadManagement
        public const string Pages_Tenant_LeadManagement = "Pages.Tenant.LeadManagement";
        //Approval
        public const string Pages_Tenant_Approval = "Pages.Tenant.Approval";
        public const string Pages_Tenant_Approval_SaleApproval = "Pages.Tenant.Approval.SaleApproval";
        public const string Pages_Tenant_Approval_JunkApproval = "Pages.Tenant.Approval.JunkApproval";
        //UserService
        //public const string Pages_Tenant_UserService = "Pages.Tenant.UserService";
        public const string Pages_Tenant_Employee = "Pages.Tenant.Employee";
        public const string Pages_Tenant_Employee_CreateEmployee = "Pages.Tenant.Employee.CreateNewEmployee";
        public const string Pages_Tenant_Employee_EditEmployee = "Pages.Tenant.Employee.EditEmployee";
        public const string Pages_Tenant_Employee_DeleteEmployee = "Pages.Tenant.Employee.DeleteEmployee";
        public const string Pages_Tenant_NewCustomer = "Pages.Tenant.NewCustomer";
    }
}