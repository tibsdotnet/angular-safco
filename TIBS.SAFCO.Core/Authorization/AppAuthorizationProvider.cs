﻿using Abp.Authorization;
using Abp.Localization;
using Abp.MultiTenancy;

namespace TIBS.SAFCO.Authorization
{
    /// <summary>
    /// Application's authorization provider.
    /// Defines permissions for the application.
    /// See <see cref="AppPermissions"/> for all permission names.
    /// </summary>
    public class AppAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

            var pages = context.GetPermissionOrNull(AppPermissions.Pages) ?? context.CreatePermission(AppPermissions.Pages, L("Pages"));

            var administration = pages.CreateChildPermission(AppPermissions.Pages_Administration, L("Administration"));
            
            var roles = administration.CreateChildPermission(AppPermissions.Pages_Administration_Roles, L("Roles"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Create, L("CreatingNewRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Edit, L("EditingRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Delete, L("DeletingRole"));
            
            var users = administration.CreateChildPermission(AppPermissions.Pages_Administration_Users, L("Users"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Create, L("CreatingNewUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Edit, L("EditingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Delete, L("DeletingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_ChangePermissions, L("ChangingPermissions"));

            var languages = administration.CreateChildPermission(AppPermissions.Pages_Administration_Languages, L("Languages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Create, L("CreatingNewLanguage"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Edit, L("EditingLanguage"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Delete, L("DeletingLanguages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_ChangeTexts, L("ChangingTexts"));

            administration.CreateChildPermission(AppPermissions.Pages_Administration_AuditLogs, L("AuditLogs"));

            //TENANT-SPECIFIC PERMISSIONS

            var dash = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Tenant);
            var dashb = dash.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_Dashboards, L("Dashboard"), multiTenancySides: MultiTenancySides.Tenant);
            var activityover = dash.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_ActivityOverView, L("ActivityOverView"), multiTenancySides: MultiTenancySides.Tenant);
            var nb = dash.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard_NoticeBoard, L("NoticeBoard"), multiTenancySides: MultiTenancySides.Tenant);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Tenant);

            //HOST-SPECIFIC PERMISSIONS

            var editions = pages.CreateChildPermission(AppPermissions.Pages_Editions, L("Editions"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Create, L("CreatingNewEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Edit, L("EditingEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Delete, L("DeletingEdition"), multiTenancySides: MultiTenancySides.Host);

            var tenants = pages.CreateChildPermission(AppPermissions.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Create, L("CreatingNewTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Edit, L("EditingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_ChangeFeatures, L("ChangingFeatures"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Delete, L("DeletingTenant"), multiTenancySides: MultiTenancySides.Host);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Host);

            //AddressBook
            var add = pages.CreateChildPermission(AppPermissions.Pages_Tenant_AddressBook, L("AddressBook"), multiTenancySides: MultiTenancySides.Tenant);
            var cc = add.CreateChildPermission(AppPermissions.Pages_Tenant_AddressBook_Company, L("Company"), multiTenancySides: MultiTenancySides.Tenant);
            cc.CreateChildPermission(AppPermissions.Pages_Tenant_AddressBook_Company_CreateNewCompany, L("CreateCompany"), multiTenancySides: MultiTenancySides.Tenant);
            cc.CreateChildPermission(AppPermissions.Pages_Tenant_AddressBook_Company_EditCompany, L("EditCompany"), multiTenancySides: MultiTenancySides.Tenant);
            cc.CreateChildPermission(AppPermissions.Pages_Tenant_AddressBook_Company_DeleteCompany, L("DeleteCompany"), multiTenancySides: MultiTenancySides.Tenant);
            var ccc = add.CreateChildPermission(AppPermissions.Pages_Tenant_AddressBook_CompetitorCompany, L("CompetitorCompany"), multiTenancySides: MultiTenancySides.Tenant);
            ccc.CreateChildPermission(AppPermissions.Pages_Tenant_AddressBook_CompetitorCompany_CreateNewCompetitorCompany, L("CreateCompetitorCompany"), multiTenancySides: MultiTenancySides.Tenant);
            ccc.CreateChildPermission(AppPermissions.Pages_Tenant_AddressBook_CompetitorCompany_EditCompetitorCompany, L("EditCompetitorCompany"), multiTenancySides: MultiTenancySides.Tenant);
            ccc.CreateChildPermission(AppPermissions.Pages_Tenant_AddressBook_CompetitorCompany_DeleteCompetitorCompany, L("DeleteCompetitorCompany"), multiTenancySides: MultiTenancySides.Tenant);
            var jk = add.CreateChildPermission(AppPermissions.Pages_Tenant_AddressBook_JunkCompanies, L("JunkCompany"), multiTenancySides: MultiTenancySides.Tenant);
            var cou = add.CreateChildPermission(AppPermissions.Pages_Tenant_AddressBook_Country, L("Country"), multiTenancySides: MultiTenancySides.Tenant);
            cou.CreateChildPermission(AppPermissions.Pages_Tenant_AddressBook_Country_CreateNewCountry, L("CreateCountry"), multiTenancySides: MultiTenancySides.Tenant);
            cou.CreateChildPermission(AppPermissions.Pages_Tenant_AddressBook_Country_EditCountry, L("EditCountry"), multiTenancySides: MultiTenancySides.Tenant);
            cou.CreateChildPermission(AppPermissions.Pages_Tenant_AddressBook_Country_DeleteCountry, L("DeleteCountry"), multiTenancySides: MultiTenancySides.Tenant);
            var rgn = add.CreateChildPermission(AppPermissions.Pages_Tenant_AddressBook_Region, L("Region"), multiTenancySides: MultiTenancySides.Tenant);
            rgn.CreateChildPermission(AppPermissions.Pages_Tenant_AddressBook_Region_CreateNewRegion, L("CreateNewRegion"), multiTenancySides: MultiTenancySides.Tenant);
            rgn.CreateChildPermission(AppPermissions.Pages_Tenant_AddressBook_Region_EditRegion, L("EditRegion"), multiTenancySides: MultiTenancySides.Tenant);
            rgn.CreateChildPermission(AppPermissions.Pages_Tenant_AddressBook_Region_DeleteRegion, L("DeleteRegion"), multiTenancySides: MultiTenancySides.Tenant);
            //Product
            var poduct = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Product, L("Product"), multiTenancySides: MultiTenancySides.Tenant);
            var poductclassification = poduct.CreateChildPermission(AppPermissions.Pages_Tenant_Product_ProductClassification, L("ProductClassification"), multiTenancySides: MultiTenancySides.Tenant);
            poductclassification.CreateChildPermission(AppPermissions.Pages_Tenant_Product_ProductClassification_CreateNewProductClassification, L("CreateNewCreateNewProductClassification"), multiTenancySides: MultiTenancySides.Tenant);
            poductclassification.CreateChildPermission(AppPermissions.Pages_Tenant_Product_ProductClassification_EditProductClassification, L("EditProductClassification"), multiTenancySides: MultiTenancySides.Tenant);
            poductclassification.CreateChildPermission(AppPermissions.Pages_Tenant_Product_ProductClassification_DeleteProductClassification, L("DeleteProductClassification"), multiTenancySides: MultiTenancySides.Tenant);
            var poducts = poduct.CreateChildPermission(AppPermissions.Pages_Tenant_Product_Products, L("Products"), multiTenancySides: MultiTenancySides.Tenant);
            poducts.CreateChildPermission(AppPermissions.Pages_Tenant_Product_Products_CreateProduct, L("CreateNewProducts"), multiTenancySides: MultiTenancySides.Tenant);
            poducts.CreateChildPermission(AppPermissions.Pages_Tenant_Product_Products_EditProduct, L("EditProducts"), multiTenancySides: MultiTenancySides.Tenant);
            poducts.CreateChildPermission(AppPermissions.Pages_Tenant_Product_Products_DeleteProduct, L("DeleteProducts"), multiTenancySides: MultiTenancySides.Tenant);
            //Review
            //var activity = review.CreateChildPermission(AppPermissions.Pages_Tenant_Review_Activity, L("Activity"), multiTenancySides: MultiTenancySides.Tenant);
            //activity.CreateChildPermission(AppPermissions.Pages_Tenant_Review_Activity_CreateActivity, L("CreateActivity"), multiTenancySides: MultiTenancySides.Tenant);
            //activity.CreateChildPermission(AppPermissions.Pages_Tenant_Review_Activity_EditActivity, L("EditActivity"), multiTenancySides: MultiTenancySides.Tenant);
            //activity.CreateChildPermission(AppPermissions.Pages_Tenant_Review_Activity_DeleteActivity, L("DeleteActivity"), multiTenancySides: MultiTenancySides.Tenant);
            var ann = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Announcement, L("Announcement"), multiTenancySides: MultiTenancySides.Tenant);
            ann.CreateChildPermission(AppPermissions.Pages_Tenant_Announcement_CreateAnnouncement, L("CreateAccounctment"), multiTenancySides: MultiTenancySides.Tenant);
            ann.CreateChildPermission(AppPermissions.Pages_Tenant_Announcement_EditAnnouncement, L("EditAnnouncement"), multiTenancySides: MultiTenancySides.Tenant);
            ann.CreateChildPermission(AppPermissions.Pages_Tenant_Announcement_DeleteAnnouncement, L("DeleteAnnouncement"), multiTenancySides: MultiTenancySides.Tenant);
            
            //Requisitions
            var req = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Requisitions, L("Requisitions"), multiTenancySides: MultiTenancySides.Tenant);
            req.CreateChildPermission(AppPermissions.Pages_Tenant_Requisitions_CreateNewRequisitions, L("CreateRequisitions"), multiTenancySides: MultiTenancySides.Tenant);
            req.CreateChildPermission(AppPermissions.Pages_Tenant_Requisitions_EditRequisitions, L("EditRequisitions"), multiTenancySides: MultiTenancySides.Tenant);
            req.CreateChildPermission(AppPermissions.Pages_Tenant_Requisitions_DeleteRequisitions, L("DeleteRequisitions"), multiTenancySides: MultiTenancySides.Tenant);
            //LeadManagement
            var lead = pages.CreateChildPermission(AppPermissions.Pages_Tenant_LeadManagement, L("LeadManagement"), multiTenancySides: MultiTenancySides.Tenant);
            //Approval
            var app = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Approval, L("Approval"), multiTenancySides: MultiTenancySides.Tenant);
            var saa = app.CreateChildPermission(AppPermissions.Pages_Tenant_Approval_SaleApproval, L("SaleApproval"), multiTenancySides: MultiTenancySides.Tenant);
            var jua = app.CreateChildPermission(AppPermissions.Pages_Tenant_Approval_JunkApproval, L("JunkApproval"), multiTenancySides: MultiTenancySides.Tenant);
            //UserService
            //var user = pages.CreateChildPermission(AppPermissions.Pages_Tenant_UserService, L("UserService"), multiTenancySides: MultiTenancySides.Tenant);
            var newc = pages.CreateChildPermission(AppPermissions.Pages_Tenant_NewCustomer, L("NewCustomer"), multiTenancySides: MultiTenancySides.Tenant);
            var emp = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Employee, L("Employee"), multiTenancySides: MultiTenancySides.Tenant);
            emp.CreateChildPermission(AppPermissions.Pages_Tenant_Employee_CreateEmployee, L("CreateEmployee"), multiTenancySides: MultiTenancySides.Tenant);
            emp.CreateChildPermission(AppPermissions.Pages_Tenant_Employee_EditEmployee, L("EditEmployee"), multiTenancySides: MultiTenancySides.Tenant);
            emp.CreateChildPermission(AppPermissions.Pages_Tenant_Employee_DeleteEmployee, L("DeleteEmployee"), multiTenancySides: MultiTenancySides.Tenant);

        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, SAFCOConsts.LocalizationSourceName);
        }
    }
}
