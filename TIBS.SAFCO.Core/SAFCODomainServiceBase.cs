﻿using Abp.Domain.Services;

namespace TIBS.SAFCO
{
    public abstract class SAFCODomainServiceBase : DomainService
    {
        /* Add your common members for all your domain services. */

        protected SAFCODomainServiceBase()
        {
            LocalizationSourceName = SAFCOConsts.LocalizationSourceName;
        }
    }
}
