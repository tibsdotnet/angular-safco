﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.SAFCO.CompetitorCompaniesss
{
    [Table("CompetitorCompany")]
    public class CompetitorCompany : FullAuditedEntity
    {
        [Required]
        [MaxLength(GetLength.MaxNamegLength)]
        public virtual string CompetitorCompanyName { get; set; }

        [Required]
        [MaxLength(GetLength.MaxCodeLength)]
        public virtual string CompetitorCompanyCode { get; set; }
    }
}
