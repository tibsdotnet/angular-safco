﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Compnaies;
using TIBS.SAFCO.Regions;

namespace TIBS.SAFCO.CompanyContacts
{
    [Table("Contact")]
    public class CompanyContact:FullAuditedEntity
    {
        [ForeignKey("CompanyId")]
        public virtual Company Companies { get; set; }
        public virtual int CompanyId { get; set; }
        [ForeignKey("RegionId")]
        public virtual Region Regions { get; set; }
        public virtual int? RegionId { get; set; }
        public virtual string TitleOfCourtey { get; set; }
        [Required]
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        [Required]
        public virtual string EmailId { get; set; }
        public virtual string Address { get; set; }
        public virtual string Description { get; set; }
        public virtual string Notes { get; set; }
        public virtual string Fax { get; set; }
        public virtual string POBox { get; set; }
        public virtual string ProfilePictuePath { get; set; }
        public virtual string PhoneNo { get; set; }

    }
}
