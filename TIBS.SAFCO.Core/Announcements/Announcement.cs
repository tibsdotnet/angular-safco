﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.SAFCO.Announcements
{
    [Table("Announcements")]
   public class Announcement:FullAuditedEntity
    {
        [Required]
        public virtual string Title { get; set; }
        public virtual string Description { get; set; }
        public virtual DateTime PublishedDate { get; set; }
        public virtual DateTime ExpiredDate { get; set; }
    }
}
