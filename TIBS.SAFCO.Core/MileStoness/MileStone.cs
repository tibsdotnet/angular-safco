﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.SAFCO.MileStoness
{
    [Table("MileStone")]
    public class MileStone : FullAuditedEntity
    {
        [Required]
        public virtual string MileStoneName { get; set; }
        public virtual string MileStoneDescription { get; set; }
    }
}
