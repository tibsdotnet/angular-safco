﻿using Abp.Application.Features;
using TIBS.SAFCO.Authorization.Roles;
using TIBS.SAFCO.Authorization.Users;
using TIBS.SAFCO.MultiTenancy;

namespace TIBS.SAFCO.Editions
{
    public class FeatureValueStore : AbpFeatureValueStore<Tenant, Role, User>
    {
        public FeatureValueStore(TenantManager tenantManager) 
            : base(tenantManager)
        {
        }
    }
}
