﻿using Abp.Application.Editions;

namespace TIBS.SAFCO.Editions
{
    public class EditionManager : AbpEditionManager
    {
        public const string DefaultEditionName = "Standard";
    }
}
