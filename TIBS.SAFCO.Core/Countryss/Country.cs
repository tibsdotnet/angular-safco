﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.SAFCO.Countryss
{
    [Table("Country")]
    public class Country : FullAuditedEntity
    {
        [Required]
        public virtual string CountryName { get; set; }
        public virtual string PrintCountryName { get; set; }
        [Required]
        public virtual string ISOCode { get; set; }
        public virtual string ISO3Code { get; set; }
        public virtual string ISDCode { get; set; }
        public virtual string NumericCode { get; set; }
        public virtual string Divisions { get; set; }

    }
}
