﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Authorization.Users;
using TIBS.SAFCO.Employees;
using TIBS.SAFCO.LeadSources;
using TIBS.SAFCO.LeadStatus;
using TIBS.SAFCO.Regions;

namespace TIBS.SAFCO.Compnaies
{
    [Table("Companies")]
   public class Company:FullAuditedEntity
    {
        [Required]
        public virtual string Title { get; set; }
        public virtual string Description { get; set; }
        public virtual string Address { get; set; }
        [ForeignKey("RegionId")]
        public virtual Region Regions{get;set;}
        public virtual int RegionId{get;set;}
        [ForeignKey("LeadSourceId")]
        public virtual LeadSource LeadSources { get; set; }
        public virtual int? LeadSourceId { get; set; }
        [ForeignKey("StatusId")]
        public virtual Status status { get; set; }
        public virtual int? StatusId { get; set; }
        public virtual string POBox { get; set; }
        public virtual string PhoneNo { get; set; }
        public virtual string WebSite { get; set; }
        public virtual string Fax { get; set; }
        [ForeignKey("ContributedId")]
        public virtual User AbpUser { get; set; }
        public virtual long? ContributedId { get; set; }
        public virtual DateTime PublishedTime { get; set; }

        [ForeignKey("ManagedById")]
        public virtual User AbpUser1 { get; set; }
        public virtual long? ManagedById { get; set; }

        [ForeignKey("ManagedId")]
        public virtual Employee ManagedUser { get; set; }
        public virtual int? ManagedId { get; set; }

        public virtual DateTime? Published { get; set; }

        public virtual string EmailId { get; set; }
    }
}
