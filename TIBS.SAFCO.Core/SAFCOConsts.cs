﻿namespace TIBS.SAFCO
{
    /// <summary>
    /// Some general constants for the application.
    /// </summary>
    public class SAFCOConsts
    {
        public const string LocalizationSourceName = "SAFCO";
    }
}