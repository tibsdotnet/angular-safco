﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.SAFCO.Activities
{
    [Table("Activities")]
   public class Activity:FullAuditedEntity
    {
        [Required]
        [MaxLength(GetLength.MaxCodeLength)]
        public virtual string ActivityCode { get; set; }
        [Required]
        [MaxLength(GetLength.MaxNamegLength)]
        public virtual string ActivityName { get; set; }
    }
}
