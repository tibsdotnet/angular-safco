﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.ProductClassifications;

namespace TIBS.SAFCO.Products
{
    [Table("Products")]
   public class Product:FullAuditedEntity
    {
        public virtual string ProductName { get; set; }
        public virtual string Description { get; set; }
        public virtual double MinimumSalePrice { get; set; }
        public virtual double MaximumSalePrice { get; set; }
        [ForeignKey("ProductClassificationId")]
        public virtual ProductClassification ProductClassifications { get; set; }
        public virtual int? ProductClassificationId { get; set; }
    }
}
