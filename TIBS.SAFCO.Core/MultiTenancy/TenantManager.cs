﻿using Abp.MultiTenancy;
using TIBS.SAFCO.Authorization.Roles;
using TIBS.SAFCO.Authorization.Users;
using TIBS.SAFCO.Editions;

namespace TIBS.SAFCO.MultiTenancy
{
    /// <summary>
    /// 
    /// </summary>
    public class TenantManager : AbpTenantManager<Tenant, Role, User>
    {
        public TenantManager(EditionManager editionManager)
            : base(editionManager)
        {

        }
    }
}
