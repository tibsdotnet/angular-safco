﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.CompetitorCompaniesss;
using TIBS.SAFCO.Compnaies;
using TIBS.SAFCO.Employees;
using TIBS.SAFCO.LeadSources;
using TIBS.SAFCO.MileStoness;

namespace TIBS.SAFCO.Leadss
{
    [Table("Lead")]
    public class Lead : FullAuditedEntity
    {
        public virtual string Title { get; set; }
        
        public virtual string Description { get; set; }
        [ForeignKey("CompanyId")]
        public virtual Company Companys { get; set; }
        public virtual int CompanyId { get; set; }
        
        [ForeignKey("LeadSourceId")]
        public virtual LeadSource LeadSources { get; set; }
        public virtual int? LeadSourceId { get; set; }
        
        [ForeignKey("MileStoneId")]
        public virtual MileStone MileStones { get; set; }
        public virtual int MileStoneId { get; set; }
        
        public virtual int Rating { get; set; }
        
        public virtual float ExpectedAmount { get; set; }
        
        public virtual Nullable<System.DateTime> ExpectedClosure { get; set; }
        
        public virtual float ActualAmount { get; set; }
        
        public virtual Nullable<System.DateTime> ActualClosure { get; set; }
       
        [MaxLength(GetLength.MaxOrderLength)]
        public virtual string OrderNo { get; set; }
        
        public virtual bool Archived { get; set; }
        
        public virtual Nullable<System.DateTime> Published { get; set; }
        
        [ForeignKey("EmployeeId")]
        public virtual Employee ManagedByEmployee { get; set; }
        public virtual int EmployeeId { get; set; }
       
        [ForeignKey("ChefId")]
        public virtual Employee ManagedByChef { get; set; }
        public virtual int? ChefId { get; set; }

        [ForeignKey("CompetitorId")]
        public virtual CompetitorCompany CompetitorCompanies { get; set; }
        public virtual int? CompetitorId { get; set; }
    }
}
