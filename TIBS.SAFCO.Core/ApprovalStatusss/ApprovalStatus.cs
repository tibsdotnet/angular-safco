﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.SAFCO.ApprovalStatusss
{
    [Table("ApprovalStatus")]
    public class ApprovalStatus : FullAuditedEntity
    {
        public virtual string ApprovalStatusCode { get; set; }
        public virtual string ApprovalStatusName { get; set; }
    }
}
