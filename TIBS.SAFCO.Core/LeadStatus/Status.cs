﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.SAFCO.LeadStatus
{
    [Table("Status")]
   public class Status:FullAuditedEntity
    {
        public virtual string StatusCode { get; set; }
        public virtual string StatusName { get; set; }
    }
}
