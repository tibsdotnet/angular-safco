﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Countryss;

namespace TIBS.SAFCO.Regions
{
    [Table("Regions")]
    public class Region : FullAuditedEntity
    {
        [Required]
        [MaxLength(GetLength.MaxCodeLength)]
        public virtual string Code { get; set; }
        [Required]
        [MaxLength(GetLength.MaxNamegLength)]
        public virtual string Name { get; set; }
        [ForeignKey("CountryId")]
        public virtual Country Countries { get; set; }
        public virtual int CountryId { get; set; }
        public virtual string Type { get; set; }
    }
}
