﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Authorization.Roles;
using TIBS.SAFCO.Authorization.Users;
using TIBS.SAFCO.Regions;

namespace TIBS.SAFCO.Employees
{
    [Table("Employees")]
   public class Employee:FullAuditedEntity
    {
        public virtual string LastName { get; set; }
        public virtual string FirstName { get; set; }
        [ForeignKey("RoleId")]
        public virtual Role Roles { get; set; }

        public virtual int? RoleId { get; set; }
        public virtual string TitleOfCourtesy { get; set; }
        public virtual Nullable<System.DateTime> BirthDate { get; set; }
        public virtual Nullable<System.DateTime> HireDate { get; set; }
        public virtual string Address { get; set; }
        [ForeignKey("RegionId")]
        public virtual Region Regions { get; set; }
        public virtual int? RegionId { get; set; }
        public virtual string POBoxNo { get; set; }
        public virtual string Phone { get; set; }
        public virtual string Extension { get; set; }
        
        public virtual string Notes { get; set; }
        
        public string PhotoPath { get; set; }
        
        public Nullable<System.DateTime> Published { get; set; }
        public bool Archived { get; set; }
       
        public string Email { get; set; }

        [ForeignKey("UserId")]
        public virtual User accountUser { get; set; }
        public virtual long? UserId { get; set; }
    }
}
