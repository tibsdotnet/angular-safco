﻿namespace TIBS.SAFCO.Emailing
{
    public interface IEmailTemplateProvider
    {
        string GetDefaultTemplate();
    }
}
