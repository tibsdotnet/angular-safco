﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.SAFCO.ProductClassifications
{
    [Table("ProductClassification")]
    public class ProductClassification:FullAuditedEntity
    {
        public virtual string ProductClassificationCode { get; set; }
        public virtual string ProductClassificationName { get; set; }
    }
}
