﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Activities;
using TIBS.SAFCO.Authorization.Users;
using TIBS.SAFCO.Leadss;

namespace TIBS.SAFCO.LeadActivities
{
    [Table("LeadActivity")]
    public class LeadActivity:FullAuditedEntity
    {
        public virtual string Notes { get; set; }
        public virtual string Title { get; set; }
        [ForeignKey("LeadId")]
        public virtual Lead Leads { get; set; }
        public virtual int? LeadId { get; set; }
        [ForeignKey("ActivityId")]
        public virtual Activity Activities { get; set; }
        public virtual int? ActivityId { get; set; }
        [ForeignKey("UserId")]
        public virtual User AbpUser { get; set; }
        public virtual long? UserId { get; set; }
    }
}
