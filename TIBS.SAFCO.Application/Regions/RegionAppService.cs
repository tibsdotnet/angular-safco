﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Countryss;
using TIBS.SAFCO.Regions.Dto;
using Abp.Linq.Extensions;
using Abp.Extensions;
using Abp.Linq;
using Abp.AutoMapper;
using Abp.UI;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.Regions.Export;
using Abp.Authorization;
using TIBS.SAFCO.Authorization;
using System.Data.Entity;
using System.Linq.Dynamic;

namespace TIBS.SAFCO.Regions
{
    public class RegionAppService:SAFCOAppServiceBase,IRegionAppService
    {
        private readonly IRepository<Region> _regionrespository;
        private readonly IRepository<Country> _countrypository;
        private readonly IRegionListExporter _RegionListExporter;
        public RegionAppService(IRepository<Region> regionrepository, IRepository<Country> countryrepository, IRegionListExporter RegionListExporter)
        {
            _regionrespository = regionrepository;
            _countrypository = countryrepository;
            _RegionListExporter = RegionListExporter;
        }
        public async Task<PagedResultOutput<Region_ViewDto>> GetRegion(GetRegionInput input)
        {
            var regionquery = _regionrespository.GetAll().WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Name.Contains(input.Filter) ||
                         p.Code.Contains(input.Filter) ||
                         p.Countries.CountryName.Contains(input.Filter) ||
                         p.Type.Contains(input.Filter)

                );
            var regiondto=(from c in regionquery select new Region_ViewDto{Name=c.Name,Code=c.Code,CountryName=c.Countries.CountryName,Type=c.Type,Id=c.Id});
            var regioncount = await regiondto.CountAsync();
            var regiond = await regiondto
           .OrderBy(input.Sorting)
           .PageBy(input)
           .ToListAsync();
            var regiondtos = regiond.MapTo<List<Region_ViewDto>>();
            return new PagedResultOutput<Region_ViewDto>(regioncount,regiondtos);
        }
        public async Task<FileDto> GetRegionToExcel()
        {

            var region = _regionrespository
                .GetAll().ToList();
            var regiondto = (from c in region select new Region_ViewDto { Name = c.Name, Code = c.Code, CountryName = c.Countries.CountryName, Type = c.Type, Id = c.Id });
            var regionListDtos = regiondto.MapTo<List<Region_ViewDto>>();


            return _RegionListExporter.ExportToFile(regionListDtos);
        }
        public async Task<GetRegion> GetRegionForEdit(NullableIdInput<long> input)
        {
            

            var region = _regionrespository
                .GetAll().Where(p => p.Id == input.Id).FirstOrDefault();
            var countryname = _countrypository.GetAll().Select(r => new RegionCountryDto { CountryId = r.Id, CountryName = r.CountryName }).ToArray();
            var output = new GetRegion
            {
                CountryName=countryname
            };
            output.Regions = region.MapTo<CreateRegionInput>();

            return output;

        }
        public async Task CreateOrUpdateRegion(CreateRegionInput input)
        {
            if (input.Id != 0)
            {
                await UpdateRegion(input);
            }
            else
            {
                await CreateRegion(input);
            }
        }
         [AbpAuthorize(AppPermissions.Pages_Tenant_AddressBook_Region_CreateNewRegion)]
        public async Task CreateRegion(CreateRegionInput input)
        {
            var regioncount = _regionrespository.GetAll().Where(p => p.Name == input.Name || p.Code == input.Code).Count();
            if(regioncount<=0)
            {
                await _regionrespository.InsertAsync(input.MapTo<Region>());
            }
            else
            {
                throw new UserFriendlyException("Ooops!", "Duplicate Data Occured in Region Name '" + input.Name + "' or Region Code '" + input.Code + "'...");
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_AddressBook_Region_EditRegion)]
        public async Task UpdateRegion(CreateRegionInput input)
        {
            var regioncount = _regionrespository.GetAll().Where(p => (p.Name == input.Name || p.Code == input.Code) && input.Id != p.Id).Count();
            if(regioncount<=0)
            {
                await _regionrespository.UpdateAsync(input.MapTo<Region>());
            }
            else
            {
                throw new UserFriendlyException("Ooops!", "Duplicate Data Occured in Region Name '" + input.Name + "' or Region Code '" + input.Code + "'...");
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_AddressBook_Region_DeleteRegion)]
        public async Task DeleteRegion(IdInput input)
        {
            await _regionrespository.DeleteAsync(input.Id);
        }
    }
}
