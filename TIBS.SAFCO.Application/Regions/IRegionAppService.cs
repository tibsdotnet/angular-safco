﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.Regions.Dto;

namespace TIBS.SAFCO.Regions
{
    public interface IRegionAppService:IApplicationService
    {
        Task<PagedResultOutput<Region_ViewDto>> GetRegion(GetRegionInput input);
        Task<GetRegion> GetRegionForEdit(NullableIdInput<long> input);
        Task CreateOrUpdateRegion(CreateRegionInput input);
        Task DeleteRegion(IdInput input);
        Task<FileDto> GetRegionToExcel();
    }
}
