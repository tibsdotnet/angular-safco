﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.Regions.Dto;

namespace TIBS.SAFCO.Regions.Export
{
    public interface IRegionListExporter
    {
        FileDto ExportToFile(List<Region_ViewDto> regionListDtos);
    }
}
