﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.DataExporting.Excel.EpPlus;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.Regions.Dto;

namespace TIBS.SAFCO.Regions.Export
{
    public class RegionListExporter : EpPlusExcelExporterBase, IRegionListExporter
    {
        public FileDto ExportToFile(List<Region_ViewDto> regionListDtos)
        {
            return CreateExcelPackage(
                "Region.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Region"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("RegionCode"),
                        L("RegionName"),
                        L("CountryName")
                    );

                    AddObjects(
                        sheet, 2, regionListDtos,
                        _ => _.Code,
                        _ => _.Name,
                        _ => _.CountryName

                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(3);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 3; i++)
                    {

                        sheet.Column(i).AutoFit();
                    }
                });
        }

    }
}
