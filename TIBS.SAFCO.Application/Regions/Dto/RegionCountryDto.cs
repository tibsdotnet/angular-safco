﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.SAFCO.Regions.Dto
{
   public class RegionCountryDto
    {
       public int CountryId { get; set; }
       public string CountryName { get; set; }
    }
}
