﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using System.ComponentModel.DataAnnotations;
namespace TIBS.SAFCO.Regions.Dto
{
    [AutoMapTo(typeof(Region))]
    public class CreateRegionInput : IInputDto
    {
        [Required]
        [MaxLength(GetLength.MaxCodeLength)]
        public virtual string Code { get; set; }
        [Required]
        [MaxLength(GetLength.MaxNamegLength)]
        public virtual string Name { get; set; }
        
        public virtual int CountryId { get; set; }
        public virtual string Type { get; set; }
        public int Id { get; set; }
    }
}
