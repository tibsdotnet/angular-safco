﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;

namespace TIBS.SAFCO.Regions.Dto
{
    [AutoMapFrom(typeof(Region))]
   public class Region_ViewDto : FullAuditedEntityDto
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string CountryName { get; set; }
        public string Type { get; set; }
        public int Id { get; set; }
    }
}
