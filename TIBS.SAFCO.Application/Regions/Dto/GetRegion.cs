﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.SAFCO.Regions.Dto
{
    public class GetRegion : IOutputDto
    {
        public CreateRegionInput Regions { get; set; }
        public RegionCountryDto[] CountryName { get; set; }
    }
}
