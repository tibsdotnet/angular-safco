﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Activities.Dto;
using System.Data.Entity;
using Abp.Linq.Extensions;
using Abp.Linq;
using Abp.Extensions;
using Abp.AutoMapper;
using Abp.UI;
using Abp.Authorization;
using TIBS.SAFCO.Authorization;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.Activities.Export;

namespace TIBS.SAFCO.Activities
{
    public class ActivityAppService:SAFCOAppServiceBase,IActivityAppService
    {
        private readonly IRepository<Activity> _ActivityRepository;
        private readonly IActivityListExcel _ActivityListExcel;
        public ActivityAppService(IRepository<Activity> ActivityRepository, IActivityListExcel ActivityListExcel)
        {
            _ActivityRepository = ActivityRepository;
            _ActivityListExcel = ActivityListExcel;
        }
        public ListResultOutput<ActivityListDto> GetActivity(GetActivityInput input)
        {
            var Activity = _ActivityRepository
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Id.ToString().Contains(input.Filter) ||
                        p.ActivityCode.Contains(input.Filter) ||
                         p.ActivityName.Contains(input.Filter)

                )
                .OrderBy(p => p.ActivityName)
                .ThenBy(p => p.ActivityCode)
                .ToList();

            return new ListResultOutput<ActivityListDto>(Activity.MapTo<List<ActivityListDto>>());
        }

        public async Task<FileDto> GetActivityToExcel()
        {

            var activity = _ActivityRepository
                .GetAll().ToList();
            var activityListDtos = activity.MapTo<List<ActivityListDto>>();


            return _ActivityListExcel.ExportToFile(activityListDtos);
        }


        public async Task<GetActivity> GetActivityForEdit(NullableIdInput<long> input)
        {
            var output = new GetActivity
            {
            };

            var Activity = await _ActivityRepository
                .GetAll().Where(p => p.Id == input.Id).FirstOrDefaultAsync();

            output.Activity = Activity.MapTo<CreateActivityInput>();

            return output;

        }

        public async Task CreateOrUpdateUser(CreateActivityInput input)
        {
            if (input.Id != 0)
            {
                await UpdateActivityAsync(input);
            }
            else
            {
                await CreateActivityAsync(input);
            }
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_Review_Activity_CreateActivity)]
        public virtual async Task CreateActivityAsync(CreateActivityInput input)
        {
            var Activity = input.MapTo<Activity>();
            DateTime myUtcDateTime = DateTime.Now;

            Activity.LastModificationTime = DateTime.Now.AddHours(-12);
            var val = _ActivityRepository
              .GetAll().Where(p => p.ActivityCode == input.ActivityCode || p.ActivityName == input.ActivityName).FirstOrDefault();
            if (val == null)
            {
                await _ActivityRepository.InsertAsync(Activity);
            }
            else
            {
                throw new UserFriendlyException("Ooops!", "Duplicate Data Occured in Activity Name '" + input.ActivityName + "' or Activity Code '" + input.ActivityCode + "'...");
            }
        }
//[AbpAuthorize(AppPermissions.Pages_Tenant_Review_Activity_EditActivity)]
        public virtual async Task UpdateActivityAsync(CreateActivityInput input)
        {
            var Activity = input.MapTo<Activity>();
            Activity.ActivityName = input.ActivityName;
            Activity.ActivityCode = input.ActivityCode;
            ;
            var val = _ActivityRepository
              .GetAll().Where(p => (p.ActivityCode == input.ActivityCode || p.ActivityName == input.ActivityName) && p.Id != input.Id).FirstOrDefault();
            if (val == null)
            {
                await _ActivityRepository.UpdateAsync(Activity);
            }
            else
            {
                throw new UserFriendlyException("Ooops!", "Duplicate Data Occured in Activity Name '" + input.ActivityName + "' or Activity Code '" + input.ActivityCode + "'...");
            }
        }
       // [AbpAuthorize(AppPermissions.Pages_Tenant_Review_Activity_DeleteActivity)]
        public async Task DeleteActivity(IdInput input)
        {

            await _ActivityRepository.DeleteAsync(input.Id);

        }    
    }

}
