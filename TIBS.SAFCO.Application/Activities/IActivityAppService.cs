﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Activities.Dto;
using TIBS.SAFCO.Dto;

namespace TIBS.SAFCO.Activities
{
   public interface IActivityAppService:IApplicationService
    {
       ListResultOutput<ActivityListDto> GetActivity(GetActivityInput input);
       Task<GetActivity> GetActivityForEdit(NullableIdInput<long> input);
       Task CreateOrUpdateUser(CreateActivityInput input);
       Task DeleteActivity(IdInput input);
       Task<FileDto> GetActivityToExcel();
    }
}
