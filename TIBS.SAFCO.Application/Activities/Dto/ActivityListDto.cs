﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.SAFCO.Activities.Dto
{
    [AutoMapFrom(typeof(Activity))]
    public class ActivityListDto:FullAuditedEntityDto
    {
        public string ActivityCode{get;set;}
        public string ActivityName{get;set;}
         
        public int Id{get;set;}
    }
}
