﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;

namespace TIBS.SAFCO.Activities.Dto
{
    [AutoMapTo(typeof(Activity))]
   public class CreateActivityInput:IInputDto
    {
        public string ActivityCode { get; set; }

        public string ActivityName { get; set; }
        public int Id { get; set; }
    }
}
