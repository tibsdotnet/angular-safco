﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.SAFCO.Activities.Dto
{
    public class GetActivity:IOutputDto
    {
        public CreateActivityInput Activity { get; set; }
    }
}
