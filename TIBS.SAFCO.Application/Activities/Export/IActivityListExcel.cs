﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Activities.Dto;
using TIBS.SAFCO.Dto;

namespace TIBS.SAFCO.Activities.Export
{
    public interface IActivityListExcel
    {
        FileDto ExportToFile(List<ActivityListDto> activityListDtos);
    }
}
