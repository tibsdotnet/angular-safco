﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Activities.Dto;
using TIBS.SAFCO.DataExporting.Excel.EpPlus;
using TIBS.SAFCO.Dto;

namespace TIBS.SAFCO.Activities.Export
{
    public class ActivityListExcel : EpPlusExcelExporterBase, IActivityListExcel
    {
        public FileDto ExportToFile(List<ActivityListDto> activityListDtos)
        {
            return CreateExcelPackage(
                "Activity.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Activity"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("ActivityCode"),
                        L("ActivityName")
                    );

                    AddObjects(
                        sheet, 2, activityListDtos,
                        _ => _.ActivityCode,
                        _ => _.ActivityName
                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(2);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 2; i++)
                    {

                        sheet.Column(i).AutoFit();
                    }
                });
        }

    }
}
