﻿using System.Threading.Tasks;
using Abp.Application.Services;
using TIBS.SAFCO.Sessions.Dto;

namespace TIBS.SAFCO.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
