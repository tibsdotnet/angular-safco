﻿using System.Reflection;
using Abp.AutoMapper;
using Abp.Modules;
using TIBS.SAFCO.Authorization;

namespace TIBS.SAFCO
{
    /// <summary>
    /// Application layer module of the application.
    /// </summary>
    [DependsOn(typeof(SAFCOCoreModule), typeof(AbpAutoMapperModule))]
    public class SAFCOApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            //Adding authorization providers
            Configuration.Authorization.Providers.Add<AppAuthorizationProvider>();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());

            //Custom DTO auto-mappings
            CustomDtoMapper.CreateMappings();
        }
    }
}
