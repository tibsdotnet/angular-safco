﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Authorization.Users.Dto;

namespace TIBS.SAFCO.JunkApproval.Dto
{
    public class GetCompany:IOutputDto
    {
        public CompanyDtoInput Company { get; set; }
        public UserListDto[] User { get; set; }
    }
}
