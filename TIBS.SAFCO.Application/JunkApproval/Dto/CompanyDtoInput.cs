﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Authorization.Users.Dto;

namespace TIBS.SAFCO.JunkApproval.Dto
{
   public class CompanyDtoInput:FullAuditedEntityDto
    {
       public int CompanyId { get; set; }
       public string CompanyName { get; set; }
       public string PresentAllotedPerson { get; set; }
       public string Address { get; set; }
       public string AllotedPeson { get; set; }
       

    }
}
