﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using Abp.Application.Services.Dto;

namespace TIBS.SAFCO.JunkApproval.Dto
{
    public class JunkApprovalViewDto:FullAuditedEntityDto
    {
        public string CompanyName { get; set; }
        public string Notes { get; set; }
        public string Status { get; set; }
        public string ContributedBy { get; set; }
        public string ManagedBy { get; set; }
        public int CompanyId { get; set; }
    }
}
