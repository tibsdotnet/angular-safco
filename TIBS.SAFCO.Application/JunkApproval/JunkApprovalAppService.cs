﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.JunkApproval.Dto;
using TIBS.SAFCO.Leadss;
using Abp.AutoMapper;
using Abp.Linq.Extensions;
using Abp.Extensions;
using Abp.Linq;
using System.Data.Entity;
using TIBS.SAFCO.Compnaies;
using TIBS.SAFCO.Companies.Dto;
using TIBS.SAFCO.JunkApproval.Export;
using TIBS.SAFCO.Dto;
using System.Linq.Dynamic;

namespace TIBS.SAFCO.JunkApproval
{
    public class JunkApprovalAppService:SAFCOAppServiceBase,IJunkApprovalAppService
    {
        private readonly IRepository<Lead> _LeadRepository;
        private readonly IRepository<Company> _CompanyRepository;
        private readonly IJunkAppListExcel _JunkAppListExcel;
        public JunkApprovalAppService(IJunkAppListExcel JunkAppListExcel, IRepository<Lead> LeadRepository, IRepository<Company> CompanyRepository)
        {
            _LeadRepository = LeadRepository;
            _CompanyRepository = CompanyRepository;
            _JunkAppListExcel = JunkAppListExcel;
        }
        public async Task<PagedResultOutput<JunkApprovalViewDto>> GetJunkCompanies(GetJunkApprovalInput input)
        {
            var companyquery = _LeadRepository.GetAll().Where(p => p.MileStoneId == 7)
                            .WhereIf(
                            !input.Filter.IsNullOrWhiteSpace(),
                            p =>
                                p.Companys.Title.Contains(input.Filter) ||



                                p.MileStones.MileStoneName.Contains(input.Filter) ||

                                p.Description.Contains(input.Filter)


                            );
            var company = from l in companyquery join c in _CompanyRepository.GetAll() on l.CompanyId equals c.Id where c.StatusId != 5 select new { c, l };

            var companycount = await companyquery.CountAsync();
            var companydtos = from p in company select new JunkApprovalViewDto { Id = p.l.Id, CompanyId = p.c.Id, CompanyName = p.c.Title, Status = p.l.MileStoneId != null ? p.l.MileStones.MileStoneName : "", ManagedBy = p.c.ManagedUser.FirstName, ContributedBy = p.c.AbpUser.UserName };
            var companydto = (await companydtos
                                   .OrderBy(input.Sorting)
                                   .PageBy(input)
                                   .ToListAsync());
            var com = companydto.MapTo<List<JunkApprovalViewDto>>();
            return new PagedResultOutput<JunkApprovalViewDto>(companycount, com);
        }

        public async Task<FileDto> GetJunkApprovalToExcel()
        {

            var lead = _LeadRepository
                .GetAll().Where(p => p.MileStoneId == 7);
            var company = from l in lead join c in _CompanyRepository.GetAll() on l.CompanyId equals c.Id where c.StatusId != 5 select new { c, l };
            var companydtos = from p in company select new JunkApprovalViewDto { Id = p.l.Id, CompanyId = p.c.Id, CompanyName = p.c.Title, Status = p.l.MileStoneId != null ? p.l.MileStones.MileStoneName : "", ManagedBy = p.c.ManagedUser.FirstName, ContributedBy = p.c.AbpUser.UserName };
            var junkAppListDto = companydtos.MapTo<List<JunkApprovalViewDto>>();
            return _JunkAppListExcel.ExportToFile(junkAppListDto);
        }

        public async Task UpadteCompanyJunk(int Id)
        {
            var company = await _CompanyRepository.GetAll().Where(p => p.Id == Id).FirstOrDefaultAsync();
            company.StatusId = 5;
            var companydto = company.MapTo<Company>();
            await _CompanyRepository.UpdateAsync(companydto);
            var lead = _LeadRepository.GetAll().Where(p => p.CompanyId == Id).FirstOrDefault();
            lead.Archived = true;
            var leadinput = lead.MapTo<Lead>();
            await _LeadRepository.UpdateAsync(leadinput);
        }
        public async Task UpdateCompany(CreateCompanyInput input)
        {
            var comapnyinput = input.MapTo<Company>();
            await _CompanyRepository.UpdateAsync(comapnyinput);
        }
        //public async Task<CompanyDtoInput> GetCompanyForEdit(NullableIdInput<long> input)
        //{
        //    var company = await _CompanyRepository.GetAll().Where(p => p.Id == input.Id).ToListAsync();
        //    var output = from c in company select new CompanyDtoInput { CompanyId = c.Id, CompanyName = c.Title, AllotedPeson = c.ManagedUser.FirstName, Address = c.Address };
        //    var user=
        //    return output;


        //}
    }
}
