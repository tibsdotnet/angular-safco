﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.JunkApproval.Dto;

namespace TIBS.SAFCO.JunkApproval
{
    public interface IJunkApprovalAppService:IApplicationService
    {
        Task<PagedResultOutput<JunkApprovalViewDto>> GetJunkCompanies(GetJunkApprovalInput input);
        Task UpadteCompanyJunk(int Id);
        Task<FileDto> GetJunkApprovalToExcel();

    }
}
