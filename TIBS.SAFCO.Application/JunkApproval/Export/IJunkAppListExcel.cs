﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.JunkApproval.Dto;

namespace TIBS.SAFCO.JunkApproval.Export
{
    public interface IJunkAppListExcel
    {
        FileDto ExportToFile(List<JunkApprovalViewDto> junkAppListDto);
    }
}
