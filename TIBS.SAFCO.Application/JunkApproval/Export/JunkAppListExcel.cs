﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.DataExporting.Excel.EpPlus;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.JunkApproval.Dto;

namespace TIBS.SAFCO.JunkApproval.Export
{
    public class JunkAppListExcel : EpPlusExcelExporterBase, IJunkAppListExcel
    {
        public FileDto ExportToFile(List<JunkApprovalViewDto> junkAppListDto)
        {
            return CreateExcelPackage(
                "JunkApproval.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("JunkApproval"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("CompanyName"),
                        L("Status"),
                        L("ContributedBy"),
                        L("ManagedBy")
                    );

                    AddObjects(
                        sheet, 2, junkAppListDto,
                        _ => _.CompanyName,
                        _ => _.Status,
                        _ => _.ContributedBy,
                        _ => _.ManagedBy
 
                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(4);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 4; i++)
                    {

                        sheet.Column(i).AutoFit();
                    }
                });
        }

    }
}
