﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;

namespace TIBS.SAFCO.ProductClassifications.Dto
{
    [AutoMapTo(typeof(ProductClassification))]
    public class CreateProductClassifiactionInput : IInputDto
    {
        public virtual string ProductClassificationCode { get; set; }
        public virtual string ProductClassificationName { get; set; }
        public int Id { get; set; }
    }
}
