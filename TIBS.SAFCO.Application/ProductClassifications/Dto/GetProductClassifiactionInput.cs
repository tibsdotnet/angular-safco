﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.SAFCO.ProductClassifications.Dto
{
    public class GetProductClassifiactionInput:IInputDto
    {
        public string Filter { get; set; }
    }
}
