﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;

namespace TIBS.SAFCO.ProductClassifications.Dto
{
    [AutoMapFrom(typeof(ProductClassification))]
   public class ProductClassifiactionListDto:FullAuditedEntityDto
    {
        public  string ProductClassificationCode { get; set; }
        public  string ProductClassificationName { get; set; }
        public int Id { get; set; }
    }
}
