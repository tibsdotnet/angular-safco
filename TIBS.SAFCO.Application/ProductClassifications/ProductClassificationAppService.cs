﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Abp.Linq.Extensions;
using Abp.Extensions;
using Abp.Linq;
using Abp.AutoMapper;
using System.Data.Entity;
using System.Threading.Tasks;
using TIBS.SAFCO.ProductClassifications.Dto;
using Abp.UI;
using Abp.Authorization;
using TIBS.SAFCO.Authorization;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.ProductClassifications.Export;

namespace TIBS.SAFCO.ProductClassifications
{
   public class ProductClassificationAppService:SAFCOAppServiceBase,IProductClassificationAppService
    {
       private readonly IRepository<ProductClassification> _ProductClassificationRepository;
       private readonly IProductClassListExcel _ProductClassListExcel;
       public ProductClassificationAppService(IProductClassListExcel ProductClassListExcel, IRepository<ProductClassification> ProductClassificationRepository)
       {
           _ProductClassificationRepository = ProductClassificationRepository;
           _ProductClassListExcel = ProductClassListExcel;
       }
       public ListResultOutput<ProductClassifiactionListDto> GetProductClassification(GetProductClassifiactionInput input)
       {
           var ProductClassification = _ProductClassificationRepository
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Id.ToString().Contains(input.Filter) ||
                       p.ProductClassificationCode.Contains(input.Filter) ||
                        p.ProductClassificationName.Contains(input.Filter)
                       
               )
               .OrderBy(p => p.ProductClassificationName)
               .ThenBy(p => p.ProductClassificationCode)
               .ToList();

           return new ListResultOutput<ProductClassifiactionListDto>(ProductClassification.MapTo<List<ProductClassifiactionListDto>>());
       }

       public async Task<FileDto> GetProductClassificationToExcel()
       {

           var prodcl = _ProductClassificationRepository
               .GetAll().ToList();
           var productClassListDtos = prodcl.MapTo<List<ProductClassifiactionListDto>>();


           return _ProductClassListExcel.ExportToFile(productClassListDtos);
       }


       public async Task<GetProductClassification> GetProductClassificationForEdit(NullableIdInput<long> input)
       {
           var output = new GetProductClassification
           {
           };

           var ProductClassification =await _ProductClassificationRepository
               .GetAll().Where(p => p.Id == input.Id).FirstOrDefaultAsync();

           output.ProductClassification = ProductClassification.MapTo<CreateProductClassifiactionInput>();

           return output;

       }

       public async Task CreateOrUpdateUser(CreateProductClassifiactionInput input)
       {
           if (input.Id != 0)
           {
               await UpdateProductClassificationAsync(input);
           }
           else
           {
               await CreateProductClassificationAsync(input);
           }
       }
       [AbpAuthorize(AppPermissions.Pages_Tenant_Product_ProductClassification_CreateNewProductClassification)]

       public virtual async Task CreateProductClassificationAsync(CreateProductClassifiactionInput input)
       {
           var ProductClassification = input.MapTo<ProductClassification>();
           DateTime myUtcDateTime = DateTime.Now;

           ProductClassification.LastModificationTime = DateTime.Now.AddHours(-12);
           var val = _ProductClassificationRepository
             .GetAll().Where(p => p.ProductClassificationCode == input.ProductClassificationCode || p.ProductClassificationName == input.ProductClassificationName).FirstOrDefault();
           if (val == null)
           {
               await _ProductClassificationRepository.InsertAsync(ProductClassification);
           }
           else
           {
               throw new UserFriendlyException("Ooops!", "Duplicate Data Occured in ProductClassification Name '" + input.ProductClassificationName + "' or ProductClassification Code '" + input.ProductClassificationCode + "'...");
           }
       }
       [AbpAuthorize(AppPermissions.Pages_Tenant_Product_ProductClassification_EditProductClassification)]

       public virtual async Task UpdateProductClassificationAsync(CreateProductClassifiactionInput input)
       {
           var ProductClassification = input.MapTo<ProductClassification>();
           ProductClassification.ProductClassificationName = input.ProductClassificationName;
           ProductClassification.ProductClassificationCode = input.ProductClassificationCode;
           ;
           var val = _ProductClassificationRepository
             .GetAll().Where(p => (p.ProductClassificationCode == input.ProductClassificationCode || p.ProductClassificationName == input.ProductClassificationName) && p.Id != input.Id).FirstOrDefault();
           if (val == null)
           {
               await _ProductClassificationRepository.UpdateAsync(ProductClassification);
           }
           else
           {
               throw new UserFriendlyException("Ooops!", "Duplicate Data Occured in ProductClassification Name '" + input.ProductClassificationName + "' or ProductClassification Code '" + input.ProductClassificationCode + "'...");
           }
       }
      [AbpAuthorize(AppPermissions.Pages_Tenant_Product_ProductClassification_DeleteProductClassification)]

       public async Task DeleteProductClassification(IdInput input)
       {
           
                   await _ProductClassificationRepository.DeleteAsync(input.Id);
              
       }

    }
}
