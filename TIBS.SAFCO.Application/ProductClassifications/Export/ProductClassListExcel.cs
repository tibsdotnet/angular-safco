﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.DataExporting.Excel.EpPlus;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.ProductClassifications.Dto;

namespace TIBS.SAFCO.ProductClassifications.Export
{
    public class ProductClassListExcel : EpPlusExcelExporterBase, IProductClassListExcel
    {
        public FileDto ExportToFile(List<ProductClassifiactionListDto> productClassListDtos)
        {
            return CreateExcelPackage(
                "ProductClassification.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("ProductClassification"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("ProductClassificationCode"),
                        L("ProductClassificationName")
                    );

                    AddObjects(
                        sheet, 2, productClassListDtos,
                        _ => _.ProductClassificationCode,
                        _ => _.ProductClassificationName
                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(2);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 2; i++)
                    {

                        sheet.Column(i).AutoFit();
                    }
                });
        }

    }
}
