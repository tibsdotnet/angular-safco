﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.ProductClassifications.Dto;

namespace TIBS.SAFCO.ProductClassifications.Export
{
    public interface IProductClassListExcel
    {
        FileDto ExportToFile(List<ProductClassifiactionListDto> productClassListDtos);
    }
}
