﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.ProductClassifications.Dto;

namespace TIBS.SAFCO.ProductClassifications
{
   public interface IProductClassificationAppService:IApplicationService
    {
       ListResultOutput<ProductClassifiactionListDto> GetProductClassification(GetProductClassifiactionInput input);
       Task<GetProductClassification> GetProductClassificationForEdit(NullableIdInput<long> input);
       Task CreateOrUpdateUser(CreateProductClassifiactionInput input);
       Task DeleteProductClassification(IdInput input);
       Task<FileDto> GetProductClassificationToExcel();
    }
}
