﻿using System.Threading.Tasks;
using Abp.Application.Services;
using TIBS.SAFCO.Configuration.Tenants.Dto;

namespace TIBS.SAFCO.Configuration.Tenants
{
    public interface ITenantSettingsAppService : IApplicationService
    {
        Task<TenantSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(TenantSettingsEditDto input);
    }
}
