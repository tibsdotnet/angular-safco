﻿using System.Threading.Tasks;
using Abp.Application.Services;
using TIBS.SAFCO.Configuration.Host.Dto;

namespace TIBS.SAFCO.Configuration.Host
{
    public interface IHostSettingsAppService : IApplicationService
    {
        Task<HostSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(HostSettingsEditDto input);
    }
}
