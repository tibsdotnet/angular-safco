using System.Collections.Generic;
using Abp.Application.Services.Dto;
using TIBS.SAFCO.Editions.Dto;

namespace TIBS.SAFCO.MultiTenancy.Dto
{
    public class GetTenantFeaturesForEditOutput : IOutputDto
    {
        public List<NameValueDto> FeatureValues { get; set; }

        public List<FlatFeatureDto> Features { get; set; }
    }
}