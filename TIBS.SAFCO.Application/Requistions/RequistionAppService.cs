﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.Employees;
using TIBS.SAFCO.Compnaies;
using TIBS.SAFCO.LeadRequests;
using TIBS.SAFCO.Requistions.Dto;
using TIBS.SAFCO.Authorization.Roles;
using System.Data.Entity;
using TIBS.SAFCO.Companies.Dto;
using TIBS.SAFCO.Allott.Dto;
using TIBS.SAFCO.Requistions.Export;

namespace TIBS.SAFCO.Requistions
{
    public class RequistionAppService : SAFCOAppServiceBase, IRequistionAppService
    {
        private readonly IRepository<Employee> _EmployeeRepository;
        private readonly IRepository<Company> _CompanyRepository;
        private readonly IRepository<LeadRequest> _LeadRequestRepository;
        private readonly RoleManager _roleManager;
        private readonly IRequistionListExcel _RequistionListExcel;

        public RequistionAppService(IRequistionListExcel RequistionListExcel, IRepository<Employee> EmployeeRepository, IRepository<Company> CompanyRepository, IRepository<LeadRequest> LeadRequestRepository, RoleManager roleManager)
        {
            _EmployeeRepository = EmployeeRepository;
            _CompanyRepository = CompanyRepository;
            _LeadRequestRepository = LeadRequestRepository;
            _roleManager = roleManager;
            _RequistionListExcel = RequistionListExcel;
        }

        public async Task<PagedResultOutput<GetRequistionsList>> GetCompanyData(GetRequistionsInput input)
        {
            var companyquery = _CompanyRepository.GetAll().Where(p=>p.ManagedId==null)
                             .WhereIf(
                             !input.Filter.IsNullOrWhiteSpace(),
                             p =>
                                 p.Title.Contains(input.Filter) ||
                                 p.Description.Contains(input.Filter) ||
                                 p.Fax.Contains(input.Filter) ||
                                 p.AbpUser.UserName.Contains(input.Filter) ||
                                 p.LeadSources.Tittle.Contains(input.Filter) ||
                                 p.status.StatusName.Contains(input.Filter) ||
                                 p.Regions.Name.Contains(input.Filter) ||
                                 p.PhoneNo.Contains(input.Filter) ||
                                 p.POBox.Contains(input.Filter) ||
                                 p.PublishedTime.ToString().Contains(input.Filter) ||
                                 p.WebSite.Contains(input.Filter) ||
                                 p.ManagedById.ToString().Contains(input.Filter)
                             );
            var companydto = await companyquery
                                 .OrderBy(p => p.Title)
                                 .PageBy(input)
                                 .ToListAsync();

            List<GetRequistionsList> Gr = new List<GetRequistionsList>();

            foreach (var compa in companydto)
            {
                var LeadRequests = (from r in _LeadRequestRepository.GetAll() where r.Company_Id == compa.Id select r).ToList();
                if(LeadRequests.Count>0)
                {

                    foreach (var LeadR in LeadRequests)
                    {
                        try
                        {
                            Gr.Add(new GetRequistionsList
                            {
                                Company_Id = LeadR.Company_Id,
                                RequestedBy_Employees_ID = LeadR.RequestedBy_Employees_ID,
                                ApprovalBy_Employees_ID = LeadR.ApprovalBy_Employees_ID != null ? LeadR.ApprovalBy_Employees_ID : 0,
                                ApprovalStatus_IDD = LeadR.ApprovalStatus_IDD,
                                Lead = LeadR.Companies.Title,
                                RequestedBy = LeadR.Employees.FirstName,
                                ContributedBy = compa.AbpUser.UserName,
                                Region = compa.Regions.Name,
                                AllotedTo = LeadR.Companies.ManagedId != null ? LeadR.Employes.FirstName : "Not Yet Alloted",
                                Status = LeadR.ApprovalStatuss.ApprovalStatusName,
                                enabled = LeadR.ApprovalStatuss.ApprovalStatusName != "Allot"?true:false,
                                Id = LeadR.Id,
                                disable1 = LeadR.ApprovalStatuss.ApprovalStatusName == "Pending Approval"?true:false,
                                disable2 = LeadR.ApprovalStatuss.ApprovalStatusName == "Rejected" ? true : false
                            });
                        }
                        catch (Exception ex)
                        {

                        }
                    }                   
                }
                else
                {
                    Gr.Add(new GetRequistionsList
                    {
                        Company_Id = compa.Id,
                        RequestedBy_Employees_ID = 0,
                        ApprovalStatus_IDD = 0,
                        Lead = compa.Title,
                        RequestedBy = "No Sales Person Requested",
                        ContributedBy = compa.ContributedId!=null?compa.AbpUser.UserName:"",
                        Region=compa.Regions.Name,
                        AllotedTo = "Not Yet Alloted",
                        Status = "Allot",
                        enabled=false
                    });
                }
            }
            var companycount =  Gr.Count();


            return new PagedResultOutput<GetRequistionsList>(companycount, Gr.MapTo<List<GetRequistionsList>>());
        }

        public async Task<PagedResultOutput<GetRequistionsList>> GetLeadRequestData(GetRequistionsInput input)
        {
            long userid = (int)AbpSession.UserId;
            int empid = (from r in _EmployeeRepository.GetAll() where r.UserId == userid select r.Id).FirstOrDefault();
            var companyquery = _CompanyRepository.GetAll().Where(p => p.ManagedId == null)
                             .WhereIf(
                             !input.Filter.IsNullOrWhiteSpace(),
                             p =>
                                 p.Title.Contains(input.Filter) ||
                                 p.Description.Contains(input.Filter) ||
                                 p.Fax.Contains(input.Filter) ||
                                 p.AbpUser.UserName.Contains(input.Filter) ||
                                 p.LeadSources.Tittle.Contains(input.Filter) ||
                                 p.status.StatusName.Contains(input.Filter) ||
                                 p.Regions.Name.Contains(input.Filter) ||
                                 p.PhoneNo.Contains(input.Filter) ||
                                 p.POBox.Contains(input.Filter) ||
                                 p.PublishedTime.ToString().Contains(input.Filter) ||
                                 p.WebSite.Contains(input.Filter) ||
                                 p.ManagedById.ToString().Contains(input.Filter)
                             );
            var companydto = await companyquery
                                 .OrderBy(p => p.Title)
                                 .PageBy(input)
                                 .ToListAsync();

            List<GetRequistionsList> Gr = new List<GetRequistionsList>();

            foreach (var compa in companydto)
            {
                var LeadRequests = (from r in _LeadRequestRepository.GetAll() where r.Company_Id == compa.Id && r.RequestedBy_Employees_ID == empid select r).ToList();
                if (LeadRequests.Count > 0)
                {
                    foreach (var LeadR in LeadRequests)
                    {
                        Gr.Add(new GetRequistionsList
                        {
                            Company_Id = LeadR.Company_Id,
                            RequestedBy_Employees_ID = LeadR.RequestedBy_Employees_ID,
                            ApprovalBy_Employees_ID = LeadR.ApprovalBy_Employees_ID,
                            ApprovalStatus_IDD = LeadR.ApprovalStatus_IDD,
                            Lead = LeadR.Companies.Title,
                            RequestedBy = LeadR.Employees.FirstName,
                            ContributedBy = compa.AbpUser.UserName,
                            Region = compa.Regions.Name,
                            PhoneNo=compa.PhoneNo,
                            RequestStatus=LeadR.ApprovalStatuss.ApprovalStatusName,
                            Address=compa.Address,
                            enabled=false,
                            Id=LeadR.Id
                        });
                    }
                }
                else
                {
                    Gr.Add(new GetRequistionsList
                    {
                        Company_Id = compa.Id,
                        RequestedBy_Employees_ID = 0,
                        ApprovalStatus_IDD = 0,
                        Lead = compa.Title,
                        RequestedBy = "No Sales Person Requested",
                        ContributedBy = compa.ContributedId != null ? compa.AbpUser.UserName : "",
                        Region = compa.Regions.Name,
                        PhoneNo = compa.PhoneNo,
                        RequestStatus = "Request Lead",
                        Address = compa.Address,
                        enabled=true
                    });
                }
            }
            var companycount = Gr.Count();


            return new PagedResultOutput<GetRequistionsList>(companycount, Gr.MapTo<List<GetRequistionsList>>());
        }

        public async Task<string> Getrole()
        {
            long userid = (int)AbpSession.UserId;
            var user = await UserManager.GetUserByIdAsync(userid);
            var roleid = user.Roles.ToList();

            string[] userroles = new string[roleid.Count];
            int i = 0;
            foreach (var role in roleid)
            {
                int rol = role.RoleId;
                var userrolesquery = _roleManager.Roles.Where(p => p.Id == rol).OrderBy(p => p.DisplayName).FirstOrDefault();
                userroles[i] = userrolesquery.DisplayName;
                i++;
            }
            var uroles = userroles.ToList();

            return uroles[0].ToString();
        }

        public virtual async Task CreateLeadRequest(CreateCompanyyInput input)
        {
            var com = (from r in _CompanyRepository.GetAll() where r.Id == input.company_Id select r).FirstOrDefault();

            com.Title = input.Lead;
            com.Description = input.Description;
            com.Address = input.Address;
            com.StatusId = 2;
            var company = com.MapTo<Company>();
            await _CompanyRepository.UpdateAsync(company);

            long userid = (int)AbpSession.UserId;
            int request_Emp_Id = (from r in _EmployeeRepository.GetAll() where r.UserId == userid select r.Id).FirstOrDefault();
            CreateLeadRequestInput cr = new CreateLeadRequestInput();
            cr.Company_Id = input.company_Id;
            cr.RequestedBy_Employees_ID = request_Emp_Id;
            cr.ApprovalStatus_IDD = 1;
            cr.Archived = false;
            var LeadRequest = cr.MapTo<LeadRequest>();
            await _LeadRequestRepository.InsertAsync(LeadRequest);

        }

        public virtual async Task LeadRequestPending(int id)
        {
            var leadrequest = (from r in _LeadRequestRepository.GetAll() where r.Id == id select r).FirstOrDefault();
            leadrequest.ApprovalStatus_IDD = 1;
            var leadR = leadrequest.MapTo<LeadRequest>();
            await _LeadRequestRepository.UpdateAsync(leadR);
        }

        public virtual async Task LeadRequestReject(int id)
        {
            var leadrequest = (from r in _LeadRequestRepository.GetAll() where r.Id == id select r).FirstOrDefault();
            leadrequest.ApprovalStatus_IDD = 4;
            var leadR = leadrequest.MapTo<LeadRequest>();
            await _LeadRequestRepository.UpdateAsync(leadR);
        }

        public virtual async Task LeadRequestApproved(int id)
        {
            long userid = (int)AbpSession.UserId;
            int Approve_Emp_Id = (from r in _EmployeeRepository.GetAll() where r.UserId == userid select r.Id).FirstOrDefault();
            var leadrequest = (from r in _LeadRequestRepository.GetAll() where r.Id == id select r).FirstOrDefault();
            leadrequest.ApprovalStatus_IDD = 3;
            leadrequest.ApprovalBy_Employees_ID = Approve_Emp_Id;
            var leadR = leadrequest.MapTo<LeadRequest>();
            await _LeadRequestRepository.UpdateAsync(leadR);

            var com = (from r in _CompanyRepository.GetAll() where r.Id == leadrequest.Company_Id select r).FirstOrDefault();
            com.ManagedId = leadrequest.RequestedBy_Employees_ID;
            com.StatusId = 1;
            var company = com.MapTo<Company>();
            await _CompanyRepository.UpdateAsync(company);
        }

        public async Task<FileDto> GetRequistionsToExcel()
        {
            var roles = Getrole().ToString();
            var query = new List<GetRequistionsList>();
            if (roles.Contains("Admin") || roles.Contains("Management"))
            {
                GetRequistionsInput input = new GetRequistionsInput();
                await GetLeadRequestData(input);
            }
            else if (roles.Contains("SalesSupport ") || roles.Contains("SalesTeam") || roles.Contains("Audit/Reporting"))
            {
                GetRequistionsInput input = new GetRequistionsInput();
                await GetCompanyData(input);
            }

            var requistionListDtos = query.MapTo<List<GetRequistionsList>>();

            return _RequistionListExcel.ExportToFile(requistionListDtos);
        }
    }
    public class CreateCompanyyInput : IInputDto
    {
        public int Id { get; set; }
        public string Lead { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }

        public int RegionId { get; set; }

        public int? LeadSourceId { get; set; }

        public int? StatusId { get; set; }
        public string POBox { get; set; }
        public string PhoneNo { get; set; }
        public string WebSite { get; set; }
        public string Fax { get; set; }

        public long? ContributedId { get; set; }
        public DateTime PublishedTime { get; set; }
        public int? Managedid { get; set; }

        public int company_Id { get; set; }
    }
}
