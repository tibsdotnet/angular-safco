﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using TIBS.SAFCO.LeadRequests;

namespace TIBS.SAFCO.Requistions.Dto
{
    [AutoMapFrom(typeof(LeadRequest))]
    public class GetRequistionsList:FullAuditedEntityDto
    {
        public  int? Company_Id { get; set; }

        public  int? RequestedBy_Employees_ID { get; set; }

        public  int? ApprovalStatus_IDD { get; set; }

        public  int? ApprovalBy_Employees_ID { get; set; }

        public  bool? Archived { get; set; }

        public string Lead { get; set; }

        public string RequestedBy { get; set; }

        public string ContributedBy { get; set; }

        public string AllotedTo { get; set; }

        public string PhoneNo { get; set; }

        public string Address { get; set; }

        public string Region { get; set; }

        public string Country { get; set; }

        public string RequestStatus { get; set; }

        public string Status { get; set; }

        public bool enabled { get; set; }

        public int? Id { get; set; }

        public bool? disable1 { get; set; }

        public bool? disable2 { get; set; }
    }
}
