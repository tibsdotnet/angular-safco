﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Companies.Dto;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.Requistions.Dto;

namespace TIBS.SAFCO.Requistions
{
    public interface IRequistionAppService : IApplicationService
    {
        Task<string> Getrole();

        Task<PagedResultOutput<GetRequistionsList>> GetCompanyData(GetRequistionsInput input);

        Task<PagedResultOutput<GetRequistionsList>> GetLeadRequestData(GetRequistionsInput input);

        Task CreateLeadRequest(CreateCompanyyInput input);

        Task LeadRequestPending(int id);

        Task LeadRequestReject(int id);

        Task LeadRequestApproved(int id);
        Task<FileDto> GetRequistionsToExcel();
    }
}
