﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.Requistions.Dto;

namespace TIBS.SAFCO.Requistions.Export
{
    public interface IRequistionListExcel
    {
        FileDto ExportToFile(List<GetRequistionsList> requistionListDtos);
    }
}
