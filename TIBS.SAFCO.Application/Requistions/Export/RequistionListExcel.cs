﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.DataExporting.Excel.EpPlus;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.Requistions.Dto;

namespace TIBS.SAFCO.Requistions.Export
{
    public class RequistionListExcel : EpPlusExcelExporterBase, IRequistionListExcel
    {
        public FileDto ExportToFile(List<GetRequistionsList> requistionListDtos)
        {
            return CreateExcelPackage(
                "Requistion.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Requistion"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Lead"),
                        L("RequestedBy"),
                        L("ContributedBy"),
                        L("Region"),
                        L("AllotedTo"),
                        L("RequestStatus")
                    );

                    AddObjects(
                        sheet, 2, requistionListDtos,
                        _ => _.Lead,
                        _ => _.RequestedBy,
                        _ => _.ContributedBy,
                        _ => _.Region,
                        _ => _.AllotedTo,
                        _ => _.RequestStatus

                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(6);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 6; i++)
                    {

                        sheet.Column(i).AutoFit();
                    }
                });
        }

    }
}
