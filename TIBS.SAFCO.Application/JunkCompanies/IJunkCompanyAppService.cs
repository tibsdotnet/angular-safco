﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.JunkCompanies.Dto;

namespace TIBS.SAFCO.JunkCompanies
{
    public interface IJunkCompanyAppService:IApplicationService
    {
        Task<PagedResultOutput<JunkCompanyViewDto>> GetJunkCompany(GetCompanyInput input);
        Task<FileDto> GetJunkCompanyToExcel();
    }
}
