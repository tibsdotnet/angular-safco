﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.JunkCompanies.Dto;

namespace TIBS.SAFCO.JunkCompanies.Export
{
    public interface IJunkComListExcel
    {
        FileDto ExportToFile(List<JunkCompanyViewDto> junkComListDto);
    }
}
