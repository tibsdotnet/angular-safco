﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.DataExporting.Excel.EpPlus;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.JunkCompanies.Dto;

namespace TIBS.SAFCO.JunkCompanies.Export
{
    public class JunkComListExcel : EpPlusExcelExporterBase, IJunkComListExcel
    {
        public FileDto ExportToFile(List<JunkCompanyViewDto> junkComListDto)
        {
            return CreateExcelPackage(
                "JunkCompany.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("JunkCompany"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("CompanyName"),
                        L("Region"),
                        L("Status"),
                        L("PhoneNo"),
                        L("Contributedby"),
                        L("ManagedBy")
                    );

                    AddObjects(
                        sheet, 2, junkComListDto,
                        _ => _.CompanyName,
                        _ => _.Region,
                        _ => _.Status,
                        _ => _.PhoneNumber,
                        _ => _.ContributedBy,
                        _ => _.ManagedBy

                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(6);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 6; i++)
                    {

                        sheet.Column(i).AutoFit();
                    }
                });
        }

    }
}

