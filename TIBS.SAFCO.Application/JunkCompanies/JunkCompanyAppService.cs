﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Compnaies;
using TIBS.SAFCO.JunkCompanies.Dto;
using Abp.Linq.Extensions;
using Abp.Extensions;
using System.Data.Entity;
using Abp.AutoMapper;
using TIBS.SAFCO.JunkCompanies.Export;
using TIBS.SAFCO.Dto;
using System.Linq.Dynamic;

namespace TIBS.SAFCO.JunkCompanies
{
    public class JunkCompanyAppService:SAFCOAppServiceBase,IJunkCompanyAppService
    {
        private readonly IRepository<Company> _CompanyReposoitory;
        private readonly IJunkComListExcel _JunkComListExcel;
        public JunkCompanyAppService(IJunkComListExcel JunkComListExcel, IRepository<Company> CompanyRepository)
        {
            _CompanyReposoitory = CompanyRepository;
            _JunkComListExcel = JunkComListExcel;
        }
        public async Task<PagedResultOutput<JunkCompanyViewDto>> GetJunkCompany(GetCompanyInput input)
        {
            var companyquery = _CompanyReposoitory.GetAll().Where(p=>p.StatusId>4)
                             .WhereIf(
                             !input.Filter.IsNullOrWhiteSpace(),
                             p =>
                                 p.Title.Contains(input.Filter) ||
                               
                                 p.Fax.Contains(input.Filter) ||
                                 p.AbpUser.UserName.Contains(input.Filter) ||
                                
                                 p.status.StatusName.Contains(input.Filter) ||
                                 p.Regions.Name.Contains(input.Filter) ||
                                 p.PhoneNo.Contains(input.Filter) 
                                 

                             );
            var companydtos = (from c in companyquery select new JunkCompanyViewDto { Id = c.Id, CompanyName = c.Title, Region = c.Regions.Name, Status = c.StatusId != null ? c.status.StatusName : "", PhoneNumber = c.PhoneNo, ContributedBy = c.ContributedId != null ? c.AbpUser.UserName : "", ManagedBy = c.ManagedId != null ? c.ManagedUser.FirstName : "" });
            var companycount = await companydtos.CountAsync();
            var companydto = await companydtos
                                 .OrderBy(input.Sorting)
                                 .PageBy(input)
                                 .ToListAsync();


            var junkcompanydto = companydto.MapTo<List<JunkCompanyViewDto>>();

            return new PagedResultOutput<JunkCompanyViewDto>(companycount, junkcompanydto);
        }

        public async Task<FileDto> GetJunkCompanyToExcel()
        {

            var junk = _CompanyReposoitory.GetAll().Where(p => p.StatusId > 4);
            var companydtos = (from c in junk select new JunkCompanyViewDto { Id = c.Id, CompanyName = c.Title, Region = c.Regions.Name, Status = c.StatusId != null ? c.status.StatusName : "", PhoneNumber = c.PhoneNo, ContributedBy = c.ContributedId != null ? c.AbpUser.UserName : "", ManagedBy = c.ManagedId != null ? c.ManagedUser.FirstName : "" });
            var junkComListDto = companydtos.MapTo<List<JunkCompanyViewDto>>();
            return _JunkComListExcel.ExportToFile(junkComListDto);
        }
    }
}
