﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using TIBS.SAFCO.Compnaies;

namespace TIBS.SAFCO.JunkCompanies.Dto
{
    [AutoMapFrom(typeof(Company))]
    public class JunkCompanyViewDto:FullAuditedEntityDto
    {
        public string CompanyName { get; set; }
        public string PhoneNumber { get; set; }
        public string Status { get; set; }
        public string ContributedBy { get; set; }
        public string ManagedBy { get; set; }
        public string Region { get; set; }
    }
}
