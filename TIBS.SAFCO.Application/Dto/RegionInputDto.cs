﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.SAFCO.Dto
{
   public class RegionInputDto
    {
       public int RegionId { get; set; }
       public string RegionName { get; set; }
    }
}
