﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.SAFCO.Dto
{
   public class LeadSourceInputDto
    {
       public int LeadSourceId { get; set; }
       public string LeadSourceName { get; set; }
    }
}
