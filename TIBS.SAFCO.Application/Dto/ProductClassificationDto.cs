﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.SAFCO.Dto
{
   public class ProductClassificationDto:IDto
    {
       public int ProductClassificationId { get; set; }
       public string ProductClassificationName { get; set; }
    }
}
