﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.SAFCO.Dto
{
    public class UserListInputDtos
    {
        public long UserId { get; set; }
        public string UserName { get; set; }
    }
}
