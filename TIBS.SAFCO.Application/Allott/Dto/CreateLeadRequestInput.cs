﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using System.ComponentModel.DataAnnotations;
using TIBS.SAFCO.LeadRequests;

namespace TIBS.SAFCO.Allott.Dto
{
    [AutoMapTo(typeof(LeadRequest))]
    public class CreateLeadRequestInput : IInputDto
    {
        public int Id { get; set; }

        public  int Company_Id { get; set; }

        public  int RequestedBy_Employees_ID { get; set; }

        public  int ApprovalStatus_IDD { get; set; }

        public  int? ApprovalBy_Employees_ID { get; set; }

        public  bool? Archived { get; set; }
    }
}
