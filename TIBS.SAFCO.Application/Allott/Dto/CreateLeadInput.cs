﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using System.ComponentModel.DataAnnotations;
using TIBS.SAFCO.Leadss;

namespace TIBS.SAFCO.Allott.Dto
{
    [AutoMapTo(typeof(Lead))]
    public class CreateLeadInput:IInputDto
    {
        public int Id { get; set; }

        public  string Title { get; set; }

        public  string Description { get; set; }

        [Required]
        public  int CompanyId { get; set; }

        public  int? LeadSourceId { get; set; }

        [Required]
        public  int MileStoneId { get; set; }

        public  int Rating { get; set; }

        public  float ExpectedAmount { get; set; }

        public  Nullable<System.DateTime> ExpectedClosure { get; set; }

        public  float ActualAmount { get; set; }

        public  Nullable<System.DateTime> ActualClosure { get; set; }

        [MaxLength(GetLength.MaxOrderLength)]
        public  string OrderNo { get; set; }

        public  bool Archived { get; set; }

        public Nullable<System.DateTime> Published { get; set; }

        [Required]
        public virtual int EmployeeId { get; set; }

        public virtual int? ChefId { get; set; }
    }
}
