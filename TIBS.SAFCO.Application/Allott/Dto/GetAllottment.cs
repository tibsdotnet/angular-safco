﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Compnaies;

namespace TIBS.SAFCO.Allott.Dto
{
    public class GetAllottment : IOutputDto
    {
        public companyy companydata { get; set; }

        public AllotPerson[] AllotTo { get; set; }
    }

    [AutoMapTo(typeof(Company))]
    public class companyy
    {
        public int Id { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }

        public int RegionId { get; set; }

        public long? Managedbyid { get; set; }

        public string PresentlyAllotedTo { get; set; }

        public int? Managedid { get; set; }
    }

    public class AllotPerson
    {
        public int EmpId { get; set; }

        public string EmpName { get; set; }
    }
}
