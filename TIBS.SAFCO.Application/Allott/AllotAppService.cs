﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using TIBS.SAFCO.Dto;
using System.Data.Entity;
using TIBS.SAFCO.Employees;
using TIBS.SAFCO.Compnaies;
using TIBS.SAFCO.LeadRequests;
using TIBS.SAFCO.Allott.Dto;
using TIBS.SAFCO.Companies.Dto;
using TIBS.SAFCO.Leadss;
using System.Data.SqlClient;

namespace TIBS.SAFCO.Allott
{
    public class AllotAppService : SAFCOAppServiceBase, IAllotAppService
    {
        private readonly IRepository<Employee> _EmployeeRepository;
        private readonly IRepository<Company> _CompanyRepository;
        private readonly IRepository<LeadRequest> _LeadRequestRepository;
        private readonly IRepository<Lead> _LeadRepository;
        public AllotAppService(IRepository<Employee> EmployeeRepository, IRepository<Company> CompanyRepository, IRepository<LeadRequest> LeadRequestRepository, IRepository<Lead> LeadRepository)
        {
            _EmployeeRepository = EmployeeRepository;
            _CompanyRepository = CompanyRepository;
            _LeadRequestRepository = LeadRequestRepository;
            _LeadRepository = LeadRepository;
        }

        public async Task<GetAllottment> GetAllotForEdit(NullableIdInput<long> input)
        {
            var company = (from r in _CompanyRepository.GetAll() where r.Id == input.Id select r).FirstOrDefault();
            var companyy = new companyy();
            companyy.Title = company.Title;
            companyy.Id=company.Id;
            companyy.RegionId=company.RegionId;
            companyy.Address=company.Address;
            companyy.Description=company.Description;
            companyy.Managedbyid=company.ManagedById;
            companyy.Managedid = company.ManagedId;
            companyy.PresentlyAllotedTo = company.ManagedId != null ? company.ManagedUser.FirstName : "Not Alloted";

            var output = new GetAllottment
            {
                companydata = companyy
                
            };

            var AllotPersonsDtos = (await _EmployeeRepository.GetAll().Where(p => p.Roles.DisplayName.Contains("Sale") || p.Roles.DisplayName.Contains("Support"))
                  .OrderBy(r => r.FirstName)
                  .Select(r => new AllotPerson
                  {
                      EmpId = r.Id,
                      EmpName = r.FirstName

                  })
                  .ToArrayAsync());

            output.AllotTo = AllotPersonsDtos;
            

            return output;

        }

        public virtual async Task CreateAllotmentAsync(CreateCompanyInput input)
        {
            var com = (from r in _CompanyRepository.GetAll() where r.Id == input.Id select r).FirstOrDefault();
            
            com.ManagedId = input.Managedid;
            com.Title = input.Title;
            com.Description = input.Description;
            com.Address = input.Address;
            com.StatusId = 1;
            var company = com.MapTo<Company>();
            await _CompanyRepository.UpdateAsync(company);

            long userid = (int)AbpSession.UserId;
            int Approval_Emp_Id = (from r in _EmployeeRepository.GetAll() where r.UserId == userid select r.Id).FirstOrDefault();
            CreateLeadRequestInput cr = new CreateLeadRequestInput();
            cr.Company_Id = input.Id;
            cr.RequestedBy_Employees_ID = (int)input.Managedid;
            cr.ApprovalStatus_IDD = 3;
            cr.ApprovalBy_Employees_ID = Approval_Emp_Id;
            cr.Archived = false;
            var LeadRequest = cr.MapTo<LeadRequest>();
            await _LeadRequestRepository.InsertAsync(LeadRequest);

            CreateLeadInput Cl = new CreateLeadInput();
            Cl.Title = input.Title;
            Cl.CompanyId = input.Id;
            Cl.LeadSourceId = company.LeadSourceId;
            Cl.MileStoneId = 2;
            Cl.Archived = false;
            Cl.EmployeeId = (int)input.Managedid;
            Cl.Rating = 0;
            Cl.ExpectedAmount = 0;
            Cl.ActualAmount = 0;
            var Lead = Cl.MapTo<Lead>();
            await _LeadRepository.InsertAsync(Lead);

        }
    }
    public class returnn
    {
        public string data { get; set; }
    }
}
