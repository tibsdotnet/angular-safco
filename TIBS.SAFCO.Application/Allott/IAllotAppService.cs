﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Allott.Dto;
using TIBS.SAFCO.Companies.Dto;
using TIBS.SAFCO.Dto;

namespace TIBS.SAFCO.Allott
{
    public interface IAllotAppService : IApplicationService
    {
        Task<GetAllottment> GetAllotForEdit(NullableIdInput<long> input);

        Task CreateAllotmentAsync(CreateCompanyInput input);
    }
}
