﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Leadss;

namespace TIBS.SAFCO.SaleApprovals.Dto
{
    [AutoMapFrom(typeof(Lead))]
    public class SaleApproval : FullAuditedEntityDto
    {
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string OrderNo { get; set; }
        public string MileStone{ get; set; }
        public float ExpectedAmount { get; set; }
        public DateTime ExpectedClosure { get; set; }
        public float ActualAmount { get; set; }
        public DateTime ActualClosure { get; set; }
        public string Employee { get; set; }

    }
}
