﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.SaleApprovals.Dto;

namespace TIBS.SAFCO.SaleApprovals.Export
{
    public interface ISaleAppListExport
    {
        FileDto ExportToFile(List<SaleApprovalList> salAppListDto);
    }
}
