﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.DataExporting.Excel.EpPlus;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.SaleApprovals.Dto;

namespace TIBS.SAFCO.SaleApprovals.Export
{
    class SaleAppListExport : EpPlusExcelExporterBase, ISaleAppListExport
    {
        public FileDto ExportToFile(List<SaleApprovalList> salAppListDto)
        {
            return CreateExcelPackage(
                "SaleApproval.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("SaleApproval"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("CompanyName"),
                        L("Employee")
                    );

                    AddObjects(
                        sheet, 2, salAppListDto,
                        _ => _.Company,
                        _ => _.Employee

                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(2);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 2; i++)
                    {

                        sheet.Column(i).AutoFit();
                    }
                });
        }

    }
}


