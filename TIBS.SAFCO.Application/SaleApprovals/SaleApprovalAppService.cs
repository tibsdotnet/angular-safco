﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Session;
using System.Globalization;
using Abp.UI;
using TIBS.SAFCO.Leadss;
using TIBS.SAFCO.SaleApprovals.Dto;
using TIBS.SAFCO.Compnaies;
using TIBS.SAFCO.SaleApprovals.Export;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.Leads.Dto;
using TIBS.SAFCO.Employees;
using TIBS.SAFCO.Authorization.Roles;
using TIBS.SAFCO.Authorization.Users.Dto;
using System.Linq.Dynamic;
using TIBS.SAFCO.Tenants.Dashboard;

namespace TIBS.SAFCO.SaleApprovals
{
    public class SaleApprovalAppService : SAFCOAppServiceBase, ISaleApprovalAppService
    {
        private readonly IRepository<Lead> _LeadRespository;
        private readonly IRepository<Company> _CompanyRepositry;
        private readonly ISaleAppListExport _SaleAppListExport;
        private readonly IRepository<Employee> _EmployeeRepository;
       private readonly RoleManager _roleManager;
        
        public SaleApprovalAppService(ISaleAppListExport SaleAppListExport, IRepository<Lead> LeadRespository, IRepository<Company> CompanyRepositry, IRepository<Employee> EmployeeRepository, RoleManager roleManager)
        {
            _LeadRespository = LeadRespository;
            _SaleAppListExport = SaleAppListExport;
            _CompanyRepositry = CompanyRepositry;
            _EmployeeRepository = EmployeeRepository;
           _roleManager=roleManager;
        }
        public async Task<PagedResultOutput<SaleApproval>> GetSaleApprovalReport(GetSaleApprovalInput input)
        {
            var query = (from r in _LeadRespository.GetAll()
                         where r.MileStoneId == 5 && r.Archived == false
                         select r);

            var sale = (from r in query join c in _CompanyRepositry.GetAll() on r.CompanyId equals c.Id select new { r, c });

            var salecount = await sale.CountAsync();


            var saledto = from c in sale select new SaleApproval { Id = c.r.Id, Title = c.c.Title, FirstName = c.c.ManagedUser.FirstName, LastName = c.c.ManagedUser.LastName };

            var salereport = (await saledto
                                    .OrderBy(input.Sorting)
                                    .PageBy(input)
                                    .ToListAsync());

            var salelist = salereport.MapTo<List<SaleApproval>>();
            return new PagedResultOutput<SaleApproval>(salecount, salelist);
        }

       public async Task<FileDto> GetSaleApprovalToExcel()
       {

           var query = (from r in _LeadRespository.GetAll()
                        where r.MileStoneId == 5
                        select r);

           var sale = (from r in query join c in _CompanyRepositry.GetAll() on r.CompanyId equals c.Id select new { r, c });
           var saledto = from c in sale select new SaleApprovalList { Company = c.c.Title, Employee = c.c.ManagedUser.FirstName };

           var salAppListDto = saledto.MapTo<List<SaleApprovalList>>();
           return _SaleAppListExport.ExportToFile(salAppListDto);
       }
        public async Task<GetLead> GetLeadForEdit(NullableIdInput<long> input)
       {
           var lead =await _LeadRespository.GetAll().Where(p => p.Id == input.Id).FirstOrDefaultAsync();
           var salesemployee = new List<EmployeeListDto>();
           var usersuppoort = new List<EmployeeListDto>();
           using (var context = new SpDbContext())
           {
               try
               {
                   var list = context.Database.SqlQuery<User_Result>("exec Sp_EmployeewithRole").ToList();
                   salesemployee = (from r in list where r.DisplayName == "Sales Team" select r).Select(d => new EmployeeListDto { EmployeeId = d.Id, EmployeeName = d.FirstName }).ToList();
                   usersuppoort = (from r in list where r.DisplayName == "Sales Support" select r).Select(d => new EmployeeListDto { EmployeeId = d.Id, EmployeeName = d.FirstName }).ToList();
               }
               catch(Exception ex)
               {
               }
           }
           var company = (from l in _LeadRespository.GetAll() join c in _CompanyRepositry.GetAll() on l.CompanyId equals c.Id where l.Id==input.Id select new { c, l }).FirstOrDefault();

           var output = new GetLead { Employee = salesemployee.ToArray(), UserList = usersuppoort.ToArray() };
           output.Lead = lead.MapTo<CreateLeadInput>();

           return output;
       }
        public async Task UpdateSalesApproval(CreateLeadInput input)
        {
            
            input.Archived = true;
            var leadinput = input.MapTo<Lead>();
            await _LeadRespository.UpdateAsync(leadinput);
            var company = _CompanyRepositry.GetAll().Where(p => p.Id == input.CompanyId).FirstOrDefault();
            company.StatusId = 4;
            company.LastModificationTime = DateTime.Now;
            var companyinput = company.MapTo<Company>();
            await _CompanyRepositry.UpdateAsync(companyinput);

        }
    }

    public class User_Result
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string DisplayName { get; set; }
    }
}

