﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.Leads.Dto;
using TIBS.SAFCO.SaleApprovals.Dto;

namespace TIBS.SAFCO.SaleApprovals
{
    public interface ISaleApprovalAppService : IApplicationService
    {
        Task<PagedResultOutput<SaleApproval>> GetSaleApprovalReport(GetSaleApprovalInput input);
        Task<FileDto> GetSaleApprovalToExcel();
        Task<GetLead> GetLeadForEdit(NullableIdInput<long> input);
        Task UpdateSalesApproval(CreateLeadInput input);
    }
}
