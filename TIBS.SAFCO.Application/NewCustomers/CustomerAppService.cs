﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Compnaies;
using TIBS.SAFCO.Leadss;
using TIBS.SAFCO.NewCustomers.Dto;
using Abp.AutoMapper;
using Abp.Linq.Extensions;
using Abp.Linq;
using Abp.Extensions;
using System.Data.Entity;
using TIBS.SAFCO.NewCustomers.Export;
using TIBS.SAFCO.Dto;

namespace TIBS.SAFCO.NewCustomers
{
    public class CustomerAppService:SAFCOAppServiceBase,ICustomerAppService
    {
        private readonly IRepository<Lead> _LeadRepository;
        private readonly IRepository<Company> _CompanyRepository;
        private readonly INewCusListExcel _NewCusListExcel;
        public CustomerAppService(INewCusListExcel NewCusListExcel, IRepository<Lead> LeadRepository, IRepository<Company> CompanyRepository)
        {
            _LeadRepository = LeadRepository;
            _CompanyRepository = CompanyRepository;
            _NewCusListExcel = NewCusListExcel;

        }
        public async Task<PagedResultOutput<NewCustomerDto>> GetCustomers(GetCustomerInput input)
        {
            var customerquery = _CompanyRepository.GetAll().WhereIf(!input.Filter.IsNullOrWhiteSpace(), p => p.Title.Contains(input.Filter) || p.ManagedUser.FirstName.Contains(input.Filter) || p.AbpUser.UserName.Contains(input.Filter));
            var joindtos = from c in customerquery join l in _LeadRepository.GetAll() on c.Id equals l.CompanyId where c.StatusId==4
                           && (l.ActualClosure >= input.StartDate && l.ActualClosure<input.EndDate)
                           select new { c, l };
            var customercompanydto = await joindtos
                                 .OrderBy(p => p.c.Title)
                                 .PageBy(input)
                                 .ToListAsync();
            var companycount =await joindtos.CountAsync();
            var customerdto=(from p in customercompanydto select new NewCustomerDto {Id=p.l.Id,CompanyName=p.c.Title,ContributedBy=p.c.ContributedId!=null?p.c.AbpUser.Name+" "+p.c.AbpUser.Surname:"",ManagedBy=p.c.ManagedId!=null?p.c.ManagedUser.FirstName+" "+p.c.ManagedUser.LastName:"",OrderNumber=p.l.OrderNo,Amount=p.l.ActualAmount.ToString(),ClosedDate=p.l.ActualClosure.ToString()});

            return new PagedResultOutput<NewCustomerDto>(companycount,customerdto.MapTo<List<NewCustomerDto>>());
        }

        public async Task<FileDto> GetNewCustomerToExcel()
        {

            var lead = _CompanyRepository
                .GetAll();
            var joindto = from c in lead join l in _LeadRepository.GetAll() on c.Id equals l.CompanyId where c.StatusId == 4 select new { c, l };
            var leaddtos = (from p in joindto select new NewCustomerDto { Id = p.l.Id, CompanyName = p.c.Title, ContributedBy = p.c.ContributedId != null ? p.c.AbpUser.Name + " " + p.c.AbpUser.Surname : "", ManagedBy = p.c.ManagedId != null ? p.c.ManagedUser.FirstName + " " + p.c.ManagedUser.LastName : "", OrderNumber = p.l.OrderNo, Amount = p.l.ActualAmount.ToString(), ClosedDate = p.l.ActualClosure.ToString() });
            var newCusListDto = leaddtos.MapTo<List<NewCustomerDto>>();
            return _NewCusListExcel.ExportToFile(newCusListDto);
        }
    }
}
