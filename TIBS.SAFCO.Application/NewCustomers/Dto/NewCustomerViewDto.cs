﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using TIBS.SAFCO.Leadss;
using TIBS.SAFCO.Compnaies;

namespace TIBS.SAFCO.NewCustomers.Dto
{
    [AutoMapFrom(typeof(Company))]
    public class NewCustomerDto : FullAuditedEntityDto
    {
        public int Id { get; set; }
        public string CompanyName { get; set; }
        public string ContributedBy { get; set; }
        public string ManagedBy { get; set; }
        public string OrderNumber { get; set; }
        public string Amount { get; set; }
        public string ClosedDate { get; set; }
    }
}
