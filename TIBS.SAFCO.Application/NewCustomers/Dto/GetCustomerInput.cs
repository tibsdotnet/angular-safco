﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.Timing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Dto;

namespace TIBS.SAFCO.NewCustomers.Dto
{
   public class GetCustomerInput: PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Title";
            }
            if (StartDate == DateTime.MinValue)
            {
                StartDate = Clock.Now;
            }

            StartDate = StartDate.Date;

            if (EndDate == DateTime.MinValue)
            {
                EndDate = Clock.Now;
            }
        }
    }
}
