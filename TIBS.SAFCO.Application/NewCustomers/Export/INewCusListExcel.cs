﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.NewCustomers.Dto;

namespace TIBS.SAFCO.NewCustomers.Export
{
    public interface INewCusListExcel
    {
        FileDto ExportToFile(List<NewCustomerDto> newCusListDto);
    }
}
