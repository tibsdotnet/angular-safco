﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.DataExporting.Excel.EpPlus;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.NewCustomers.Dto;

namespace TIBS.SAFCO.NewCustomers.Export
{
    public class NewCusListExcel : EpPlusExcelExporterBase, INewCusListExcel
    {
        public FileDto ExportToFile(List<NewCustomerDto> newCusListDto)
        {
            return CreateExcelPackage(
                "NewCustomer.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("NewCustomer"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("CompanyName"),
                        L("ContributedBy"),
                        L("ManagedBy"),
                        L("OrderNumber"),
                        L("Amount"),
                        L("ClosedDate")
                    );

                    AddObjects(
                        sheet, 2, newCusListDto,
                        _ => _.CompanyName,
                        _ => _.ContributedBy,
                        _ => _.ManagedBy,
                        _ => _.OrderNumber,
                        _ => _.Amount,
                        _ => _.ClosedDate

                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(6);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 6; i++)
                    {

                        sheet.Column(i).AutoFit();
                    }
                });
        }

    }
}

