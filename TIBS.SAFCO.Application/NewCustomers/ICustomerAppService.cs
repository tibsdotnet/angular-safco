﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.NewCustomers.Dto;

namespace TIBS.SAFCO.NewCustomers
{
    public interface ICustomerAppService:IApplicationService
    {
        Task<PagedResultOutput<NewCustomerDto>> GetCustomers(GetCustomerInput input);
        Task<FileDto> GetNewCustomerToExcel();
    }
}
