﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.CompetitorCompanies.Dto;
using TIBS.SAFCO.Dto;

namespace TIBS.SAFCO.CompetitorCompanies
{
    public interface ICompetitorCompanyAppService : IApplicationService
    {
        Task<PagedResultOutput<CCListDto>> GetCompetitorCompany(GetCompetitorCompanyInput input);
        Task CreateOrUpdateCompetitorCompany(CreateCCompanyInput input);
        Task DeleteCompetitorCompany(IdInput input);
        Task<GetCompany> GetCompanyForEdit(NullableIdInput<long> input);

        Task<FileDto> GetCompetitorCompanyToExcel();
    }
}
