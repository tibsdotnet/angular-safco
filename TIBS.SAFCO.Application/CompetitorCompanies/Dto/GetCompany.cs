﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.SAFCO.CompetitorCompanies.Dto
{
    public class GetCompany : IOutputDto
    {
        public CreateCCompanyInput company { get; set; }
    }
}