﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.CompetitorCompaniesss;

namespace TIBS.SAFCO.CompetitorCompanies.Dto
{
    [AutoMapFrom(typeof(CompetitorCompany))]
    public class CCListDto : EntityDto<long>
    {
        public string CompetitorCompanyName { get; set; }
        public virtual string CompetitorCompanyCode { get; set; }
    }
}
