﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.CompetitorCompaniesss;

namespace TIBS.SAFCO.CompetitorCompanies.Dto
{
    [AutoMapTo(typeof(CompetitorCompany))]

    public class CreateCCompanyInput : IInputDto
    {
        [Required]
        [MaxLength(GetLength.MaxNamegLength)]
        public string CompetitorCompanyName { get; set; }
        [Required]
        [MaxLength(GetLength.MaxCodeLength)]
        public string CompetitorCompanyCode { get; set; }
        public long Id { get; set; }
    }
}
