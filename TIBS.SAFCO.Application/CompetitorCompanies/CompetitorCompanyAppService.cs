﻿using Abp.Domain.Repositories;
using System;
using Abp.Extensions;
using System.Data.Entity;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Linq.Extensions;
using System.Linq.Dynamic;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Abp.UI;
using System.Threading.Tasks;
using TIBS.SAFCO.CompetitorCompaniesss;
using TIBS.SAFCO.CompetitorCompanies.Export;
using Abp.Application.Services.Dto;
using TIBS.SAFCO.CompetitorCompanies.Dto;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.Authorization;

namespace TIBS.SAFCO.CompetitorCompanies
{
    public class CompetitorCompanyAppService: SAFCOAppServiceBase,ICompetitorCompanyAppService
    {
        private readonly IRepository<CompetitorCompany> _ComRespository;
        private readonly ICompetitorCompanyListExcelExporter _competitorListExcelExporter;

        public CompetitorCompanyAppService(ICompetitorCompanyListExcelExporter competitorListExcelExporter, IRepository<CompetitorCompany> ComRespository)
        {
            _ComRespository = ComRespository;
            _competitorListExcelExporter = competitorListExcelExporter;

        }
        public async Task<PagedResultOutput<CCListDto>> GetCompetitorCompany(GetCompetitorCompanyInput input)
         {
             var query = _ComRespository.GetAll()
                 .WhereIf(
                     !input.Filter.IsNullOrWhiteSpace(),
                     u =>
                         u.CompetitorCompanyName.Contains(input.Filter) ||
                         u.CompetitorCompanyCode.Contains(input.Filter)
                         );
             
             var company = await query
                 .OrderBy(input.Sorting)
                 .PageBy(input)
                 .ToListAsync();
             var companyListDtos = company.MapTo<List<CCListDto>>();
             var companyCount = await query.CountAsync();
             return new PagedResultOutput<CCListDto>(
               companyCount,
               companyListDtos
               );
         }

        public async Task<FileDto> GetCompetitorCompanyToExcel()
        {

            var cc = _ComRespository
                .GetAll();
            var ccListDtos = cc.MapTo<List<CCListDto>>();


            return _competitorListExcelExporter.ExportToFile(ccListDtos);
        }

        public async Task CreateOrUpdateCompetitorCompany(CreateCCompanyInput input)
        {
            if (input.Id != 0)
            {
                await UpdateCompanyAsync(input);
            }
            else
            {
                await CreateCompetitorCompanyAsync(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_AddressBook_CompetitorCompany_CreateNewCompetitorCompany)]

        public virtual async Task CreateCompetitorCompanyAsync(CreateCCompanyInput input)
        {
            var company = input.MapTo<CompetitorCompany>();
            DateTime myUtcDateTime = DateTime.Now;

            company.LastModificationTime = DateTime.Now.AddHours(-12);
            var val = _ComRespository
              .GetAll().Where(p => p.CompetitorCompanyName == input.CompetitorCompanyName || p.CompetitorCompanyCode == input.CompetitorCompanyCode).FirstOrDefault();
            if (val == null)
            {
                await _ComRespository.InsertAsync(company);
            }
            else
            {
                throw new UserFriendlyException("Ooops!", "Duplicate Data Occured in Competitor Name '" + input.CompetitorCompanyName + "' or Competitor Code '" + input.CompetitorCompanyCode + "'...");
            }
        }


        [AbpAuthorize(AppPermissions.Pages_Tenant_AddressBook_CompetitorCompany_EditCompetitorCompany)]
        public virtual async Task UpdateCompanyAsync(CreateCCompanyInput input)
        {
            var company = input.MapTo<CompetitorCompany>();
            company.CompetitorCompanyName = input.CompetitorCompanyName;
            company.CompetitorCompanyCode = input.CompetitorCompanyCode;
            var val = _ComRespository
              .GetAll().Where(p => (p.CompetitorCompanyName == input.CompetitorCompanyName || p.CompetitorCompanyCode == input.CompetitorCompanyCode) && p.Id != input.Id).FirstOrDefault();
            if (val == null)
            {
                await _ComRespository.UpdateAsync(company);
            }
            else
            {
                throw new UserFriendlyException("Ooops!", "Duplicate Data Occured in Competitor Name '" + input.CompetitorCompanyName + "' or Competitor Code '" + input.CompetitorCompanyCode + "'...");
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_AddressBook_CompetitorCompany_DeleteCompetitorCompany)]
        public async Task DeleteCompetitorCompany(IdInput input)
        {
            
                        await _ComRespository.DeleteAsync(input.Id);
           
        }
        public async Task<GetCompany> GetCompanyForEdit(NullableIdInput<long> input)
        {
            var output = new GetCompany
            {
            };

            var country = _ComRespository
                .GetAll().Where(p => p.Id == input.Id).FirstOrDefault();

            output.company = country.MapTo<CreateCCompanyInput>();

            return output;

        }  

    }
}
