﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.CompetitorCompanies.Dto;
using TIBS.SAFCO.DataExporting.Excel.EpPlus;
using TIBS.SAFCO.Dto;

namespace TIBS.SAFCO.CompetitorCompanies.Export
{
    class CompetitorCompanyListExcelExporter : EpPlusExcelExporterBase, ICompetitorCompanyListExcelExporter
    {


        public FileDto ExportToFile(List<CCListDto> ccListDtos)
        {
            return CreateExcelPackage(
                "CompetitorCompany.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("CompetitorCompany"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("CompetitorCompanyCode"),
                        L("CompetitorCompanyName")
                        //L("IsDeleted"),
                        //L("DeleterUserId"),
                        //L("DeletionTime"),
                        //L("LastModificationTime"),
                        //L("LastModifierUserId"),
                        //L("CreationTime"),
                        //L("CreatorUserId")
                    );

                    AddObjects(
                        sheet, 2, ccListDtos,
                        _ => _.CompetitorCompanyCode,

                        _ => _.CompetitorCompanyName
                        //_ => _.IsDeleted,
                        //_ => _.DeleterUserId,
                        //_ => _.DeletionTime,
                        //_ => _.LastModificationTime,
                        //_ => _.LastModifierUserId,
                        //_ => _.CreationTime,
                        //_ => _.CreatorUserId

                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(2);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 2; i++)
                    {

                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}

