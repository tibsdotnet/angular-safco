﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.CompetitorCompanies.Dto;
using TIBS.SAFCO.Dto;

namespace TIBS.SAFCO.CompetitorCompanies.Export
{
    public interface ICompetitorCompanyListExcelExporter
    {
        FileDto ExportToFile(List<CCListDto> ccListDtos);
    }
}
