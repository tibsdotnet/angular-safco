﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using TIBS.SAFCO.Authorization.Dto;

namespace TIBS.SAFCO.Authorization.Users.Dto
{
    public class GetUserPermissionsForEditOutput : IOutputDto
    {
        public List<FlatPermissionDto> Permissions { get; set; }

        public List<string> GrantedPermissionNames { get; set; }
    }
}