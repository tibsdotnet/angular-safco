﻿using System;
using Abp.Application.Services.Dto;

namespace TIBS.SAFCO.Authorization.Users.Dto
{
    public class GetUserForEditOutput : IOutputDto
    {
        public string ProfilePictureId { get; set; }

        public UserEditDto User { get; set; }

        public UserRoleDto[] Roles { get; set; }

        public UserTargetDto UserT { get; set; }
    }
}