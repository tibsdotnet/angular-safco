using System.Collections.Generic;
using TIBS.SAFCO.Authorization.Users.Dto;
using TIBS.SAFCO.Dto;

namespace TIBS.SAFCO.Authorization.Users.Exporting
{
    public interface IUserListExcelExporter
    {
        FileDto ExportToFile(List<UserListDto> userListDtos);
    }
}