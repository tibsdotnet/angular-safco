﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.Employees.Dto;

namespace TIBS.SAFCO.Employees
{
   public interface IEmployeeAppService:IApplicationService
    {
       Task<PagedResultOutput<EmployeeViewDto>> GetEmployee(GetEmployeeInput input);
       Task<GetEmployee> GetEmployeeFofEdit(NullableIdInput input);
       Task CreateOrUpdateEmployee(CreateEmployeeInput input);
       Task DeleteEmployee(IdInput input);
       Task UpdateAccount(AccountInput input);
      string GetProfile(NullableIdInput<int> input);
      Task<FileDto> GetEmployeeToExcel();
    }
}
