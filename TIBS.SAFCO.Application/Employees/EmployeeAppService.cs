﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Employees.Dto;
using Abp.Linq.Extensions;
using Abp.Extensions;
using Abp.Linq;
using Abp.AutoMapper;
using System.Data.Entity;
using System.Linq.Dynamic;
using TIBS.SAFCO.Regions;
using TIBS.SAFCO.Authorization.Users.Dto;
using TIBS.SAFCO.Authorization.Roles;
using TIBS.SAFCO.Dto;
using Abp.Authorization;
using TIBS.SAFCO.Authorization;
using TIBS.SAFCO.Employees.Export;

namespace TIBS.SAFCO.Employees
{
    public class EmployeeAppService:SAFCOAppServiceBase,IEmployeeAppService
    {
        private readonly IRepository<Employee> _EmployeeRepository;
        private readonly IRepository<Region> _RegionRepoistory;
        private readonly RoleManager _roleManager;
        private readonly IEmployeeListExcel _EmployeeListExcel;

        public EmployeeAppService(IEmployeeListExcel EmployeeListExcel, IRepository<Employee> EmployeeRepository, RoleManager roleManager, IRepository<Region> RegionRepoistory)
        {
            _EmployeeRepository = EmployeeRepository;
            _RegionRepoistory = RegionRepoistory;
            _roleManager = roleManager;
            _EmployeeListExcel = EmployeeListExcel;
        }
        public async Task<PagedResultOutput<EmployeeViewDto>> GetEmployee(GetEmployeeInput input)
        {
            long userid = (int)AbpSession.UserId;
            var user = await UserManager.GetUserByIdAsync(userid);
            var roleid = user.Roles.ToList();

            string[] userroles = new string[roleid.Count];
            int i = 0;
            foreach (var role in roleid)
            {
                int rol = role.RoleId;
                var userrolesquery = _roleManager.Roles.Where(p => p.Id == rol).OrderBy(p => p.DisplayName).FirstOrDefault();
                userroles[i] = userrolesquery.DisplayName;
                i++;
            }
            var uroles = userroles.ToList();


            var employeequery = _EmployeeRepository.GetAll().Where(r => r.UserId == userid)
                               .WhereIf(
                               !input.Filter.IsNullOrWhiteSpace(),
                               u=>
                                u.FirstName.Contains(input.Filter)||
                               u.LastName.Contains(input.Filter)||
                               u.Address.Contains(input.Filter)||
                               u.Phone.Contains(input.Filter)
                               );

            if (uroles.Contains("Admin") || uroles.Contains("Management") || uroles.Contains("Audit/Reporting"))
            {
                employeequery = _EmployeeRepository.GetAll()
                               .WhereIf(
                               !input.Filter.IsNullOrWhiteSpace(),
                               u =>
                                u.FirstName.Contains(input.Filter) ||
                               u.LastName.Contains(input.Filter) ||
                               u.Address.Contains(input.Filter) ||
                               u.Phone.Contains(input.Filter)
                               );
            }
            var employeedto = from c in employeequery select new EmployeeViewDto { Id = c.Id, Address = c.Address, FirstName = c.FirstName, LastName = c.LastName, PhoneNumber = c.Phone, ProfilePicturePath = c.PhotoPath, UserStatusForEmoloyee = c.UserId != null ? "User Created" : "Not Yet Created" };
            var employeeCount = await employeedto.CountAsync();
            var employess = await employeedto
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();
            var employeedtos = employess.MapTo<List<EmployeeViewDto>>();
            return new PagedResultOutput<EmployeeViewDto>(employeeCount, employeedtos);
        }
        public async Task<GetEmployee> GetEmployeeFofEdit(NullableIdInput input)
        {
            var employeeoutput = _EmployeeRepository.GetAll().Where(p => p.Id == input.Id).FirstOrDefault();
            var regindto =await _RegionRepoistory.GetAll().Select(r => new RegionInputDto { RegionId = r.Id, RegionName = r.Name }).ToArrayAsync();
            var userRoleDtos = (await _roleManager.Roles
               .OrderBy(r => r.DisplayName)
               .Select(r => new UserRoleDto
               {
                   RoleId = r.Id,
                   RoleName = r.Name,
                   RoleDisplayName = r.DisplayName
               })
               .ToArrayAsync());
            var output = new GetEmployee {Role=userRoleDtos,Region=regindto};
            output.Employee = employeeoutput.MapTo<CreateEmployeeInput>();
            return output;
        }

        public async Task<FileDto> GetEmployeeToExcel()
        {

            var emp = _EmployeeRepository
                .GetAll();
            var employeedto = from c in emp select new EmployeeViewDto { Id = c.Id, Address = c.Address, FirstName = c.FirstName, LastName = c.LastName, PhoneNumber = c.Phone };
            var employeeListDto = employeedto.MapTo<List<EmployeeViewDto>>();
            return _EmployeeListExcel.ExportToFile(employeeListDto);
        }

        public async Task CreateOrUpdateEmployee(CreateEmployeeInput input)
        {
            if(input.Id==0)
            {
                await CreateEmployee(input);

            }
            else
            {
                await UpdateEmployee(input);

            }

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Employee_CreateEmployee)]
        public async Task CreateEmployee(CreateEmployeeInput input)
        {
            var employee = input.MapTo<Employee>();
            await _EmployeeRepository.InsertAsync(employee);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Employee_EditEmployee)]
        public async Task UpdateEmployee(CreateEmployeeInput input)
        {
            var employee = input.MapTo<Employee>();
            await _EmployeeRepository.UpdateAsync(employee);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Employee_DeleteEmployee)]
        public async Task DeleteEmployee(IdInput input)
        {
            await _EmployeeRepository.DeleteAsync(input.Id);
        }
        public virtual async Task UpdateAccount(AccountInput input)
        {
            if (input.AccountId == 0)
            {
                var query = _EmployeeRepository.GetAll();
                var emp = (from c in query where c.Id == input.Id select c).FirstOrDefault();





                emp.UserId = (UserManager.Users.Select(x => (int?)x.Id).Max() ?? 0);

                var employee = emp.MapTo<Employee>();
                try
                {
                    await _EmployeeRepository.UpdateAsync(employee);
                }
                catch (Exception ex)
                {
                }

            }

        }
        public string GetProfile(NullableIdInput<int> input)
        {
            string Path = "";
            
            var query = (from r in _EmployeeRepository.GetAll()
                         where r.Id == input.Id
                         orderby r.Id descending
                         select r).FirstOrDefault();
            if (query != null)
            {
                Path = query.PhotoPath;

            }

            return Path;
        }
    }
}
