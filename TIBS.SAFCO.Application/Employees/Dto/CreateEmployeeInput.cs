﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;

namespace TIBS.SAFCO.Employees.Dto
{
    [AutoMapTo(typeof(Employee))]
    public class CreateEmployeeInput:IInputDto
    {
        public int Id { get; set; }
        public  string LastName { get; set; }
        public  string FirstName { get; set; }
        public  int? RoleId { get; set; }
        public  string TitleOfCourtesy { get; set; }
        public  Nullable<System.DateTime> BirthDate { get; set; }
        public  Nullable<System.DateTime> HireDate { get; set; }
        public  string Address { get; set; }
        
        public  int? RegionId { get; set; }
        public  string POBoxNo { get; set; }
        public  string Phone { get; set; }
        public  string Extension { get; set; }

        public  string Notes { get; set; }

        public string PhotoPath { get; set; }

        public Nullable<System.DateTime> Published { get; set; }
        public bool Archived { get; set; }

        public string Email { get; set; }
        public long? UserId { get; set; }
    }
}
