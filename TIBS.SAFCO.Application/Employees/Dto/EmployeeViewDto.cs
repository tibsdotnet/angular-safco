﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;

namespace TIBS.SAFCO.Employees.Dto
{
    [AutoMapFrom(typeof(Employee))]
    public class EmployeeViewDto:FullAuditedEntityDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string ProfilePicturePath { get; set; }
        public string UserStatus { get; set; }
        public string PhoneNumber { get; set; }
        public string UserStatusForEmoloyee { get; set; }
    }
}
