﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.Authorization.Users.Dto;
namespace TIBS.SAFCO.Employees.Dto
{
    public class GetEmployee:IOutputDto
    {
        public CreateEmployeeInput Employee { get; set; }
        public RegionInputDto[] Region { get; set; }
        public UserRoleDto[] Role { get; set; }
    }
}
