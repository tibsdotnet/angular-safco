﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.Employees.Dto;

namespace TIBS.SAFCO.Employees.Export
{
    public interface IEmployeeListExcel
    {
        FileDto ExportToFile(List<EmployeeViewDto> employeeListDto);
    }
}
