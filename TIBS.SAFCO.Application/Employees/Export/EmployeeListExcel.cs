﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.DataExporting.Excel.EpPlus;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.Employees.Dto;

namespace TIBS.SAFCO.Employees.Export
{
    public class EmployeeListExcel : EpPlusExcelExporterBase, IEmployeeListExcel
    {
        public FileDto ExportToFile(List<EmployeeViewDto> employeeListDto)
        {
            return CreateExcelPackage(
                "Employee.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Employee"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("FirstName"),
                        L("LastName"),
                        L("PhoneNumber"),
                        L("Address"),
                        L("UserStatus")
                    );

                    AddObjects(
                        sheet, 2, employeeListDto,
                        _ => _.FirstName,
                        _ => _.LastName,
                        _ => _.PhoneNumber,
                        _ => _.Address,
                        _ => _.UserStatus
                        

                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(5);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 5; i++)
                    {

                        sheet.Column(i).AutoFit();
                    }
                });
        }

    }
}

