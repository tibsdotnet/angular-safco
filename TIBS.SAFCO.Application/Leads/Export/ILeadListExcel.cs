﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.Leads.Dto;

namespace TIBS.SAFCO.Leads.Export
{
    public interface ILeadListExcel
    {
        FileDto ExportToFile(List<LeadViewDto> leadListDtos);
    }
}
