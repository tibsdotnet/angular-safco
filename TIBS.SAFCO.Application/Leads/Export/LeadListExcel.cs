﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.DataExporting.Excel.EpPlus;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.Leads.Dto;

namespace TIBS.SAFCO.Leads.Export
{
    public class LeadListExcel : EpPlusExcelExporterBase, ILeadListExcel
    {
        public FileDto ExportToFile(List<LeadViewDto> leadListDtos)
        {
            return CreateExcelPackage(
                "Lead.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Lead"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("CompanyName"),
                        L("AllotedTo"),
                        L("ContributedBy"),
                        L("ActualAmount"),
                        L("ActualClosure"),
                        L("ExpAmount"),
                        L("ExpClosure"),
                        L("Status")
                    );

                    AddObjects(
                        sheet, 2, leadListDtos,
                        _ => _.CompanyName,
                        _ => _.AllotedTo,
                        _ => _.ContributedBy,
                        _ => _.ActualAmount,
                        _ => _.ActualClosure,
                        _ => _.ExpAmount,
                        _ => _.ExpClosure,
                        _ => _.Status
                       

                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(8);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 8; i++)
                    {

                        sheet.Column(i).AutoFit();
                    }
                });
        }

    }
}
