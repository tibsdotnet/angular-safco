﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using Abp.Application.Services.Dto;
using TIBS.SAFCO.Leadss;

namespace TIBS.SAFCO.Leads.Dto
{
    [AutoMapTo(typeof(Lead))]
    public class CreateLeadInput:IInputDto
    {
        public int Id { get; set; }
        public  string Title { get; set; }

        public  string Description { get; set; }
        
        public  int CompanyId { get; set; }

       
        public  int? LeadSourceId { get; set; }

       
        public  int MileStoneId { get; set; }

        public  int Rating { get; set; }

        public  float ExpectedAmount { get; set; }

        public  Nullable<System.DateTime> ExpectedClosure { get; set; }

        public  float ActualAmount { get; set; }

        public  Nullable<System.DateTime> ActualClosure { get; set; }

       
        public  string OrderNo { get; set; }

        public  bool Archived { get; set; }

        public  Nullable<System.DateTime> Published { get; set; }

        
        public  int EmployeeId { get; set; }

        public  int? ChefId { get; set; }

        public int? CompetitorId { get; set; }
    }
}
