﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Authorization.Users.Dto;

namespace TIBS.SAFCO.Leads.Dto
{
    public class GetLead:IOutputDto
    {
        public CreateLeadInput Lead { get; set; }
        public EmployeeListDto[] Employee { get; set; }
        public EmployeeListDto[] UserList { get; set; }
        public string CompanyName { get; set; }
    }
}
