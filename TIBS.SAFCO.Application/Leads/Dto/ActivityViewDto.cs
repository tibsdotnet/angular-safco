﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.LeadActivities;

namespace TIBS.SAFCO.Leads.Dto
{
    [AutoMapFrom(typeof(LeadActivity))]
    public class ActivityViewDto:FullAuditedEntityDto
    {
        public string Name { get; set; }
        public DateTime ActivateTime { get; set; }
        public string Notes { get; set; }
        public string ActivationName { get; set; }
        public bool LoginUser { get; set; }
        public string UserId { get; set; }
        public string Title { get; set; }
        public long? CommentedId { get; set; }
    }
}
