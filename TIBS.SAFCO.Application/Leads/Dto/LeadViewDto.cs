﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using TIBS.SAFCO.Leadss;

namespace TIBS.SAFCO.Leads.Dto
{
    [AutoMapFrom(typeof(Lead))]
    public class LeadViewDto:FullAuditedEntityDto
    {
        public int Id { get; set; }
        public string CompanyName { get; set; }
        public string Status { get; set; }
        public string AllotedTo { get; set; }
        public string ContributedBy { get; set; }
        public string ExpAmount { get; set; }
        public string ExpClosure { get; set; }
        public string ActualAmount { get; set; }
        public string ActualClosure { get; set; }
        public int CompanyId { get; set; }
        public int MileStoneId { get; set; }
        public int? CompetitorId { get; set; }
    }
}
