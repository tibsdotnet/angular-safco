﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.SAFCO.Leads.Dto
{
    public class EmployeeListDto:IOutputDto
    {
        public string EmployeeName { get; set; }
        public int EmployeeId { get; set; }
    }
}
