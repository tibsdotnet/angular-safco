﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using TIBS.SAFCO.LeadActivities;
using Abp.Application.Services.Dto;

namespace TIBS.SAFCO.Leads.Dto
{
    [AutoMapTo(typeof(LeadActivity))]
    public class ActivityInput:IInputDto
    {
        public  string Notes { get; set; }
        public  string Title { get; set; }
        
        public  int? LeadId { get; set; }
       
        public  int? ActivityId { get; set; }
       
        public  long? UserId { get; set; }
    }
}
