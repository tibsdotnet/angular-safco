﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Companies.Dto;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.Leads.Dto;

namespace TIBS.SAFCO.Leads
{
    public interface ILeadAppService:IApplicationService
    {
        Task<PagedResultOutput<LeadViewDto>> GetLeads(GetLeadInput input);
        Task<GetContacts> GetContactForEdit(NullableIdInput<long> input);
        List<ActivityViewDto> GetActivities(NullableIdInput<long> input);
        Task CreateActivityInput(ActivityInput input);

        Task<FileDto> GetLeadToExcel();

        Task<Getcompany> GetLeadEdit(NullableIdInput<long> input);

        Task UpdateEsimation(CreateLeadInput input);
        Task UpdateStatus(CreateLeadInput input);
        Task<PagedResultOutput<LeadScheduleListDto>> Getleadschedules(GetLeadScheduleInput input);
        Task<GetSchedule> GetMainScheduleForEdit(NullableIdInput<long> input);
        Task CreateOrUpdateSchedule(CreateScheduleInput input);
        Task DeletetSchedule(IdInput input);
    }
}
