﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using TIBS.SAFCO.Leadss;
using Abp.Domain.Repositories;
using Abp.Application.Services.Dto;
using TIBS.SAFCO.Leads.Dto;
using Abp.Linq.Extensions;
using Abp.Extensions;
using System.Data.Entity;
using TIBS.SAFCO.Compnaies;
using TIBS.SAFCO.Companies.Dto;
using TIBS.SAFCO.Regions;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.CompanyContacts;
using TIBS.SAFCO.LeadActivities;
using Abp.Runtime.Session;
using TIBS.SAFCO.Leads.Export;
using TIBS.SAFCO.Authorization.Roles;
using TIBS.SAFCO.LeadSources;
using TIBS.SAFCO.MileStoness;
using TIBS.SAFCO.CompetitorCompaniesss;
using TIBS.SAFCO.Schedulee;
using System.Linq.Dynamic;
using TIBS.SAFCO.Employees;

namespace TIBS.SAFCO.Leads
{
    public class LeadAppService:SAFCOAppServiceBase,ILeadAppService
    {
        private readonly IRepository<Lead> _LeadRepository;
        private readonly IRepository<Company> _CompanyRepository;
        private readonly IRepository<Region> _RegionRepository;
        private readonly IRepository<CompanyContact> _CompanyContactRepository;
        private readonly IRepository<LeadActivity> _LeadActivityRepository;
        private readonly ILeadListExcel _LeadListExcel;
        private readonly RoleManager _roleManager;
        private readonly IRepository<LeadSource> _LeadSourceRepository;
        private readonly IRepository<MileStone> _StatusRepositoy;
        private readonly IRepository<Schedules> _ScheduleRepository;
        private readonly IRepository<CompetitorCompany> _CompetitorCompanyRepository;
        private readonly IRepository<Employee> _EmployeeRepository;
        public LeadAppService(ILeadListExcel LeadListExcel,RoleManager roleManager,IRepository<Lead> LeadRepository, IRepository<Company> CompanyRepository, IRepository<Region> RegionRepository, IRepository<CompanyContact> CompanyContactRepository, IRepository<LeadActivity> LeadActivityRepository,
                              IRepository<LeadSource> LeadSourceRepository, IRepository<MileStone> StatusRepositoy, IRepository<CompetitorCompany> CompetitorCompanyRepository, IRepository<Schedules> ScheduleRepository, IRepository<Employee> EmployeeRepository) 
        {
            _LeadRepository = LeadRepository;
            _CompanyRepository = CompanyRepository;
            _RegionRepository = RegionRepository;
            _CompanyContactRepository = CompanyContactRepository;
            _LeadActivityRepository = LeadActivityRepository;
            _LeadListExcel = LeadListExcel;
            _roleManager = roleManager;
            _LeadSourceRepository = LeadSourceRepository;
            _StatusRepositoy = StatusRepositoy;
            _CompetitorCompanyRepository = CompetitorCompanyRepository;
            _ScheduleRepository = ScheduleRepository;
            _EmployeeRepository = EmployeeRepository;
        }
        public async Task<PagedResultOutput<LeadViewDto>> GetLeads(GetLeadInput input)
        {
            long userid = (int)AbpSession.UserId;
            var user = await UserManager.GetUserByIdAsync(userid);
            var roleid = user.Roles.ToList();

            string[] userroles = new string[roleid.Count];
            int i = 0;
            foreach (var role in roleid)
            {
                int rol = role.RoleId;
                var userrolesquery = _roleManager.Roles.Where(p => p.Id == rol).OrderBy(p => p.DisplayName).FirstOrDefault();
                userroles[i] = userrolesquery.DisplayName;
                i++;
            }
            var uroles = userroles.ToList();
            var lead = _LeadRepository.GetAll().Where(p => p.Id == 0);
            if (uroles.Contains("Admin") || uroles.Contains("Management") || uroles.Contains("Audit/Reporting"))
            {
                lead = _LeadRepository.GetAll().Where(p => p.Archived == false).WhereIf(!input.Filter.IsNullOrWhiteSpace(),
                p => p.Companys.Title.Contains(input.Filter) ||
                p.MileStones.MileStoneName.Contains(input.Filter) ||
                p.ExpectedAmount.ToString().Contains(input.Filter) ||
                p.ExpectedClosure.ToString().Contains(input.Filter) ||
                p.ActualAmount.ToString().Contains(input.Filter) ||

                p.ActualClosure.ToString().Contains(input.Filter));
            }
            else
            {
                lead = (from l in _LeadRepository.GetAll() join c in _CompanyRepository.GetAll() on l.CompanyId equals c.Id where (c.ManagedById == userid || c.ContributedId == userid) && l.Archived == false select l)
                    .WhereIf(!input.Filter.IsNullOrWhiteSpace(),
                 p => p.Companys.Title.Contains(input.Filter) ||
                 p.MileStones.MileStoneName.Contains(input.Filter) ||
                 p.ExpectedAmount.ToString().Contains(input.Filter) ||
                 p.ExpectedClosure.ToString().Contains(input.Filter) ||
                 p.ActualAmount.ToString().Contains(input.Filter) ||

                 p.ActualClosure.ToString().Contains(input.Filter));
            }

            var joindto = from l in lead join c in _CompanyRepository.GetAll() on l.CompanyId equals c.Id select new { l, c };
            var leaddtos = from p in joindto select new LeadViewDto { CompanyId = p.c.Id, CompanyName = p.c.Title, AllotedTo = p.c.ManagedId != null ? p.c.ManagedUser.FirstName : "", ContributedBy = p.c.ContributedId != null ? p.c.AbpUser.UserName : "", ActualAmount = p.l.ActualAmount.ToString(), ActualClosure = p.l.ActualClosure.ToString(), ExpAmount = p.l.ExpectedAmount.ToString(), ExpClosure = p.l.ExpectedClosure.ToString(), Id = p.l.Id, Status = p.l.MileStones.MileStoneName };
            var leadcount = await lead.CountAsync();
            var leaddtoss = await leaddtos
                                 .OrderBy(input.Sorting)
                                 .PageBy(input)
                                 .ToListAsync();
            var leadviewdto = leaddtoss.MapTo<List<LeadViewDto>>();
            foreach (var ann in leadviewdto)
            {
                if (ann.ExpClosure != "")
                {
                    ann.ExpClosure = DateTime.Parse(ann.ExpClosure).ToString("dd-MMM-yyyy");
                }
                if (ann.ActualClosure != "")
                {
                    ann.ActualClosure = DateTime.Parse(ann.ActualClosure).ToString("dd-MMM-yyyy");
                }
            }
            return new PagedResultOutput<LeadViewDto>(leadcount, leadviewdto);
        }
        public async Task<GetContacts> GetContactForEdit(NullableIdInput<long> input)
        {
            var Region = await _RegionRepository.GetAll().Select(r => new RegionInputDto { RegionName = r.Name, RegionId = r.Id }).ToArrayAsync();
            var output = new GetContacts { Region = Region };
            var Contact = await _CompanyContactRepository.GetAll().Where(p => p.Id == input.Id).FirstOrDefaultAsync();
            output.CompanyContact = Contact.MapTo<CreateContactInput>();
            return output;
        }
        public List<ActivityViewDto> GetActivities(NullableIdInput<long> input)
        {
            long userid = (long)AbpSession.GetUserId();
            var leadactivity = _LeadActivityRepository.GetAll().Where(p => p.LeadId == input.Id);
            var output = from c in leadactivity select new ActivityViewDto { Title = c.Title, Name = c.UserId != null ? c.AbpUser.UserName : "", ActivateTime = c.CreationTime, Notes = c.Notes, ActivationName = c.Activities.ActivityName, LoginUser = c.UserId == userid ? true : false, CommentedId = (long)c.UserId };
            var output1 = output.ToList();
            foreach (var outp in output1)
            {

                outp.UserId = (from c in _EmployeeRepository.GetAll() where c.UserId == outp.CommentedId select c.PhotoPath).FirstOrDefault();
            }
            return output1;
        }
        public async Task CreateActivityInput(ActivityInput input)
        {
            var activity = input.MapTo<LeadActivity>();
            activity.UserId = AbpSession.GetUserId();
            await _LeadActivityRepository.InsertAsync(activity);
                
        }

        public async Task<FileDto> GetLeadToExcel()
        {

            var lead = _LeadRepository
                .GetAll();
            var joindto = from l in lead join c in _CompanyRepository.GetAll() on l.CompanyId equals c.Id select new { l, c };
            var leaddtos = from p in joindto select new LeadViewDto { CompanyId = p.c.Id, CompanyName = p.c.Title, AllotedTo = p.c.ManagedUser.FirstName, ContributedBy = p.c.AbpUser.UserName, ActualAmount = p.l.ActualAmount.ToString(), ActualClosure = p.l.ActualClosure.ToString(), ExpAmount = p.l.ExpectedAmount.ToString(), ExpClosure = p.l.ExpectedClosure.ToString(), Id = p.l.Id, Status = p.l.MileStones.MileStoneName };
            var leadListDtos = leaddtos.MapTo<List<LeadViewDto>>();
            foreach (var ann in leadListDtos)
            {
                if (ann.ExpClosure != "")
                {
                    ann.ExpClosure = DateTime.Parse(ann.ExpClosure).ToString("dd-MMM-yyyy");
                }
                if (ann.ActualClosure != "")
                {
                    ann.ActualClosure = DateTime.Parse(ann.ActualClosure).ToString("dd-MMM-yyyy");
                }
            }
            return _LeadListExcel.ExportToFile(leadListDtos);
        }

        public async Task<Getcompany> GetLeadEdit(NullableIdInput<long> input)
        {
            long userid = (int)AbpSession.UserId;
            var leadSource = await _LeadSourceRepository.GetAll().Select(p => new LeadSourceInputDto { LeadSourceId = p.Id, LeadSourceName = p.Tittle }).ToArrayAsync();
            var Satus = await _StatusRepositoy.GetAll().Select(r => new StatusInputDto { StatusId = r.Id, StatusName = r.MileStoneName }).ToArrayAsync();
            var Region = await _RegionRepository.GetAll().Select(r => new RegionInputDto { RegionName = r.Name, RegionId = r.Id }).ToArrayAsync();
            var competitor = await _CompetitorCompanyRepository.GetAll().Select(r => new CompetitorInputDto { CompetitorId = r.Id, CompetitorName = r.CompetitorCompanyName }).ToArrayAsync();
            var userDtos = (await UserManager.Users.Where(p => p.Id == userid)
                         .OrderBy(r => r.UserName)
                         .Select(r => new UserListInputDtos
                         {
                             UserId = r.Id,
                             UserName = r.UserName
                         }).ToArrayAsync());

            
            try
            {
                var companydto1 = (from c in _CompanyRepository.GetAll() join l in _LeadRepository.GetAll() on c.Id equals l.CompanyId where c.Id == input.Id select new { c, l }).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            var companydto = (from c in _CompanyRepository.GetAll() join l in _LeadRepository.GetAll() on c.Id equals l.CompanyId where c.Id == input.Id select new { c, l }).FirstOrDefault();
            var leaddtos = new LeadViewDto { CompetitorId = companydto.l.CompetitorId != null ? companydto.l.CompetitorId : 0, MileStoneId = companydto.l.MileStoneId, CompanyId =companydto.l.CompanyId, CompanyName = companydto.c.Title, AllotedTo = companydto.c.ManagedId != null ? companydto.c.ManagedUser.FirstName : "", ContributedBy = companydto.c.ContributedId != null ? companydto.c.AbpUser.UserName : "", ActualAmount = companydto.l.ActualAmount.ToString(), ActualClosure = companydto.l.ActualClosure.ToString(), ExpAmount = companydto.l.ExpectedAmount.ToString(), ExpClosure = companydto.l.ExpectedClosure.ToString(), Id = companydto.l.Id, Status = companydto.l.MileStones.MileStoneName };


            var output = new Getcompany
            {
                Region = Region,
                LeadSource = leadSource,
                StatusName = Satus,
                UserName = userDtos,
                Lead = leaddtos,
                Competitor = competitor


            };
            output.Company = companydto.c.MapTo<CreateCompanyInput>();
            return output;
        }

        public async Task UpdateEsimation(CreateLeadInput input)
        {
            var lead = _LeadRepository.GetAll().Where(p => p.Id == input.Id).FirstOrDefault();
            lead.ExpectedAmount = input.ExpectedAmount;
            lead.ExpectedClosure = input.ExpectedClosure;
            var leadinput = lead.MapTo<Lead>();
            try
            {
                await _LeadRepository.UpdateAsync(leadinput);

            }
            catch (Exception ex)
            {

            }


        }
        public async Task UpdateStatus(CreateLeadInput input)
        {
            var lead = await _LeadRepository.GetAll().Where(p => p.Id == input.Id).FirstOrDefaultAsync();
            lead.MileStoneId = input.MileStoneId;
            var leadinput = lead.MapTo<Lead>();
            await _LeadRepository.UpdateAsync(leadinput);
            string Title = (from c in _StatusRepositoy.GetAll().Where(p => p.Id == input.MileStoneId) select c.MileStoneName).FirstOrDefault();
            ActivityInput activity = new ActivityInput();
            activity.Title = "Lead " + Title;
            activity.LeadId = input.Id;
            await CreateActivityInput(activity);

        }

        public async Task SaveCompetitor(CreateLeadInput input)
        {
            var lead = _LeadRepository.GetAll().Where(p => p.Id == input.Id).FirstOrDefault();
            lead.CompetitorId = input.CompetitorId;
            var leadinput = lead.MapTo<Lead>();
            await _LeadRepository.UpdateAsync(leadinput);
        }
        public async Task<PagedResultOutput<LeadScheduleListDto>> Getleadschedules(GetLeadScheduleInput input)
        {
            var query = (from r in _ScheduleRepository.GetAll().Where(v => v.LeadId == input.LeadId)
                         select r)
                         .WhereIf(
                  !input.Filter.IsNullOrWhiteSpace(),
                  u =>
                      u.LeadId == input.LeadId &&
                      u.Title.ToString().Contains(input.Filter) ||
                      u.ScheduleDate.ToString().Contains(input.Filter) ||
                      u.CompanyName.ToString().Contains(input.Filter) ||
                      u.ContactNo.ToString().Contains(input.Filter) ||
                      u.PersonName.ToString().Contains(input.Filter) ||
                      u.Remind.ToString().Contains(input.Filter) ||
                      u.Notes.ToString().Contains(input.Filter) ||
                      u.Email.ToString().Contains(input.Filter) ||
                      u.Id.ToString().Contains(input.Filter)
              );

            var leadschedulecount = await query.CountAsync();
            var leadschedule = await query.OrderBy(p => p.Id)
               .PageBy(input)
               .ToListAsync();

            var leadscheduledtos = leadschedule.MapTo<List<LeadScheduleListDto>>();

            foreach (var lis in leadscheduledtos)
            {
                var user = (from r in UserManager.Users.Where(r => r.Id == lis.UpdateByUser) select r.UserName).FirstOrDefault();
                if (user != null)
                {
                    lis.UserName = user;
                }
                if (lis.ScheduleDate != null)
                {
                    lis.ScheduleDate = DateTime.Parse(lis.ScheduleDate).ToString("dd-MMM-yyyy");

                }
                if (lis.Remind != null)
                {
                    lis.Remind = DateTime.Parse(lis.Remind).ToString("hh:mm");
                }
            }

            return new PagedResultOutput<LeadScheduleListDto>(leadschedulecount, leadscheduledtos);
        }
        public async Task<GetSchedule> GetMainScheduleForEdit(NullableIdInput<long> input)
        {
            var schdule =await _ScheduleRepository.GetAll().Where(p => p.Id == input.Id).FirstOrDefaultAsync();
            var output = new GetSchedule { };
            try
            {
                output.Schedule = schdule.MapTo<CreateScheduleInput>();

            }catch(Exception ex)
            {

            }
           
                //output.Schedule = schdule.MapTo<CreateMainProjectScheduleInput>();
               
            
           
            return output;
        }
        public async Task CreateOrUpdateSchedule(CreateScheduleInput input)
        {
            if (input.Id != 0)
            {
                await UpdateScheduleAsync(input);
            }
            else
            {
                await CreateScheduleAsync(input);
            }
        }
        public async Task CreateScheduleAsync(CreateScheduleInput input)
        {
            var Schedule = input.MapTo<Schedules>();
            await _ScheduleRepository.InsertAsync(Schedule);

        }
        public async Task UpdateScheduleAsync(CreateScheduleInput input)
        {
            var Schedule = input.MapTo<Schedules>();
            await _ScheduleRepository.UpdateAsync(Schedule);
        }
        public async Task DeletetSchedule(IdInput input)
        {

            await _ScheduleRepository.DeleteAsync(input.Id);

        }
    }

    public class CompetitorInputDto
    {
        public int CompetitorId { get; set; }
        public string CompetitorName { get; set; }
    }
}
