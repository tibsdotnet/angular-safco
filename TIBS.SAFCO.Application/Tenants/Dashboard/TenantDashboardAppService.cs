﻿using System.Linq;
using Abp;
using Abp.Authorization;
using TIBS.SAFCO.Authorization;
using TIBS.SAFCO.Tenants.Dashboard.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Abp.Domain.Repositories;
using TIBS.SAFCO.Employees;
using TIBS.SAFCO.Authorization.Roles;
using TIBS.SAFCO.MileStoness;
using TIBS.SAFCO.Announcements;

namespace TIBS.SAFCO.Tenants.Dashboard
{
    public class TenantDashboardAppService : SAFCOAppServiceBase, ITenantDashboardAppService
    {
        private readonly IRepository<Employee> _EmployeeRepository;
        private readonly RoleManager _roleManager;
        private readonly IRepository<MileStone> _MileStoneRepository;
        private readonly IRepository<Announcement> _AnnouncementRepository;
        public TenantDashboardAppService(IRepository<Employee> EmployeeRepository, RoleManager roleManager, IRepository<MileStone> MileStoneRepository, IRepository<Announcement> AnnouncementRepository)
        {
            _EmployeeRepository = EmployeeRepository;
            _roleManager = roleManager;
            _MileStoneRepository = MileStoneRepository;
            _AnnouncementRepository = AnnouncementRepository;
        }

        public async Task<Chart> MainProjectActivityRate(long id)
        {
            long userid = id;
            if (id == 0)
            {
                userid = (int)AbpSession.UserId;
            }
            Chart cr = new Chart();
            int emp = (from r in _EmployeeRepository.GetAll() where r.UserId == userid select r.Id).FirstOrDefault();

            var EmpId = new SqlParameter
            {
                ParameterName = "EmpId",
                Value = emp
            };
            var EmpId1 = new SqlParameter
            {
                ParameterName = "EmpId",
                Value = emp
            };
            var EmpId2 = new SqlParameter
            {
                ParameterName = "EmpId",
                Value = emp
            };
            var EmpId3 = new SqlParameter
            {
                ParameterName = "EmpId",
                Value = emp
            };

            using (var context = new SpDbContext())
            {
                try
                {
                    var list = context.Database.SqlQuery<sp_overall_Result>(
                   "sp_MonthlyOrder @EmpId",
                     EmpId).ToList();
                    
                    cr.MonthlyOrder = list.ToArray();

                    var list1 = context.Database.SqlQuery<sp_PMonthLead_Result>(
                   "sp_PMonthLeads @EmpId",
                     EmpId1).ToList();

                    cr.PMonthLeads = list1.ToArray();

                    var list2 = context.Database.SqlQuery<sp_PMonthLead_Result>(
                   "sp_LSixMonthLeads @EmpId",
                     EmpId2).ToList();

                    cr.LSixMonthLeads = list2.ToArray();

                    var list3 = context.Database.SqlQuery<sp_overall_Leads>(
                  "sp_MonthlyLeads @EmpId",
                    EmpId3).ToList();
                    var distinctservicestatus = list3.Select(y => y.Date).Distinct().ToArray();

                    List<charr> cl = new List<charr>();
                    foreach (var st in _MileStoneRepository.GetAll())
                    {
                        double[] arr = new double[distinctservicestatus.Length];
                        var data = list3.Where(z => z.Id == st.Id).ToList();
                        if (data.Count > 0)
                        {
                            int i = 0;
                            foreach (var da in data)
                            {
                                if (da.Amount != null)
                                {
                                    arr[i] = Convert.ToDouble(da.Amount);
                                }
                                i++;
                            }
                        }

                        cl.Add(new charr { data = arr.ToArray(), name = st.MileStoneName });
                    }
                    cr.MonthlyLeads = cl.ToArray();
                    cr.LeadStatus = distinctservicestatus;
                }
                catch (Exception ex)
                {

                }
                
            }

           
            return cr;
        }

        public async Task<board> SalesBoard()
        {
            board br = new board();

            long userid = (int)AbpSession.UserId;
            var user = await UserManager.GetUserByIdAsync(userid);
            var roleid = user.Roles.ToList();

            string[] userroles = new string[roleid.Count];
            int i = 0;
            foreach (var role in roleid)
            {
                int rol = role.RoleId;
                var userrolesquery = _roleManager.Roles.Where(p => p.Id == rol).OrderBy(p => p.DisplayName).FirstOrDefault();
                userroles[i] = userrolesquery.DisplayName;
                i++;
            }
            var uroles = userroles.ToList();

            if (uroles.Contains("Admin") || uroles.Contains("Management") || uroles.Contains("Audit/Reporting"))
            {
                using (var context = new SpDbContext())
                {
                    try
                    {
                        var list = context.Database.SqlQuery<sp_salesborad_Result>("sp_EmployeeBoard").ToList();
                        br.SalesBoard = list.ToArray();

                    }
                    catch (Exception ex)
                    {

                    }

                }
            }

            return br;
        }

        public async Task<AnnouncementNotify> NAnnouncement()
        {
            AnnouncementNotify AN = new AnnouncementNotify();
            using (var context = new SpDbContext())
            {
                try
                {
                    var data = context.Database.SqlQuery<Announcementdata>("sp_GetAnnouncement").ToList();
                    AN.Announcements = data.ToArray();
                }
                catch (Exception ex)
                {

                }
            }
            return AN;
        }
    }

    public class Chart
    {
        public Array MonthlyOrder { get; set; }

        public Array PMonthLeads { get; set; }

        public Array LSixMonthLeads { get; set; }

        public Array MonthlyLeads { get; set; }

        public Array LeadStatus { get; set; }
    }

    public class Announcementdata
    {
        public string Title { get; set; }

        public string Description { get; set; }
    }

    public class board
    {
        public Array SalesBoard { get; set; }
    }

    public class AnnouncementNotify
    {
        public Array Announcements { get; set; }
    }

    public class ListChart
    {
        public string name { get; set; }

        public decimal value { get; set; }
    }

    public partial class sp_overall_Result
    {
        public string Date { get; set; }

        public decimal Amount { get; set; }
    }

    public partial class sp_overall_Leads
    {
        public string Date { get; set; }

        public decimal Amount { get; set; }

        public string Status { get; set; }

        public int Id { get; set; }
    }

    public partial class sp_PMonthLead_Result
    {
        public string Status { get; set; }

        public decimal Amount { get; set; }

        public int Id { get; set; }
    }
    public partial class sp_salesborad_Result
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string src { get; set; }
        public int NewCustomer { get; set; }
        public decimal Amount { get; set; }
        public long Rank { get; set; }
    }

    public class charr
    {
        public Array data { get; set; }

        public string name { get; set; }
    }
}