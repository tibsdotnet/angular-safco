﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.SAFCO.Tenants.Dashboard
{
    public class SpDbContext : DbContext
    {
         public SpDbContext()
            : base("Default")
        {

        }

        /* This constructor is used by ABP to pass connection string defined in SAFCODataModule.PreInitialize.
         * Notice that, actually you will not directly create an instance of SAFCODbContext since ABP automatically handles it.
         */
        public SpDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }

        /* This constructor is used in tests to pass a fake/mock connection.
         */
        public SpDbContext(DbConnection dbConnection)
            : base(dbConnection, true)
        {

        }
    }
}
