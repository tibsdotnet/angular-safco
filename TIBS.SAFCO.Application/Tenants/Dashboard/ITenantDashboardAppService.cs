﻿using Abp.Application.Services;
using System.Threading.Tasks;
using TIBS.SAFCO.Tenants.Dashboard.Dto;

namespace TIBS.SAFCO.Tenants.Dashboard
{
    public interface ITenantDashboardAppService : IApplicationService
    {
        Task<Chart> MainProjectActivityRate(long id);

        Task<board> SalesBoard();

        Task<AnnouncementNotify> NAnnouncement();
    }
}
