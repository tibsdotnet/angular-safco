﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.Products.Dto;

namespace TIBS.SAFCO.Products
{
   public interface IProductAppService:IApplicationService
    {
       Task<PagedResultOutput<ProductListDto>> GetProduct(GetProductInput input);
       Task<GetProduct> GetProductForEdit(NullableIdInput<long> input);
       Task CreateOrEditProduct(CreateProductInput input);
      // Task CreateProduct(CreateProductInput input);
       Task DeleteProduct(IdInput input);
       Task<FileDto> GetProductToExcel();
    }
}
