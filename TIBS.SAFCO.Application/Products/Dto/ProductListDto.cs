﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using Abp.Application.Services.Dto;

namespace TIBS.SAFCO.Products.Dto
{
    [AutoMapFrom(typeof(Product))]
   public  class ProductListDto:FullAuditedEntityDto
    {
        public  string ProductName { get; set; }
        public  string Description { get; set; }
        public  double MinimumSalePrice { get; set; }
        public  double MaximumSalePrice { get; set; }
        public string ProductClassificationName { get; set; }
        public int Id { get; set; }
    }
}
