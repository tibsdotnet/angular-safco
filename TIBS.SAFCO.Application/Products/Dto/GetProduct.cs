﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Dto;
namespace TIBS.SAFCO.Products.Dto
{
    public class GetProduct:IOutputDto
    {
        public CreateProductInput Product { get; set; }
        public ProductClassificationDto[] ProductClassificationName { get; set; } 
    }
}
