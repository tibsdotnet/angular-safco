﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using System.ComponentModel.DataAnnotations;

namespace TIBS.SAFCO.Products.Dto
{
    [AutoMapTo(typeof(Product))]
   public class CreateProductInput:IInputDto
    {
        [Required]
        public  string ProductName { get; set; }
        public  string Description { get; set; }
        public  double MinimumSalePrice { get; set; }
        public  double MaximumSalePrice { get; set; }
        
        public  int? ProductClassificationId { get; set; }
        public int Id { get; set; }
    }
}
