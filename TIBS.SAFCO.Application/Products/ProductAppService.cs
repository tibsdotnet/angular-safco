﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Products.Dto;
using Abp.Linq.Extensions;
using Abp.Extensions;
using System.Data.Entity;
using Abp.AutoMapper;
using TIBS.SAFCO.ProductClassifications;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.Products.Export;
using Abp.Authorization;
using TIBS.SAFCO.Authorization;
using System.Linq.Dynamic;

namespace TIBS.SAFCO.Products
{
    public class ProductAppService:SAFCOAppServiceBase,IProductAppService
    {
        private readonly IRepository<Product> _ProductRepository;
        private readonly IRepository<ProductClassification> _ProductClassificationRepository;
        private readonly IProductListExcel _ProductListExcel;
        public ProductAppService(IProductListExcel ProductListExcel, IRepository<Product> ProductRepository, IRepository<ProductClassification> ProductClassificationRepository)
        {
            _ProductRepository = ProductRepository;
            _ProductClassificationRepository = ProductClassificationRepository;
            _ProductListExcel = ProductListExcel;
        }
        public async Task<PagedResultOutput<ProductListDto>> GetProduct(GetProductInput input)
        {
            var Productquery = _ProductRepository
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Id.ToString().Contains(input.Filter) ||
                        p.ProductName.Contains(input.Filter) ||
                         p.ProductClassifications.ProductClassificationName.Contains(input.Filter) ||
                         p.MaximumSalePrice.ToString().Contains(input.Filter) ||
                         p.MinimumSalePrice.ToString().Contains(input.Filter)

                );
            var productClassificationDtos = from c in Productquery select new ProductListDto { ProductName = c.ProductName,Description=c.Description, MaximumSalePrice = c.MaximumSalePrice, MinimumSalePrice = c.MinimumSalePrice, ProductClassificationName = c.ProductClassificationId != null ? c.ProductClassifications.ProductClassificationName : "", Id = c.Id };
            var productcount = await productClassificationDtos.CountAsync();
            var productdto = await productClassificationDtos
                                 .OrderBy(input.Sorting)
                                 .PageBy(input)
                                 .ToListAsync();
            var product = productdto.MapTo<List<ProductListDto>>();
            return new PagedResultOutput<ProductListDto>(productcount, product);
        }
        public async Task<FileDto> GetProductToExcel()
        {

            var product = _ProductRepository
                .GetAll().ToList();
            var productClassificationDtos = from c in product select new ProductListDto { ProductName = c.ProductName, Description = c.Description, MaximumSalePrice = c.MaximumSalePrice, MinimumSalePrice = c.MinimumSalePrice, ProductClassificationName = c.ProductClassificationId != null ? c.ProductClassifications.ProductClassificationName : "", Id = c.Id };
            var productListDtos = productClassificationDtos.MapTo<List<ProductListDto>>();


            return _ProductListExcel.ExportToFile(productListDtos);
        }
        public async Task<GetProduct> GetProductForEdit(NullableIdInput<long> input)
        {
            var Product = await _ProductRepository.GetAll().Where(p => p.Id == input.Id).FirstOrDefaultAsync();
            var ProductClassification = await _ProductClassificationRepository.GetAll().Select(p=>new ProductClassificationDto{ProductClassificationId=p.Id,ProductClassificationName=p.ProductClassificationName}).ToArrayAsync();
            var output = new GetProduct 
            {
                ProductClassificationName=ProductClassification
            };
            output.Product = Product.MapTo<CreateProductInput>();
            return output;
        }
        public async Task CreateOrEditProduct(CreateProductInput input)
        {
            if(input.Id==0)
            {
                await CreateProduct(input);

            }
            else
            {
                await UpdateProduct(input);

            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Product_Products_CreateProduct)]
        public async Task CreateProduct(CreateProductInput input)
        {
            var product = input.MapTo<Product>();
            await _ProductRepository.InsertAsync(product);
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Product_Products_EditProduct)]
        public async Task UpdateProduct(CreateProductInput input)
        {
            var product = input.MapTo<Product>();
            await _ProductRepository.UpdateAsync(product);
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Product_Products_DeleteProduct)]
        public async Task DeleteProduct(IdInput input)
        {
            await _ProductRepository.DeleteAsync(input.Id);
        }
    }
}
