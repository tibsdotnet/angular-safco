﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.Products.Dto;

namespace TIBS.SAFCO.Products.Export
{
    public interface IProductListExcel
    {
        FileDto ExportToFile(List<ProductListDto> productListDtos);
    }
}
