﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.DataExporting.Excel.EpPlus;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.Products.Dto;

namespace TIBS.SAFCO.Products.Export
{
    public class ProductListExcel : EpPlusExcelExporterBase, IProductListExcel
    {
        public FileDto ExportToFile(List<ProductListDto> productListDtos)
        {
            return CreateExcelPackage(
                "Product.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Product"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("ProductName"),
                        L("Description"),
                        L("MinimumSalePrice"),
                        L("MaximumSalePrice"),
                        L("ProductClassificationName")
                    );

                    AddObjects(
                        sheet, 2, productListDtos,
                        _ => _.ProductName,
                        _ => _.Description,
                        _ => _.MinimumSalePrice,
                        _ => _.MaximumSalePrice,
                        _ => _.ProductClassificationName
                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(5);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 5; i++)
                    {

                        sheet.Column(i).AutoFit();
                    }
                });
        }

    }
}
