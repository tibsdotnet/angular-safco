﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Announcements.Dto;
using TIBS.SAFCO.Dto;

namespace TIBS.SAFCO.Announcementss.Export
{
    public interface IAnnouncementListExcel
    {
        FileDto ExportToFile(List<Announcements_View_Dto> announcementsListDtos);
    }
}
