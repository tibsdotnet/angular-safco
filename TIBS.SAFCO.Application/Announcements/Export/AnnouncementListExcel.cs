﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Announcements.Dto;
using TIBS.SAFCO.DataExporting.Excel.EpPlus;
using TIBS.SAFCO.Dto;

namespace TIBS.SAFCO.Announcementss.Export
{
    public class AnnouncementListExcel : EpPlusExcelExporterBase, IAnnouncementListExcel
    {
        public FileDto ExportToFile(List<Announcements_View_Dto> announcementsListDtos)
        {
            return CreateExcelPackage(
                "Announcements.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Announcements"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Title"),
                        L("Description"),
                        L("PublishedDate"),
                        L("ExpiredDate")
                    );

                    AddObjects(
                        sheet, 2, announcementsListDtos,
                        _ => _.Title,
                        _ => _.Description,
                        _ => _.PublishedDate,
                        _ => _.ExpiredDate
                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(4);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 4; i++)
                    {

                        sheet.Column(i).AutoFit();
                    }
                });
        }

    }
}
