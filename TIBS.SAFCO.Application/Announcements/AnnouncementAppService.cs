﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Announcements.Dto;
using Abp.Linq.Extensions;
using Abp.Extensions;
using Abp.Linq;
using System.Linq.Dynamic;
using System.Data.Entity;
using Abp.AutoMapper;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.Announcementss.Export;
using Abp.Authorization;
using TIBS.SAFCO.Authorization;
using Abp.UI;
namespace TIBS.SAFCO.Announcements
{
    public class AnnouncementAppService:SAFCOAppServiceBase,IAnnouncementAppService
    {
        private readonly IRepository<Announcement> _AnnouncementsRepository;
        private readonly IAnnouncementListExcel _AnnouncementListExcel;
        public AnnouncementAppService(IAnnouncementListExcel AnnouncementListExcel, IRepository<Announcement> AccouncementRepository)
        {
            _AnnouncementsRepository = AccouncementRepository;
            _AnnouncementListExcel = AnnouncementListExcel;
        }
        public async Task<PagedResultOutput<Announcements_View_Dto>> GetAccouncement(GetAnnouncementInput input)
        {
            var query = _AnnouncementsRepository.GetAll()
                      .WhereIf(
                      !input.Filter.IsNullOrWhiteSpace(),
                      p => p.Title.Contains(input.Filter) ||
                          p.Description.Contains(input.Filter) ||
                          p.PublishedDate.ToString().Contains(input.Filter) ||
                          p.ExpiredDate.ToString().Contains(input.Filter)
                     );
            var ancount = await query.CountAsync();
            var accouncements = await query
               .OrderBy(input.Sorting)
               .PageBy(input)
               .ToListAsync();
            var announcementdto = accouncements.MapTo<List<Announcements_View_Dto>>();
            foreach(var ann in announcementdto)
            {
                if(ann.ExpiredDate!=null)
                {
                    ann.ExpiredDate = DateTime.Parse(ann.ExpiredDate).ToString("dd-MMM-yyyy");
                }
                if(ann.PublishedDate!=null)
                {
                    ann.PublishedDate = DateTime.Parse(ann.PublishedDate).ToString("dd-MMM-yyyy");
                }
            }
            return new PagedResultOutput<Announcements_View_Dto>(ancount,announcementdto);
        }
        public async Task<FileDto> GetAnnouncementToExcel()
        {

            var announcement = _AnnouncementsRepository
                .GetAll().ToList();
            var announcementsListDtos = announcement.MapTo<List<Announcements_View_Dto>>();
            foreach (var ann in announcementsListDtos)
            {
                if (ann.ExpiredDate != null)
                {
                    ann.ExpiredDate = DateTime.Parse(ann.ExpiredDate).ToString("dd-MMM-yyyy");
                }
                if (ann.PublishedDate != null)
                {
                    ann.PublishedDate = DateTime.Parse(ann.PublishedDate).ToString("dd-MMM-yyyy");
                }
            }

            return _AnnouncementListExcel.ExportToFile(announcementsListDtos);
        }
        public async Task<GetAnnouncement> GetAnnouncementForEdit(NullableIdInput<long> input)
        {
            var output = new GetAnnouncement { };
            var acconcementpoutput = await _AnnouncementsRepository.GetAll().Where(p => p.Id == input.Id).FirstOrDefaultAsync();
            output.Announcement = acconcementpoutput.MapTo<CreateAnnouncementInput>();
            return output;
        }
        public async Task CreateOrUpdateAnnouncement(CreateAnnouncementInput input)
        {

            if(input.Id==0)
            {
                await CreateAnnouncement(input);

            }
            else
            {
                await UpdateAccouncement(input);

            }

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Announcement_CreateAnnouncement)]
        public async Task CreateAnnouncement(CreateAnnouncementInput input)
        {
            var accouncement = input.MapTo<Announcement>();
            if (input.ExpiredDate > input.PublishedDate)
            {
                await _AnnouncementsRepository.InsertAsync(accouncement);
            }
            else
            {
                throw new UserFriendlyException("Ooops!", "Expired Date Must Be Greate Than Published Date");
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Announcement_EditAnnouncement)]
        public async Task UpdateAccouncement(CreateAnnouncementInput input)
        {
            var accouncement = input.MapTo<Announcement>();
            if (input.ExpiredDate > input.PublishedDate)
            {
                await _AnnouncementsRepository.UpdateAsync(accouncement);
            }
            else
            {
                throw new UserFriendlyException("Ooops!", "Expired Date Must Be Greate Than Published Date");
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Announcement_DeleteAnnouncement)]
        public async Task DeleteAnnouncment(IdInput input)
        {
            await _AnnouncementsRepository.DeleteAsync(input.Id);
        }
    }
}
