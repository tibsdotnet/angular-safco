﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Announcements.Dto;
using TIBS.SAFCO.Dto;

namespace TIBS.SAFCO.Announcements
{
   public interface IAnnouncementAppService:IApplicationService
    {
       Task<PagedResultOutput<Announcements_View_Dto>> GetAccouncement(GetAnnouncementInput input);
       Task<GetAnnouncement> GetAnnouncementForEdit(NullableIdInput<long> input);
       Task CreateOrUpdateAnnouncement(CreateAnnouncementInput input);
       Task DeleteAnnouncment(IdInput input);
       Task<FileDto> GetAnnouncementToExcel();
    }
}
