﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TIBS.SAFCO.Announcements.Dto
{
    [AutoMapTo(typeof(Announcement))]
    public class CreateAnnouncementInput:IInputDto
    {
        [Required]
        public  string Title { get; set; }
        public  string Description { get; set; }
        public  DateTime PublishedDate { get; set; }
        public  DateTime ExpiredDate { get; set; }
        public int Id { get; set; }
    }
}
