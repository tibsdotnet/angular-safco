﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.SAFCO.Announcements.Dto
{
    [AutoMapFrom(typeof(Announcement))]
    public class Announcements_View_Dto:FullAuditedEntityDto
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string PublishedDate { get; set; }
        public string ExpiredDate { get; set; }
    }
}
