﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.SAFCO.Announcements.Dto
{
    public class GetAnnouncement:IOutputDto
    {
        public CreateAnnouncementInput Announcement { get; set; }
    }
}
