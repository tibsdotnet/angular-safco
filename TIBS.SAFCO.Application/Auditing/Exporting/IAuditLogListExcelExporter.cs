﻿using System.Collections.Generic;
using TIBS.SAFCO.Auditing.Dto;
using TIBS.SAFCO.Dto;

namespace TIBS.SAFCO.Auditing.Exporting
{
    public interface IAuditLogListExcelExporter
    {
        FileDto ExportToFile(List<AuditLogListDto> auditLogListDtos);
    }
}
