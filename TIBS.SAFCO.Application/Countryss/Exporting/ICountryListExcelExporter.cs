﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Countryss.Dto;
using TIBS.SAFCO.Dto;

namespace TIBS.SAFCO.Countryss.Exporting
{
    public interface ICountryListExcelExporter
    {
        FileDto ExportToFile(List<CountryListDto> countryListDtos);
    }
}
