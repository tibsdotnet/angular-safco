﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Countryss.Dto;
using TIBS.SAFCO.DataExporting.Excel.EpPlus;
using TIBS.SAFCO.Dto;

namespace TIBS.SAFCO.Countryss.Exporting
{
    public class CountryListExcelExporter : EpPlusExcelExporterBase, ICountryListExcelExporter
    {
        public FileDto ExportToFile(List<CountryListDto> countryListDtos)
        {
            return CreateExcelPackage(
                "Country.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Country"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("CountryName"),
                        L("PrintCountryName"),
                        L("ISDCode"),
                        L("ISOCode"),
                        L("ISO3Code"),
                        L("NumericCode"),
                        L("Divisions")
                    );

                    AddObjects(
                        sheet, 2, countryListDtos,
                        _ => _.CountryName,
                        _ => _.PrintCountryName,
                        _ => _.ISDCode,
                        _ => _.ISOCode,
                        _ => _.ISO3Code,
                        _ => _.NumericCode,
                        _ => _.Divisions

                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(7);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 7; i++)
                    {

                        sheet.Column(i).AutoFit();
                    }
                });
        }

    }
}

