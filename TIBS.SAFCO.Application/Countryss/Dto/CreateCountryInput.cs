﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIBS.SAFCO.Countryss.Dto
{
    [AutoMapTo(typeof(Country))]
    public class CreateCountryInput : IInputDto
    {
        public string CountryName { get; set; }
        public string PrintCountryName { get; set; }
        public string ISOCode { get; set; }
        public string ISO3Code { get; set; }
        public string ISDCode { get; set; }
        public string NumericCode { get; set; }
        public string Divisions { get; set; }
        public long Id { get; set; }
    }
}
