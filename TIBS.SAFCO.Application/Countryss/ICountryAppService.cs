﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Countryss.Dto;
using TIBS.SAFCO.Dto;

namespace TIBS.SAFCO.Countryss
{
    public interface ICountryAppService : IApplicationService
    {
        Task<PagedResultOutput<CountryListDto>> GetCountry(GetCountryInput input);
        Task CreateOrUpdateUser(CreateCountryInput input);
        Task<GetCountry> GetCountryForEdit(NullableIdInput<long> input);
        Task DeleteCountry(IdInput input);
        Task<FileDto> GetCountryToExcel();
    }
}
