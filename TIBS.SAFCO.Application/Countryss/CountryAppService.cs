﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using TIBS.SAFCO.Countryss.Exporting;
using TIBS.SAFCO.Countryss.Dto;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.Authorization;
using System.Data.Entity;
using System.Linq.Dynamic;

namespace TIBS.SAFCO.Countryss
{
    public class CountryAppService : SAFCOAppServiceBase, ICountryAppService
    {
        private readonly IRepository<Country> _CountryRepository;
        private readonly ICountryListExcelExporter _countryListExcelExporter;

        public CountryAppService(ICountryListExcelExporter countryListExcelExporter, IRepository<Country> CountryRepository)
        {
            _CountryRepository = CountryRepository;
            _countryListExcelExporter = countryListExcelExporter;
        }

        public async Task<PagedResultOutput<CountryListDto>> GetCountry(GetCountryInput input)
        {
            var query = _CountryRepository
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.CountryName.Contains(input.Filter) ||
                         p.ISDCode.Contains(input.Filter) ||
                         p.ISOCode.Contains(input.Filter) ||
                         p.ISO3Code.Contains(input.Filter) ||
                         p.NumericCode.Contains(input.Filter) ||
                         p.PrintCountryName.Contains(input.Filter) ||
                         p.Divisions.Contains(input.Filter)
                );
                var country = await query.CountAsync();
                var countrys = await query
               .OrderBy(input.Sorting)
               .PageBy(input)
               .ToListAsync();
                var countrydto = countrys.MapTo<List<CountryListDto>>();

            //var

                return new PagedResultOutput<CountryListDto>(country, countrydto);
        }

        public async Task<FileDto> GetCountryToExcel()
        {

            var country = _CountryRepository
                .GetAll();
            var countryListDtos = country.MapTo<List<CountryListDto>>();


            return _countryListExcelExporter.ExportToFile(countryListDtos);
        }


        public async Task<GetCountry> GetCountryForEdit(NullableIdInput<long> input)
        {
            var output = new GetCountry
            {
            };

            var country = _CountryRepository
                .GetAll().Where(p => p.Id == input.Id).FirstOrDefault();

            output.country = country.MapTo<CreateCountryInput>();

            return output;

        }

        public async Task CreateOrUpdateUser(CreateCountryInput input)
        {
            if (input.Id != 0)
            {
                await UpdateCountryAsync(input);
            }
            else
            {
                await CreateCountryAsync(input);
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_AddressBook_Country_CreateNewCountry)]
        public virtual async Task CreateCountryAsync(CreateCountryInput input)
        {
            var Country = input.MapTo<Country>();
            DateTime myUtcDateTime = DateTime.Now;

            Country.LastModificationTime = DateTime.Now.AddHours(-12);
            var val = _CountryRepository
              .GetAll().Where(p => p.ISDCode == input.ISDCode || p.CountryName == input.CountryName).FirstOrDefault();
            if (val == null)
            {
                await _CountryRepository.InsertAsync(Country);
            }
            else
            {
                throw new UserFriendlyException("Ooops!", "Duplicate Data Occured in Country Name '" + input.CountryName + "' or ISD Code '" + input.ISDCode + "'...");
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_AddressBook_Country_EditCountry)]
        public virtual async Task UpdateCountryAsync(CreateCountryInput input)
        {
            var Country = input.MapTo<Country>();
            Country.CountryName = input.CountryName;
            Country.ISOCode = input.ISOCode;
            Country.ISDCode = input.ISDCode;
            Country.ISO3Code = input.ISO3Code;
            Country.NumericCode = input.NumericCode;
            Country.PrintCountryName = input.PrintCountryName;
            Country.Divisions = input.Divisions;
            var val = _CountryRepository
              .GetAll().Where(p => (p.ISDCode == input.ISDCode || p.CountryName == input.CountryName) && p.Id != input.Id).FirstOrDefault();
            if (val == null)
            {
                await _CountryRepository.UpdateAsync(Country);
            }
            else
            {
                throw new UserFriendlyException("Ooops!", "Duplicate Data Occured in Country Name '" + input.CountryName + "' or ISD Code '" + input.ISDCode + "'...");
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_AddressBook_Country_DeleteCountry)]
        public async Task DeleteCountry(IdInput input)
        {

            await _CountryRepository.DeleteAsync(input.Id);



        }
    }
}
