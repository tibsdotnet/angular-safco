﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using TIBS.SAFCO.CompanyContacts;
using System.ComponentModel.DataAnnotations;

namespace TIBS.SAFCO.Companies.Dto
{
    [AutoMapTo(typeof(CompanyContact))]
   public class CreateContactInput:IInputDto
    {
        public int Id { get; set; }
       
        public virtual int CompanyId { get; set; }
       
        public virtual int? RegionId { get; set; }
        public virtual string TitleOfCourtey { get; set; }
        [Required]
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        [Required]
        public virtual string EmailId { get; set; }
        public virtual string Address { get; set; }
        public virtual string Description { get; set; }
        public virtual string Notes { get; set; }
        public virtual string Fax { get; set; }
        public virtual string POBox { get; set; }
    }
}
