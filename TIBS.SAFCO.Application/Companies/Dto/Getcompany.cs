﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Dto;
using TIBS.SAFCO.Leads;
using TIBS.SAFCO.Leads.Dto;

namespace TIBS.SAFCO.Companies.Dto
{
   public class Getcompany:IOutputDto
    {
       public CreateCompanyInput Company { get; set; }
       public RegionInputDto[] Region { get; set; }

       public LeadSourceInputDto[] LeadSource { get; set; }
       public StatusInputDto[] StatusName { get; set; }
       public UserListInputDtos[] UserName { get; set; }

       public int StatusId { get; set; }

       public LeadViewDto Lead { get; set; }

       public CompetitorInputDto[] Competitor { get; set; }
    }
}
