﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using TIBS.SAFCO.Compnaies;

namespace TIBS.SAFCO.Companies.Dto
{
    [AutoMapFrom(typeof(Company))]
   public class Company_View_Dto:FullAuditedEntityDto
    {
        public  string Title { get; set; }
        public  string Description { get; set; }
        public  string Address { get; set; }
        
        public  string Region { get; set; }
        
        public  string LeadSource { get; set; }
       
        public  string Status { get; set; }
        public  string POBox { get; set; }
        public  string PhoneNo { get; set; }
        public  string WebSite { get; set; }
        public  string Fax { get; set; }
        public string ManagedBy { get; set; }
        public  string ContributedName { get; set; }
        public  string PublishedTime { get; set; }
        public bool EditDeltePermission { get; set; }
    }
}
