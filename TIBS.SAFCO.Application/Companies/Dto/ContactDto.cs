﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using TIBS.SAFCO.CompanyContacts;

namespace TIBS.SAFCO.Companies.Dto
{
    [AutoMapFrom(typeof(CompanyContact))]
   public  class ContactDto:FullAuditedEntityDto
    {
        public int Id { get; set; }
        public string CompanyName { get; set; }
        public string Regionname { get; set; }
        public  string TitleOfCourtey { get; set; }
       
        public  string FirstName { get; set; }
        public  string LastName { get; set; }
        
        public  string EmailId { get; set; }
        public  string Address { get; set; }
        public  string Description { get; set; }
        public  string Notes { get; set; }
        public  string Fax { get; set; }
        public  string POBox { get; set; }
        public string ProfilePicture { get; set; }
    }
}
