﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Dto;

namespace TIBS.SAFCO.Companies.Dto
{
    public class GetContacts:IOutputDto
    {

        public CreateContactInput CompanyContact { get; set; }
        public RegionInputDto[] Region { get; set; }
    }
}
