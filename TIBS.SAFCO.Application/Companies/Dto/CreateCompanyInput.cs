﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using TIBS.SAFCO.Compnaies;
using System.ComponentModel.DataAnnotations;

namespace TIBS.SAFCO.Companies.Dto
{
    [AutoMapTo(typeof(Company))]
   public class CreateCompanyInput:IInputDto
    {
        public int Id { get; set; }
        [Required]
        public  string Title { get; set; }
        public  string Description { get; set; }
        public  string Address { get; set; }
       
        public  int RegionId { get; set; }
       
        public  int? LeadSourceId { get; set; }
        
        public  int? StatusId { get; set; }
        public  string POBox { get; set; }
        public  string PhoneNo { get; set; }
        public  string WebSite { get; set; }
        public  string Fax { get; set; }
        
        public  long? ContributedId { get; set; }
        public  DateTime PublishedTime { get; set; }
        public int? Managedid { get; set; }
    }
}
