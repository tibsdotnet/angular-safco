﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Companies.Dto;
using TIBS.SAFCO.Compnaies;
using TIBS.SAFCO.LeadSources;
using TIBS.SAFCO.LeadStatus;
using TIBS.SAFCO.Regions;
using Abp.Linq.Extensions;
using Abp.Extensions;
using System.Data.Entity;
using Abp.AutoMapper;
using TIBS.SAFCO.Dto;
using Abp.UI;
using TIBS.SAFCO;
using Abp.Authorization;
using TIBS.SAFCO.Authorization;
using TIBS.SAFCO.CompanyContacts;
using TIBS.SAFCO.Companies.Export;
using TIBS.SAFCO.Authorization.Roles;
using Abp.Runtime.Session;
using System.Linq.Dynamic;

namespace TIBS.SAFCO.Companies
{
   
    public class CompanyAppService : SAFCOAppServiceBase, ICompanyAppService
    {
        private readonly IRepository<Region> _RegionRepository;
        private readonly IRepository<LeadSource> _LeadSourceRepository;
        private readonly IRepository<Status> _StatusRepositoy;
        private readonly IRepository<Company> _CompanyRepository;
        private readonly IRepository<CompanyContact> _CompanyContactRepository;
        private readonly ICompanyListExcel _CompanyListExcel;
        private readonly RoleManager _roleManager;

        //private readonly AbpUser<User> _UserRepository;
        public CompanyAppService(ICompanyListExcel CompanyListExcel, IRepository<Region> regionRepository, IRepository<LeadSource> leadRepositoy, IRepository<Status> statusRepository, IRepository<Company> companyRepository, IRepository<CompanyContact> CompanyContactRepository, RoleManager roleManager)
        {
            _RegionRepository = regionRepository;
            _LeadSourceRepository = leadRepositoy;
            _StatusRepositoy = statusRepository;
            _CompanyRepository = companyRepository;
            _CompanyContactRepository = CompanyContactRepository;
            _CompanyListExcel = CompanyListExcel;
            _roleManager = roleManager;
        }
        public async Task<PagedResultOutput<Company_View_Dto>> GetCompany(GetCompanyInput input)
        {
            long userId = (long)AbpSession.GetUserId();
            var companyquery = _CompanyRepository.GetAll()
                             .WhereIf(
                             !input.Filter.IsNullOrWhiteSpace(),
                             p =>
                                 p.Title.Contains(input.Filter) ||
                                 p.Description.Contains(input.Filter) ||
                                 p.Fax.Contains(input.Filter) ||
                                 p.AbpUser.UserName.Contains(input.Filter) ||
                                 p.LeadSources.Tittle.Contains(input.Filter) ||
                                 p.status.StatusName.Contains(input.Filter) ||
                                 p.Regions.Name.Contains(input.Filter) ||
                                 p.PhoneNo.Contains(input.Filter) ||
                                 p.POBox.Contains(input.Filter) ||
                                 p.ManagedUser.FirstName.Contains(input.Filter)||
                                 p.PublishedTime.ToString().Contains(input.Filter) ||
                                 p.WebSite.Contains(input.Filter)

                             );
            long userid = (int)AbpSession.UserId;
            var companydtos = (from c in companyquery select new Company_View_Dto { Id = c.Id, Title = c.Title, Description = c.Description != null ? c.Description : "", Region = c.RegionId != null ? c.Regions.Name : "", LeadSource = c.LeadSourceId != null ? c.LeadSources.Tittle : "", Status = c.StatusId != null ? c.status.StatusName : "", PhoneNo = c.PhoneNo != null ? c.PhoneNo : "", WebSite = c.WebSite != null ? c.WebSite : "", Fax = c.Fax != null ? c.Fax : "", Address = c.Address != null ? c.Address : "", ContributedName = c.ContributedId != null ? c.AbpUser.UserName : "", ManagedBy = c.ManagedId != null ? c.ManagedUser.FirstName : "", EditDeltePermission = c.ManagedId == userId ? true : c.ContributedId == userId ? true : false });
            var companycount = await companydtos.CountAsync();
            var companydto = await companydtos
                                 .OrderBy(input.Sorting)
                                 .PageBy(input)
                                 .ToListAsync();
            var com = companydto.MapTo<List<Company_View_Dto>>();
            return new PagedResultOutput<Company_View_Dto>(companycount, com);
        }
        public async Task<FileDto> GetCompanyToExcel()
        {

            var company = _CompanyRepository
                .GetAll().ToList();
            var companydto = (from c in company select new Company_View_Dto { Id = c.Id, Title = c.Title, Description = c.Description != null ? c.Description : "", Region = c.RegionId != null ? c.Regions.Name : "", LeadSource = c.LeadSourceId != null ? c.LeadSources.Tittle : "", Status = c.StatusId != null ? c.status.StatusName : "", PhoneNo = c.PhoneNo != null ? c.PhoneNo : "", WebSite = c.WebSite != null ? c.WebSite : "", Fax = c.Fax != null ? c.Fax : "", Address = c.Address != null ? c.Address : "", ContributedName = c.ContributedId != null ? c.AbpUser.UserName : "", ManagedBy = c.ManagedId != null ? c.ManagedUser.FirstName : "" });
            var companyListDtos = companydto.MapTo<List<Company_View_Dto>>();


            return _CompanyListExcel.ExportToFile(companyListDtos);
        }
        public async Task<Getcompany> GetCompantForEdit(NullableIdInput<long> input)
        {
            long userid = (int)AbpSession.UserId;
            var leadSource = await _LeadSourceRepository.GetAll().Select(p => new LeadSourceInputDto { LeadSourceId = p.Id, LeadSourceName = p.Tittle }).ToArrayAsync();
            var Satus = await _StatusRepositoy.GetAll().Select(r => new StatusInputDto { StatusId = r.Id, StatusName = r.StatusName }).OrderBy(r=>r.StatusId).ToArrayAsync();
            var Region = await _RegionRepository.GetAll().Select(r => new RegionInputDto { RegionName = r.Name, RegionId = r.Id }).ToArrayAsync();

            var user = await UserManager.GetUserByIdAsync(userid);
            var roleid = user.Roles.ToList();

            string[] userroles = new string[roleid.Count];
            int i = 0;
            foreach (var role in roleid)
            {
                int rol = role.RoleId;
                var userrolesquery = _roleManager.Roles.Where(p => p.Id == rol).OrderBy(p => p.DisplayName).FirstOrDefault();
                userroles[i] = userrolesquery.DisplayName;
                i++;
            }
            var uroles = userroles.ToList();

            var userDtos = (await UserManager.Users
                             .OrderBy(r => r.UserName)
                             .Select(r => new UserListInputDtos
                             {
                                 UserId = r.Id,
                                 UserName = r.UserName
                             }).ToArrayAsync());

            if (uroles.Contains("Admin") || uroles.Contains("Management"))
            {
               
            }
            else
            {
                userDtos = (await UserManager.Users.Where(p => p.Id == userid)
                             .OrderBy(r => r.UserName)
                             .Select(r => new UserListInputDtos
                             {
                                 UserId = r.Id,
                                 UserName = r.UserName
                             }).ToArrayAsync());
            }
            
            var company = await _CompanyRepository.GetAll().Where(p => p.Id == input.Id).FirstOrDefaultAsync();
            var output = new Getcompany
                             {
                                 Region = Region,
                                 LeadSource = leadSource,
                                 StatusName = Satus,
                                 UserName=userDtos
                                 

                             };
            output.Company = company.MapTo<CreateCompanyInput>();
            return output;
        }
        public async Task CreateOrUpdateCompany(CreateCompanyInput input)
        {
            if (input.Id == 0)
            {
                await CreateCompany(input);
            }
            else
            {
                await UpadteCompany(input);
            }

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_AddressBook_Company_CreateNewCompany)]
        public async Task CreateCompany(CreateCompanyInput input)
        {
            var comp = _CompanyRepository.GetAll().Where(p => p.Title == input.Title);
            var compinut = input.MapTo<Company>();
           if(comp.Count()<=0)
           {
               try
               {
                   compinut.PublishedTime = DateTime.Now;
                   await _CompanyRepository.InsertAsync(compinut);

               }catch(Exception ex)
               {

               }
               
           }else
           {
               throw new UserFriendlyException("OOPS..", "Duplicate Data Occured In Company Name" + input.Title);
           }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_AddressBook_Company_EditCompany)]
        public async Task UpadteCompany(CreateCompanyInput input)
        {
            var comp = _CompanyRepository.GetAll().Where(p => p.Title == input.Title&&p.Id!=input.Id);
            if (comp.Count() <= 0)
            {
                await _CompanyRepository.UpdateAsync(input.MapTo<Company>());
            }
            else
            {
                throw new UserFriendlyException("OOPS..", "Duplicate Data Occured In Company Name" + input.Title);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_AddressBook_Company_DeleteCompany)]
        public async Task DeleteCompany(IdInput input)
        {
            await _CompanyRepository.DeleteAsync(input.Id);
        }

        public async Task<PagedResultOutput<ContactDto>> GetContacts(NullableIdInput<long> input)
        {
            var contactquery = await _CompanyContactRepository.GetAll().Where(p => p.CompanyId == input.Id).ToListAsync();
            var contactquerydto = from c in contactquery select new ContactDto { Id = c.Id, ProfilePicture = c.ProfilePictuePath != null ? c.ProfilePictuePath : "", CompanyName = c.Companies.Title, Regionname = c.RegionId != null ? c.Regions.Name : "", Address = c.Address, Description = c.Description, Fax = c.Fax, POBox = c.POBox, EmailId = c.EmailId, FirstName = c.FirstName, LastName = c.LastName, Notes = c.Notes };
            var countcontact = contactquerydto.Count();
            var contactdtos = contactquerydto.MapTo<List<ContactDto>>();
            return new PagedResultOutput<ContactDto>(countcontact, contactdtos);
        }

        public async Task<GetContacts> GetContactForEdit(NullableIdInput<long> input)
        {
            var Region = await _RegionRepository.GetAll().Select(r => new RegionInputDto { RegionName = r.Name, RegionId = r.Id }).ToArrayAsync();
            var output = new GetContacts { Region=Region};
            var Contact = await _CompanyContactRepository.GetAll().Where(p => p.Id == input.Id).FirstOrDefaultAsync();
            output.CompanyContact = Contact.MapTo<CreateContactInput>();
            return output;
        }
        public async Task CreateOrUpdate(CreateContactInput input)
        {
            if(input.Id==0)
            {
                await CreateContact(input);
            }
            else
            {
                await UpdateContact(input);
            }

        }
        public async Task CreateContact(CreateContactInput input)
        {
            var contact = input.MapTo<CompanyContact>();
            await _CompanyContactRepository.InsertAsync(contact);
        }
        public async Task UpdateContact(CreateContactInput input)
        {
            var contact = input.MapTo<CompanyContact>();
            await _CompanyContactRepository.UpdateAsync(contact);
        }
        public async Task DeleteContct(IdInput input)
        {
            await _CompanyContactRepository.DeleteAsync(input.Id);
        }
    }
}
