﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Companies.Dto;
using TIBS.SAFCO.Dto;

namespace TIBS.SAFCO.Companies
{
    public interface ICompanyAppService:IApplicationService
    {
        Task<PagedResultOutput<Company_View_Dto>> GetCompany(GetCompanyInput input);
        Task<Getcompany> GetCompantForEdit(NullableIdInput<long> input);
        Task CreateOrUpdateCompany(CreateCompanyInput input);
        Task DeleteCompany(IdInput input);
        Task<PagedResultOutput<ContactDto>> GetContacts(NullableIdInput<long> input);
        Task<GetContacts> GetContactForEdit(NullableIdInput<long> input);
        Task CreateOrUpdate(CreateContactInput input);
        Task DeleteContct(IdInput input);
        Task<FileDto> GetCompanyToExcel();
    }
}
