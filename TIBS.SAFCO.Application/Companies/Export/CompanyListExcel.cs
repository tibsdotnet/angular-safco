﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Companies.Dto;
using TIBS.SAFCO.DataExporting.Excel.EpPlus;
using TIBS.SAFCO.Dto;

namespace TIBS.SAFCO.Companies.Export
{
    public class CompanyListExcel : EpPlusExcelExporterBase, ICompanyListExcel
    {
        public FileDto ExportToFile(List<Company_View_Dto> companyListDtos)
        {
            return CreateExcelPackage(
                "Company.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Company"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("CompanyName"),
                        L("Region"),
                        L("Status"),
                        L("PhoneNo"),
                        L("Contributedby"),
                        L("ManagedBy")
                    );

                    AddObjects(
                        sheet, 2, companyListDtos,
                        _ => _.Title,
                        _ => _.Region,
                        _ => _.Status,
                        _ => _.PhoneNo,
                        _ => _.ContributedName,
                        _ => _.ManagedBy

                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(6);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 6; i++)
                    {

                        sheet.Column(i).AutoFit();
                    }
                });
        }

    }
}

