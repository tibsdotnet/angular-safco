﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.Companies.Dto;
using TIBS.SAFCO.Dto;

namespace TIBS.SAFCO.Companies.Export
{
    public interface ICompanyListExcel
    {
        FileDto ExportToFile(List<Company_View_Dto> companyListDtos);
    }
}
