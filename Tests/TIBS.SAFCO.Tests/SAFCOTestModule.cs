﻿using Abp.Modules;
using Abp.Zero.Configuration;

namespace TIBS.SAFCO.Tests
{
    [DependsOn(
        typeof(SAFCOApplicationModule),
        typeof(SAFCODataModule))]
    public class SAFCOTestModule : AbpModule
    {
        public override void PreInitialize()
        {
            //Use database as language management
            Configuration.Modules.Zero().LanguageManagement.EnableDbLocalization();
        }
    }
}
