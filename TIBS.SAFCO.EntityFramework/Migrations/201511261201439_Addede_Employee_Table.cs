namespace TIBS.SAFCO.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Addede_Employee_Table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LastName = c.String(),
                        FirstName = c.String(),
                        RoleId = c.Int(),
                        TitleOfCourtesy = c.String(),
                        BirthDate = c.DateTime(),
                        HireDate = c.DateTime(),
                        Address = c.String(),
                        RegionId = c.Int(),
                        POBoxNo = c.String(),
                        Phone = c.String(),
                        Extension = c.String(),
                        Notes = c.String(),
                        PhotoPath = c.String(),
                        Published = c.DateTime(),
                        Archived = c.Boolean(nullable: false),
                        Email = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Employee_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Regions", t => t.RegionId)
                .ForeignKey("dbo.AbpRoles", t => t.RoleId)
                .Index(t => t.RoleId)
                .Index(t => t.RegionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Employees", "RoleId", "dbo.AbpRoles");
            DropForeignKey("dbo.Employees", "RegionId", "dbo.Regions");
            DropIndex("dbo.Employees", new[] { "RegionId" });
            DropIndex("dbo.Employees", new[] { "RoleId" });
            DropTable("dbo.Employees",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Employee_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
