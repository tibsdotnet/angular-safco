namespace TIBS.SAFCO.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_LeadRequest_Table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LeadRequest",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Company_Id = c.Int(nullable: false),
                        RequestedBy_Employees_ID = c.Int(nullable: false),
                        ApprovalStatus_IDD = c.Int(nullable: false),
                        ApprovalBy_Employees_ID = c.Int(),
                        Archived = c.Boolean(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_LeadRequest_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApprovalStatus", t => t.ApprovalStatus_IDD, cascadeDelete: true)
                .ForeignKey("dbo.Companies", t => t.Company_Id, cascadeDelete: true)
                .ForeignKey("dbo.Employees", t => t.RequestedBy_Employees_ID, cascadeDelete: true)
                .ForeignKey("dbo.Employees", t => t.ApprovalBy_Employees_ID)
                .Index(t => t.Company_Id)
                .Index(t => t.RequestedBy_Employees_ID)
                .Index(t => t.ApprovalStatus_IDD)
                .Index(t => t.ApprovalBy_Employees_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LeadRequest", "ApprovalBy_Employees_ID", "dbo.Employees");
            DropForeignKey("dbo.LeadRequest", "RequestedBy_Employees_ID", "dbo.Employees");
            DropForeignKey("dbo.LeadRequest", "Company_Id", "dbo.Companies");
            DropForeignKey("dbo.LeadRequest", "ApprovalStatus_IDD", "dbo.ApprovalStatus");
            DropIndex("dbo.LeadRequest", new[] { "ApprovalBy_Employees_ID" });
            DropIndex("dbo.LeadRequest", new[] { "ApprovalStatus_IDD" });
            DropIndex("dbo.LeadRequest", new[] { "RequestedBy_Employees_ID" });
            DropIndex("dbo.LeadRequest", new[] { "Company_Id" });
            DropTable("dbo.LeadRequest",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_LeadRequest_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
