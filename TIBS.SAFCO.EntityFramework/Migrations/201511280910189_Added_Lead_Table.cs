namespace TIBS.SAFCO.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_Lead_Table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Lead",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        CompanyId = c.Int(nullable: false),
                        LeadSourceId = c.Int(),
                        MileStoneId = c.Int(nullable: false),
                        Rating = c.Int(nullable: false),
                        ExpectedAmount = c.Single(nullable: false),
                        ExpectedClosure = c.DateTime(),
                        ActualAmount = c.Single(nullable: false),
                        ActualClosure = c.DateTime(),
                        OrderNo = c.String(maxLength: 50),
                        Archived = c.Boolean(nullable: false),
                        Published = c.DateTime(),
                        EmployeeId = c.Int(nullable: false),
                        ChefId = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Lead_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: true)
                .ForeignKey("dbo.LeadSource", t => t.LeadSourceId)
                .ForeignKey("dbo.Employees", t => t.ChefId)
                .ForeignKey("dbo.Employees", t => t.EmployeeId, cascadeDelete: true)
                .ForeignKey("dbo.MileStone", t => t.MileStoneId, cascadeDelete: true)
                .Index(t => t.CompanyId)
                .Index(t => t.LeadSourceId)
                .Index(t => t.MileStoneId)
                .Index(t => t.EmployeeId)
                .Index(t => t.ChefId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Lead", "MileStoneId", "dbo.MileStone");
            DropForeignKey("dbo.Lead", "EmployeeId", "dbo.Employees");
            DropForeignKey("dbo.Lead", "ChefId", "dbo.Employees");
            DropForeignKey("dbo.Lead", "LeadSourceId", "dbo.LeadSource");
            DropForeignKey("dbo.Lead", "CompanyId", "dbo.Companies");
            DropIndex("dbo.Lead", new[] { "ChefId" });
            DropIndex("dbo.Lead", new[] { "EmployeeId" });
            DropIndex("dbo.Lead", new[] { "MileStoneId" });
            DropIndex("dbo.Lead", new[] { "LeadSourceId" });
            DropIndex("dbo.Lead", new[] { "CompanyId" });
            DropTable("dbo.Lead",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Lead_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
