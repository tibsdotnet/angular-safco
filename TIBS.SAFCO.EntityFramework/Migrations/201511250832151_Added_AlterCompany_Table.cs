namespace TIBS.SAFCO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_AlterCompany_Table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "ManagedById", c => c.Long());
            CreateIndex("dbo.Companies", "ManagedById");
            AddForeignKey("dbo.Companies", "ManagedById", "dbo.AbpUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Companies", "ManagedById", "dbo.AbpUsers");
            DropIndex("dbo.Companies", new[] { "ManagedById" });
            DropColumn("dbo.Companies", "ManagedById");
        }
    }
}
