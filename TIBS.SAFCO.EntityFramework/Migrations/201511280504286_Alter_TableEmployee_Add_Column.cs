namespace TIBS.SAFCO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Alter_TableEmployee_Add_Column : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Employees", "UserId", c => c.Long());
            CreateIndex("dbo.Employees", "UserId");
            AddForeignKey("dbo.Employees", "UserId", "dbo.AbpUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Employees", "UserId", "dbo.AbpUsers");
            DropIndex("dbo.Employees", new[] { "UserId" });
            DropColumn("dbo.Employees", "UserId");
        }
    }
}
