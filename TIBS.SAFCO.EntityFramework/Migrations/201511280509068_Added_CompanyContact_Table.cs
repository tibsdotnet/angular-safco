namespace TIBS.SAFCO.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_CompanyContact_Table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contact",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CompanyId = c.Int(nullable: false),
                        RegionId = c.Int(),
                        TitleOfCourtey = c.String(),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(),
                        EmailId = c.String(nullable: false),
                        Address = c.String(),
                        Description = c.String(),
                        Notes = c.String(),
                        Fax = c.String(),
                        POBox = c.String(),
                        ProfilePictuePath = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CompanyContact_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: true)
                .ForeignKey("dbo.Regions", t => t.RegionId)
                .Index(t => t.CompanyId)
                .Index(t => t.RegionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contact", "RegionId", "dbo.Regions");
            DropForeignKey("dbo.Contact", "CompanyId", "dbo.Companies");
            DropIndex("dbo.Contact", new[] { "RegionId" });
            DropIndex("dbo.Contact", new[] { "CompanyId" });
            DropTable("dbo.Contact",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CompanyContact_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
