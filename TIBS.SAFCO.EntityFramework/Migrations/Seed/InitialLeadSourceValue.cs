﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.EntityFramework;
using TIBS.SAFCO.LeadSources;

namespace TIBS.SAFCO.Migrations.Seed
{
    public class InitialLeadSourceValue
    {
        private readonly SAFCODbContext _context;

        public InitialLeadSourceValue(SAFCODbContext context)
        {
            _context = context;
        }

       public void Create()
       {

           var v1 = _context.LeadSources.FirstOrDefault(p => p.Tittle == "Cold Call");
           if (v1 == null)
           {
               _context.LeadSources.Add(
                   new LeadSource
                   {
                       Tittle = "Cold Call"
                   });
           }

           var v2 = _context.LeadSources.FirstOrDefault(p => p.Tittle == "Website");
           if (v2 == null)
           {
               _context.LeadSources.Add(
                   new LeadSource
                   {
                       Tittle = "Website"
                   });
           }

           var v3 = _context.LeadSources.FirstOrDefault(p => p.Tittle == "Internal Referral");
           if (v3 == null)
           {
               _context.LeadSources.Add(
                   new LeadSource
                   {
                       Tittle = "Internal Referral"
                   });
           }

           var v4 = _context.LeadSources.FirstOrDefault(p => p.Tittle == "Trade Show");
           if (v4 == null)
           {
               _context.LeadSources.Add(
                   new LeadSource
                   {
                       Tittle = "Trade Show"
                   });
           }
           var v5 = _context.LeadSources.FirstOrDefault(p => p.Tittle == "BNI");
           if (v5 == null)
           {
               _context.LeadSources.Add(
                   new LeadSource
                   {
                       Tittle = "BNI"
                   });
           }

       }
    }
}
