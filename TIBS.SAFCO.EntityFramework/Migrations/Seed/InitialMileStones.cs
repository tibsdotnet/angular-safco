﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.EntityFramework;
using TIBS.SAFCO.MileStoness;

namespace TIBS.SAFCO.Migrations.Seed
{
    public class InitialMileStones
    {
        private readonly SAFCODbContext _context;
        public InitialMileStones(SAFCODbContext context)
        {
            _context = context;
        }
        public void Create()
        {
            var v1 = _context.MileStones.FirstOrDefault(p => p.MileStoneName == "Allocated");
            if (v1 == null)
            {
                _context.MileStones.Add(
                    new MileStone
                    {
                        MileStoneName = "Allocated"
                    });
            }
            var v2 = _context.MileStones.FirstOrDefault(p => p.MileStoneName == "Suspect");
            if (v2 == null)
            {
                _context.MileStones.Add(
                    new MileStone
                    {
                        MileStoneName = "Suspect"
                    });
            }
            var v3 = _context.MileStones.FirstOrDefault(p => p.MileStoneName == "Prospect");
            if (v3 == null)
            {
                _context.MileStones.Add(
                    new MileStone
                    {
                        MileStoneName = "Prospect"
                    });
            }
            var v4 = _context.MileStones.FirstOrDefault(p => p.MileStoneName == "Negotiation");
            if (v4 == null)
            {
                _context.MileStones.Add(
                    new MileStone
                    {
                        MileStoneName = "Negotiation"
                    });
            }
            var v5 = _context.MileStones.FirstOrDefault(p => p.MileStoneName == "Won");
            if (v5 == null)
            {
                _context.MileStones.Add(
                    new MileStone
                    {
                        MileStoneName = "Won"
                    });
            }
            var v6 = _context.MileStones.FirstOrDefault(p => p.MileStoneName == "Lost");
            if (v6 == null)
            {
                _context.MileStones.Add(
                    new MileStone
                    {
                        MileStoneName = "Lost"
                    });
            }
            var v7 = _context.MileStones.FirstOrDefault(p => p.MileStoneName == "Junk");
            if (v7 == null)
            {
                _context.MileStones.Add(
                    new MileStone
                    {
                        MileStoneName = "Junk"
                    });
            }
        }
    }
}
