﻿using EntityFramework.DynamicFilters;
using TIBS.SAFCO.EntityFramework;

namespace TIBS.SAFCO.Migrations.Seed
{
    public class InitialDbBuilder
    {
        private readonly SAFCODbContext _context;

        public InitialDbBuilder(SAFCODbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            _context.DisableAllFilters();

            new DefaultEditionCreator(_context).Create();
            new DefaultLanguagesCreator(_context).Create();
            new DefaultTenantRoleAndUserCreator(_context).Create();
            new DefaultSettingsCreator(_context).Create();
            new InitialLeadSourceValue(_context).Create();
            new InitialStatusCreater(_context).Create();
            new InitialApprovalStatus(_context).Create();
            new InitialMileStones(_context).Create();

            _context.SaveChanges();
        }
    }
}
