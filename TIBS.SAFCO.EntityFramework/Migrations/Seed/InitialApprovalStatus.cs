﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.ApprovalStatusss;
using TIBS.SAFCO.EntityFramework;

namespace TIBS.SAFCO.Migrations.Seed
{
    public class InitialApprovalStatus
    {
        private readonly SAFCODbContext _context;

        public InitialApprovalStatus(SAFCODbContext context)
        {
            _context = context;
        }
        public void Create()
        {
            var v1 = _context.ApprovalStatuss.FirstOrDefault(p => p.ApprovalStatusName == "Pending Approval");
            if (v1 == null)
            {
                _context.ApprovalStatuss.Add(
                    new ApprovalStatus
                    {
                        ApprovalStatusName = "Pending Approval"
                    });
            }
            var v2 = _context.ApprovalStatuss.FirstOrDefault(p => p.ApprovalStatusName == "Evaluation");
            if (v2 == null)
            {
                _context.ApprovalStatuss.Add(
                    new ApprovalStatus
                    {
                        ApprovalStatusName = "Evaluation"
                    });
            }
            var v3 = _context.ApprovalStatuss.FirstOrDefault(p => p.ApprovalStatusName == "Approved");
            if (v3 == null)
            {
                _context.ApprovalStatuss.Add(
                    new ApprovalStatus
                    {
                        ApprovalStatusName = "Approved"
                    });
            }
            var v4 = _context.ApprovalStatuss.FirstOrDefault(p => p.ApprovalStatusName == "Rejected");
            if (v4 == null)
            {
                _context.ApprovalStatuss.Add(
                    new ApprovalStatus
                    {
                        ApprovalStatusName = "Rejected"
                    });
            }
        }
    }
}
