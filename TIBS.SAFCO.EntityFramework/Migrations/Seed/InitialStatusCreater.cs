﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.SAFCO.EntityFramework;
using TIBS.SAFCO.LeadStatus;

namespace TIBS.SAFCO.Migrations.Seed
{
    
   public class InitialStatusCreater
    {
       private readonly SAFCODbContext _context;
       public InitialStatusCreater(SAFCODbContext context)
       {

           _context=context;
       }
       public void Create()
       {

           var v1 = _context.Statues.FirstOrDefault(p => p.StatusName == "Lead");
           if (v1 == null)
           {
               _context.Statues.Add(
                   new Status
                   {
                       StatusCode = "Lead",
                       StatusName = "Lead"
                   });
           }

           var v2 = _context.Statues.FirstOrDefault(p => p.StatusName == "Suspect");
           if (v2 == null)
           {
               _context.Statues.Add(
                   new Status
                   {
                       StatusCode = "SUS",
                       StatusName = "Suspect"
                   });
           }

           var v3 = _context.Statues.FirstOrDefault(p => p.StatusName == "Prospect");
           if (v3 == null)
           {
               _context.Statues.Add(
                   new Status
                   {
                       StatusCode = "Pros",
                       StatusName = "Prospect"
                   });
           }

           var v4 = _context.Statues.FirstOrDefault(p => p.StatusName == "Customer");
           if (v4 == null)
           {
               _context.Statues.Add(
                   new Status
                   {
                       StatusCode = "Cus",
                       StatusName = "Customer"
                   });
           }

           var v5 = _context.Statues.FirstOrDefault(p => p.StatusName == "Junk");
           if (v5 == null)
           {
               _context.Statues.Add(
                   new Status
                   {
                       StatusCode = "Junk",
                       StatusName = "Junk"
                   });
           }

          

       }

    }
}
