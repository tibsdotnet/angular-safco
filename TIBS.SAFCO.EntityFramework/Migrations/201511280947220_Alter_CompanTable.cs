namespace TIBS.SAFCO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Alter_CompanTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "ManagedId", c => c.Int());
            CreateIndex("dbo.Companies", "ManagedId");
            AddForeignKey("dbo.Companies", "ManagedId", "dbo.Employees", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Companies", "ManagedId", "dbo.Employees");
            DropIndex("dbo.Companies", new[] { "ManagedId" });
            DropColumn("dbo.Companies", "ManagedId");
        }
    }
}
