namespace TIBS.SAFCO.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Table_Competitor : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CompetitorCompany",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CompetitorCompanyName = c.String(nullable: false, maxLength: 50),
                        CompetitorCompanyCode = c.String(nullable: false, maxLength: 10),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CompetitorCompany_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.CompetitorCompany",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CompetitorCompany_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
