namespace TIBS.SAFCO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Alter_Lead_Table_Add_Column : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Lead", "CompetitorId", c => c.Int());
            CreateIndex("dbo.Lead", "CompetitorId");
            AddForeignKey("dbo.Lead", "CompetitorId", "dbo.CompetitorCompany", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Lead", "CompetitorId", "dbo.CompetitorCompany");
            DropIndex("dbo.Lead", new[] { "CompetitorId" });
            DropColumn("dbo.Lead", "CompetitorId");
        }
    }
}
