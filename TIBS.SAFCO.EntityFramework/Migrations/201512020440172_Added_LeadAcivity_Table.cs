namespace TIBS.SAFCO.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_LeadAcivity_Table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LeadActivity",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Notes = c.String(),
                        Title = c.String(),
                        LeadId = c.Int(),
                        ActivityId = c.Int(),
                        UserId = c.Long(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_LeadActivity_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbpUsers", t => t.UserId)
                .ForeignKey("dbo.Activities", t => t.ActivityId)
                .ForeignKey("dbo.Lead", t => t.LeadId)
                .Index(t => t.LeadId)
                .Index(t => t.ActivityId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LeadActivity", "LeadId", "dbo.Lead");
            DropForeignKey("dbo.LeadActivity", "ActivityId", "dbo.Activities");
            DropForeignKey("dbo.LeadActivity", "UserId", "dbo.AbpUsers");
            DropIndex("dbo.LeadActivity", new[] { "UserId" });
            DropIndex("dbo.LeadActivity", new[] { "ActivityId" });
            DropIndex("dbo.LeadActivity", new[] { "LeadId" });
            DropTable("dbo.LeadActivity",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_LeadActivity_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
