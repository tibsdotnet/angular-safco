namespace TIBS.SAFCO.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class added_Companies_Table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Description = c.String(),
                        Address = c.String(),
                        RegionId = c.Int(nullable: false),
                        LeadSourceId = c.Int(),
                        StatusId = c.Int(),
                        POBox = c.String(),
                        PhoneNo = c.String(),
                        WebSite = c.String(),
                        Fax = c.String(),
                        ContributedId = c.Long(),
                        PublishedTime = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Company_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbpUsers", t => t.ContributedId)
                .ForeignKey("dbo.LeadSource", t => t.LeadSourceId)
                .ForeignKey("dbo.Regions", t => t.RegionId, cascadeDelete: true)
                .ForeignKey("dbo.Status", t => t.StatusId)
                .Index(t => t.RegionId)
                .Index(t => t.LeadSourceId)
                .Index(t => t.StatusId)
                .Index(t => t.ContributedId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Companies", "StatusId", "dbo.Status");
            DropForeignKey("dbo.Companies", "RegionId", "dbo.Regions");
            DropForeignKey("dbo.Companies", "LeadSourceId", "dbo.LeadSource");
            DropForeignKey("dbo.Companies", "ContributedId", "dbo.AbpUsers");
            DropIndex("dbo.Companies", new[] { "ContributedId" });
            DropIndex("dbo.Companies", new[] { "StatusId" });
            DropIndex("dbo.Companies", new[] { "LeadSourceId" });
            DropIndex("dbo.Companies", new[] { "RegionId" });
            DropTable("dbo.Companies",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Company_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
