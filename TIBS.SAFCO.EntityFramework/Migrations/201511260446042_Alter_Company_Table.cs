namespace TIBS.SAFCO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Alter_Company_Table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Companies", "Published", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Companies", "Published");
        }
    }
}
