﻿using System.Data.Common;
using System.Data.Entity;
using Abp.Zero.EntityFramework;
using TIBS.SAFCO.Authorization.Roles;
using TIBS.SAFCO.Authorization.Users;
using TIBS.SAFCO.MultiTenancy;
using TIBS.SAFCO.Storage;
using TIBS.SAFCO.Countryss;
using TIBS.SAFCO.Regions;
using TIBS.SAFCO.LeadSources;
using TIBS.SAFCO.LeadStatus;
using TIBS.SAFCO.Compnaies;
using TIBS.SAFCO.Announcements;
using TIBS.SAFCO.ProductClassifications;
using TIBS.SAFCO.Products;
using TIBS.SAFCO.Activities;
using TIBS.SAFCO.Employees;
using TIBS.SAFCO.ApprovalStatusss;
using TIBS.SAFCO.MileStoness;
using TIBS.SAFCO.LeadRequests;
using TIBS.SAFCO.CompanyContacts;
using TIBS.SAFCO.Leadss;
using TIBS.SAFCO.LeadActivities;
using TIBS.SAFCO.CompetitorCompaniesss;
using TIBS.SAFCO.Schedulee;
using TIBS.SAFCO.UserTargett;

namespace TIBS.SAFCO.EntityFramework
{
    public class SAFCODbContext : AbpZeroDbContext<Tenant, Role, User>
    {
        /* Define an IDbSet for each entity of the application */

        public virtual IDbSet<BinaryObject> BinaryObjects { get; set; }
        public virtual IDbSet<Country> Countries { get; set; }
        public virtual IDbSet<Region> Regions { get; set; }
        public virtual IDbSet<LeadSource> LeadSources { get; set; }
        public virtual IDbSet<Status> Statues { get; set; }
        public virtual IDbSet<Company> Companies { get; set; }
        public virtual IDbSet<Announcement> Announcements { get; set; }
        public virtual IDbSet<ProductClassification> ProductClassifications { get; set; }
        public virtual IDbSet<Product> Products { get; set; }
        public virtual IDbSet<Activity> Activities { get; set; }
        public virtual IDbSet<Employee> Employees { get; set; }
        public virtual IDbSet<ApprovalStatus> ApprovalStatuss { get; set; }
        public virtual IDbSet<MileStone> MileStones { get; set; }
        public virtual IDbSet<LeadRequest> LeadRequests { get; set; }
        public virtual IDbSet<CompanyContact> CompanyContacts { get; set; }
        public virtual IDbSet<Lead> Leads { get; set; }
        public virtual IDbSet<LeadActivity> LeadActivities { get; set; }
        public virtual IDbSet<CompetitorCompany> CompetitorCompanys { get; set; }
        public virtual IDbSet<Schedules> Schedule { get; set; }
        public virtual IDbSet<UserTarget> EmployeeTarget { get; set; }
        /* Setting "Default" to base class helps us when working migration commands on Package Manager Console.
         * But it may cause problems when working Migrate.exe of EF. ABP works either way.         * 
         */
        public SAFCODbContext()
            : base("Default")
        {

        }

        /* This constructor is used by ABP to pass connection string defined in SAFCODataModule.PreInitialize.
         * Notice that, actually you will not directly create an instance of SAFCODbContext since ABP automatically handles it.
         */
        public SAFCODbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }

        /* This constructor is used in tests to pass a fake/mock connection.
         */
        public SAFCODbContext(DbConnection dbConnection)
            : base(dbConnection, true)
        {

        }
    }
}
