using System.Data.Entity.Migrations;
using TIBS.SAFCO.Migrations.Seed;

namespace TIBS.SAFCO.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<SAFCO.EntityFramework.SAFCODbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "SAFCO";
        }

        protected override void Seed(SAFCO.EntityFramework.SAFCODbContext context)
        {
            new InitialDbBuilder(context).Create();
        }
    }
}
