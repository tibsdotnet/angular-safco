﻿(function () {
    appModule.controller('tenant.views.dashboard.index', [
        '$scope', 'abp.services.app.tenantDashboard', '$log',
        function ($scope, tenantDashboardService, $log) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            // vm.slides = [
            //{ 'src': '/images/photo2.jpg','id':'/images/photo2.jpg' },
            //{ 'src': '/images/photo3.jpg', 'id': '/images/photo3.jpg' },
            //{ 'src': '/images/photo4.jpg', 'id': '/images/photo4.jpg' },
            //{ 'src': '/images/photo5.jpg', 'id': '/images/photo5.jpg' },
            //{ 'src': '/images/photo6.jpg', 'id': '/images/photo6.jpg' },
            //{ 'src': '/images/photo7.jpg', 'id': '/images/photo7.jpg' },
            //{ 'src': '/images/photo8.jpg', 'id': '/images/photo8.jpg' },
            //{ 'src': '/images/1.jpg', 'id': '/images/1.jpg', },
            //{ 'src': '/images/1777342.jpg', 'id': '/images/1777342.jpg' },
            //{ 'src': '/images/1837898.jpg', 'id': '/images/1837898.jpg' },
            //{ 'src': '/images/download (2).jpg', 'id': '/images/download (2).jpg' },
            //{ 'src': '/images/images (10).jpg', 'id': '/images/images (10).jpg' },
            //{ 'src': '/images/spring-in-nature-wide-wallpaper-603794.jpg', 'id': '/images/spring-in-nature-wide-wallpaper-603794.jpg' },
            //{ 'src': '/images/ws_Nature_1920x1080.jpg', 'id': '/images/ws_Nature_1920x1080.jpg' }
            // ];

            // console.log(vm.slides);

            vm.options = {
                //sourceProp: ['id','amount','newCustomer','profilePictureId','rank','userName'],
                sourceProp: 'src',
                visible: 9,
                perspective: 50,
                startSlide: 0,
                border: 3,
                width: 280,
                height: 160,
                space: 200
            };

            vm.selectedClick = selectedClick;
            vm.slideChanged = slideChanged;
            vm.beforeChange = beforeChange;
            vm.lastSlide = lastSlide;
            vm.slide = 0;

            function lastSlide(index) {
                $log.log('Last Slide Selected callback triggered. \n == Slide index is: ' + index + ' ==');
            }

            function beforeChange(index) {
                $log.log('Before Slide Change callback triggered. \n == Slide index is: ' + index + ' ==');
            }

            function selectedClick(index) {
                vm.slide = index;
                $log.log('Selected Slide Clicked callback triggered. \n == Slide index is: ' + index + ' ==');
                vm.chart(vm.slides[index].id);
                newcustomer1(vm.slides[index].id);
                negotiation1(vm.slides[index].id);
                prospect1(vm.slides[index].id);               
            }

            function slideChanged(index) {
                vm.slide = index;
                $log.log('Slide Changed callback triggered. \n == Slide index is: ' + index + ' ==');
                vm.chart(vm.slides[index].id);
                newcustomer1(vm.slides[index].id);
                negotiation1(vm.slides[index].id);
                prospect1(vm.slides[index].id);
            }

            function init() {
                tenantDashboardService.salesBoard().success(function (result) {
                    vm.slides = result.salesBoard;
                    if (vm.slides == null) {
                        vm.chart(0);
                        newcustomer1(0);
                        negotiation1(0);
                        prospect1(0);
                    }
                    else {
                        vm.chart(vm.slides[0].id);
                        newcustomer1(vm.slides[0].id);
                        negotiation1(vm.slides[0].id);
                        prospect1(vm.slides[0].id);
                    }
                });

            };

            var monthlyorderamount = [];
            var score1 = [];
            vm.chart = function (userid) {
                tenantDashboardService.mainProjectActivityRate(userid).success(function (result) {
                    var name1 = [];
                    monthlyorderamount = [];
                    score1 = [];
                    for (var i = 0; i < result.monthlyOrder.length; i++) {
                        var data = {};
                        var val = [];
                        val.push(parseInt(result.monthlyOrder[i].amount));
                        name1.push(result.monthlyOrder[i].date);
                        data['data'] = val;
                        data['name'] = result.monthlyOrder[i].date;
                        if (parseInt(result.monthlyOrder[i].amount) > 0) {
                            monthlyorderamount.push([result.monthlyOrder[i].date, parseInt(result.monthlyOrder[i].amount)]);
                        }
                        score1.push(data);
                    }
                    $scope.chart1 = {
                        options: {
                            chart: {
                                type: 'column'
                            },
                            plotOptions: {
                                pie: {
                                    dataLabels: {
                                        enabled: true,
                                        format: '<b>{point.percentage:.1f}%</b>'
                                    }
                                }
                            },
                            tooltip: {
                                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f} AED</b> <br/>'
                            },
                        },
                        credits: {
                            enabled: false
                        },
                        yAxis: {
                            title: {
                                text: 'AED'
                            }

                        },
                        //xAxis: {
                        //    categories:'Monthly Wise'
                        //},
                        series: score1,
                        title: {
                            text: 'Monthly Order Amount'
                        },
                        legend: {
                            enabled: true
                        }
                    }


                    var score2 = [];
                    for (var i = 0; i < result.pMonthLeads.length; i++) {
                        score2.push([result.pMonthLeads[i].status, result.pMonthLeads[i].amount]);
                    }

                    $scope.chart2 = {
                        options: {
                            chart: {
                                type: 'pie'
                            },
                            plotOptions: {
                                pie: {
                                    dataLabels: {
                                        enabled: true,
                                        format: '<b>{point.percentage:.1f}%</b>'
                                    }
                                }
                            },
                            tooltip: {
                                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f} AED</b><br/>'
                            },
                        },
                        title: {
                            text: 'Present Month Leads'
                        },
                        legend: {
                            enabled: true
                        },
                        credits: {
                            enabled: false
                        },
                        series: [{
                            name: 'Leads',
                            //size: '80%',
                            //innerSize: '65%',
                            showInLegend: true,
                            data: score2
                        }],
                        loading: false
                    }

                    var score3 = [];
                    for (var i = 0; i < result.lSixMonthLeads.length; i++) {
                        score3.push([result.lSixMonthLeads[i].status, result.lSixMonthLeads[i].amount]);
                    }

                    $scope.chart3 = {
                        options: {
                            chart: {
                                type: 'pie'
                            },
                            plotOptions: {
                                pie: {
                                    dataLabels: {
                                        enabled: true,
                                        format: '<b>{point.percentage:.1f}%</b>'
                                    }
                                }
                            },
                            tooltip: {
                                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f} AED</b><br/>'
                            },
                        },
                        title: {
                            text: 'Last Six Month Leads'
                        },
                        legend: {
                            enabled: true
                        },
                        credits: {
                            enabled: false
                        },
                        series: [{
                            name: 'SixLeads',
                            size: '80%',
                            innerSize: '65%',
                            showInLegend: true,
                            data: score3
                        }],
                        loading: false
                    }

                    $scope.chart4 = {
                        options: {
                            chart: {
                                type: 'column'
                            },
                            plotOptions: {
                                column: {
                                    stacking: 'normal',
                                    dataLabels: {
                                        enabled: true,
                                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                                        style: {
                                            textShadow: '0 0 3px black'
                                        }
                                    }
                                }
                            },
                            tooltip: {
                                headerFormat: '<b>{point.x}</b><br/>',
                                pointFormat: '<span style="color:{point.color}">{series.name}</span>:{point.y} AED<br/>Total: {point.stackTotal} AED'
                            },
                        },
                        credits: {
                            enabled: false
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                        },
                        yAxis: {
                            title: {
                                text: 'AED'
                            },
                            stackLabels: {
                                enabled: true,
                                style: {
                                    fontWeight: 'bold',
                                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                                }
                            }
                        },
                        xAxis: {
                            categories: result.leadStatus
                        },

                        series: result.monthlyLeads,
                        title: {
                            text: 'Monthly Leads'
                        },
                        legend: {
                            enabled: true
                        }
                    }

                });
            };

            $scope.type3 = function () {
                this.chart1.options.chart.type = 'pie'
                this.chart1.series = [];
                this.chart1.series.push({
                    showInLegend: true,
                    name: 'Monthly Order',
                    data: monthlyorderamount

                })
            }

            $scope.type33 = function () {
                this.chart1.options.chart.type = 'column'
                this.chart1.series = score1;
            }

            $scope.type4 = function () {
                this.chart4.options.plotOptions.column.stacking = 'percent';
            }

            $scope.type44 = function () {
                this.chart4.options.plotOptions.column.stacking = 'normal'
            }

            init();

            $('input[name="my-checkbox"]').on('switchChange.bootstrapSwitch', function (event, state) {
                if (state == true) {
                    vm.chart(0);
                    newcustomer1(0);
                    negotiation1(0);
                    prospect1(0);                    
                }
                else {
                    vm.chart(vm.slides[vm.slide].id);
                    newcustomer1(vm.slides[vm.slide].id);
                    negotiation1(vm.slides[vm.slide].id);
                    prospect1(vm.slides[vm.slide].id);
                }
            });

            var newcustomer = 0;
            var Negotiation = 0;
            var prospect = 0;


            function newcustomer1(id) {
                $.ajax({
                    type: "POST",
                    async: false,
                    url: "/NewCustomer/ResultOFCustomer/?id=" + id,
                    content: "application/json; charset=utf-8",
                    success: function (result) {
                        newcustomer = result.result

                    }

                });

                $('#container').highcharts({
                    chart: {
                        type: 'gauge',
                        plotBorderWidth: 0,
                        plotBackgroundColor: {
                            linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                            stops: [
                                [0, '#EEF1F5'],
                                [0.3, '#EEF1F5'],
                                [1, '#EEF1F5']
                            ]
                        },
                        plotBackgroundImage: null,
                        height: 150
                    },

                    title: {
                        text: ''
                    },

                    pane: [{
                        startAngle: -60,
                        endAngle: 60,
                        background: null,
                        center: ['50%', '140%'],
                        size: 300
                    }],
                    credits: {
                        enabled: false
                    },
                    tooltip: {
                        enabled: false
                    },

                    yAxis: [{
                        min: 0,
                        max: 100,
                        minorTickPosition: 'outside',
                        tickPosition: 'outside',
                        labels: {
                            rotation: 'auto',
                            distance: 5
                        },
                        plotBands: [{
                            from: 0,
                            to: 50,
                            color: '#EF3F3F',
                            innerRadius: '100%',
                            outerRadius: '105%'
                        }, {
                            from: 50,
                            to: 100,
                            color: '#4C9628',
                            innerRadius: '100%',
                            outerRadius: '105%'
                        }],
                        pane: 0,
                        title: {
                            text: newcustomer + '<br/><span style="font-size:8px"> New Customer</span>',
                            y: -40
                        }
                    }],

                    plotOptions: {
                        gauge: {
                            dataLabels: {
                                enabled: false
                            },
                            dial: {
                                radius: '100%'
                            }
                        }
                    },
                    series: [{
                        name: 'Channel A',
                        data: [newcustomer],
                        yAxis: 0
                    }]
                });
            }

            function negotiation1(id) {
                $.ajax({
                    type: "POST",
                    async: false,
                    url: "/NewCustomer/ResultNegotiation/?id=" + id,
                    content: "application/json; charset=utf-8",
                    success: function (result) {
                        Negotiation = result.result
                    }
                });

                $('#Negozation').highcharts({
                    chart: {
                        type: 'gauge',
                        plotBorderWidth: 0,
                        plotBackgroundColor: {
                            linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                            stops: [
                                [0, '#EEF1F5'],
                                [0.3, '#EEF1F5'],
                                [1, '#EEF1F5']
                            ]
                        },
                        plotBackgroundImage: null,
                        height: 150
                    },

                    title: {
                        text: ''
                    },

                    pane: [{
                        startAngle: -60,
                        endAngle: 60,
                        background: null,
                        center: ['50%', '140%'],
                        size: 300
                    }],
                    credits: {
                        enabled: false
                    },
                    tooltip: {
                        enabled: false
                    },

                    yAxis: [{
                        min: 0,
                        max: 100,
                        minorTickPosition: 'outside',
                        tickPosition: 'outside',
                        labels: {
                            rotation: 'auto',
                            distance: 5
                        },
                        plotBands: [{
                            from: 0,
                            to: 50,
                            color: '#EF3F3F',
                            innerRadius: '100%',
                            outerRadius: '105%'
                        }, {
                            from: 50,
                            to: 100,
                            color: '#4C9628',
                            innerRadius: '100%',
                            outerRadius: '105%'
                        }],
                        pane: 0,
                        title: {
                            text: Negotiation + '<br/><span style="font-size:8px">Negotiation</span>',
                            y: -40
                        }
                    }],

                    plotOptions: {
                        gauge: {
                            dataLabels: {
                                enabled: false
                            },
                            dial: {
                                radius: '100%'
                            }
                        }
                    },
                    series: [{
                        name: 'Channel A',
                        data: [Negotiation],
                        yAxis: 0
                    }]
                });
            }

            function prospect1(id) {
                $.ajax({
                    type: "POST",
                    async: false,
                    url: "/NewCustomer/ResultProspect/?id=" + id,
                    content: "application/json; charset=utf-8",
                    success: function (result) {
                        prospect = parseInt(result.result);
                    }
                });

                $('#Prospect').highcharts({
                    chart: {
                        type: 'gauge',
                        plotBorderWidth: 0,
                        plotBackgroundColor: {
                            linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                            stops: [
                                [0, '#EEF1F5'],
                                [0.3, '#EEF1F5'],
                                [1, '#EEF1F5']
                            ]
                        },
                        plotBackgroundImage: null,
                        height: 150
                    },

                    title: {
                        text: ''
                    },

                    pane: [{
                        startAngle: -60,
                        endAngle: 60,
                        background: null,
                        center: ['50%', '140%'],
                        size: 300
                    }],
                    credits: {
                        enabled: false
                    },
                    tooltip: {
                        enabled: false
                    },

                    yAxis: [{
                        min: 0,
                        max: 100,
                        minorTickPosition: 'outside',
                        tickPosition: 'outside',
                        labels: {
                            rotation: 'auto',
                            distance: 5
                        },
                        plotBands: [{
                            from: 0,
                            to: 50,
                            color: '#EF3F3F',
                            innerRadius: '100%',
                            outerRadius: '105%'
                        }, {
                            from: 50,
                            to: 100,
                            color: '#4C9628',
                            innerRadius: '100%',
                            outerRadius: '105%'
                        }],
                        pane: 0,
                        title: {
                            text: prospect + '<br/><span style="font-size:8px">Prospect</span>',
                            y: -40
                        }
                    }],

                    plotOptions: {
                        gauge: {
                            dataLabels: {
                                enabled: false
                            },
                            dial: {
                                radius: '100%'
                            }
                        }
                    },
                    series: [{
                        name: 'Channel A',
                        data: [prospect],
                        yAxis: 0
                    }]
                });
            }

        }
    ]);
})();